#include "MeshGeneration.h"

#include <vtkPCANormalEstimation.h>
#include <vtkSignedDistance.h>
#include <vtkExtractSurface.h>
//#include <vtkPoissonReconstruction.h>
#include <vtkDelaunay2D.h>
#include <vtkMath.h>
#include <vtkSmartPointer.h>
#include <vtkPointLocator.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkPointData.h>

#include <stdio.h>
#include <iostream>     
#include <fstream> 
#include <stdlib.h>
#include <vector>

using namespace std;

void CleanSmoothPointCloud(vtkPolyData* Poly_in, vtkPolyData* Poly_out
	, vtkIdList* SelectedIDList_out
	, const int downsample_step
	, const double removeoutliers_radius, const int removeoutliers_threshold
	, const double Smooth_radius, const int SmoothIterNum);
	
void GenerateSmoothedRGBPolyData(vtkUnsignedCharArray* RGB_in, vtkIdList* SelectedIDList_in
	, vtkPolyData* Poly);
	
void Delaunay(vtkPoints* Points_in, vtkPolyData* Poly_out
	, const double alpha
	, const double MaxLength);
	
int smoothvtkpolydata(vtkPolyData* Poly, int SmoothIterNum);

void ExtractSurface(vtkPolyData* PointCloud_in, vtkPolyData* Mesh_out);

void MeshGeneration_Main(vtkPolyData* PointCloud_in, vtkPolyData* Mesh_out)
{
	vtkSmartPointer<vtkPolyData> Poly_PointCloud_smoothed = vtkSmartPointer<vtkPolyData>::New();
	vtkSmartPointer<vtkPoints> Points_PointCloud_smoothed = vtkSmartPointer<vtkPoints>::New();
	Poly_PointCloud_smoothed->SetPoints(Points_PointCloud_smoothed);

	vtkSmartPointer<vtkIdList> DownSampledIdList = vtkSmartPointer<vtkIdList>::New();
	CleanSmoothPointCloud(PointCloud_in, Poly_PointCloud_smoothed
		, DownSampledIdList
		, 5
		, 20.0, 20
		, .01, 0); //10.0, 2
	
	vtkUnsignedCharArray* RGB_in = vtkUnsignedCharArray::SafeDownCast(PointCloud_in->GetPointData()->GetAbstractArray("RGB"));
	GenerateSmoothedRGBPolyData(RGB_in, DownSampledIdList
		, Poly_PointCloud_smoothed);

	vtkSmartPointer<vtkPolyData> Poly_mesh = vtkSmartPointer<vtkPolyData>::New();
//	Delaunay(Poly_PointCloud_smoothed->GetPoints(), Poly_3, 0, 50.0);
	ExtractSurface(Poly_PointCloud_smoothed, Poly_mesh);
	Mesh_out->DeepCopy(Poly_mesh);

//	Mesh_out->GetPointData()->SetScalars(Poly_PointCloud_smoothed->GetPointData()->GetArray("RGB"));
//	std::cout << "start smoothvtkpolydata!" << std::endl;
//	smoothvtkpolydata(Mesh_out, 3);
}

void CleanSmoothPointCloud(vtkPolyData* Poly_in, vtkPolyData* Poly_out
					, vtkIdList* SelectedIDList_out
					, const int downsample_step
					, const double removeoutliers_radius, const int removeoutliers_threshold
					, const double Smooth_radius, const int SmoothIterNum)
{
	if (Poly_in == NULL)
	{
		std::cout << "Cannot find the input polydata!" << std::endl;
		return;
	}
	vtkPoints* Points_orig = Poly_in->GetPoints();
	if (Points_orig == NULL)
	{
		std::cout << "Cannot find the input points!" << std::endl;
		return;
	}
	if (Points_orig->GetNumberOfPoints() == 0)
	{
		std::cout << "Cannot find the input points!" << std::endl;
		return;
	}


	vtkSmartPointer<vtkPolyData> Poly_downsample = vtkSmartPointer<vtkPolyData>::New();
	vtkSmartPointer<vtkPoints> Points_downsample = vtkSmartPointer<vtkPoints>::New();
	Poly_downsample->SetPoints(Points_downsample);
	vtkSmartPointer<vtkIdList> SelectedIDList_downsample = vtkSmartPointer<vtkIdList>::New();
	for (vtkIdType i = 0; i < Points_orig->GetNumberOfPoints(); i+= downsample_step)
	{
		double coord[3];
		Points_orig->GetPoint(i, coord);	

		Points_downsample->InsertNextPoint(coord);
		SelectedIDList_downsample->InsertNextId(i);
	}

	vtkSmartPointer<vtkPointLocator> pointLocator = vtkSmartPointer<vtkPointLocator>::New();
	pointLocator->SetDataSet(Poly_downsample);
	pointLocator->AutomaticOn();
	pointLocator->SetNumberOfPointsPerBucket(2);
	pointLocator->BuildLocator();

	vtkSmartPointer<vtkPoints> Points_clean = vtkSmartPointer<vtkPoints>::New();
	SelectedIDList_out->Reset();
	for (vtkIdType i = 0; i < Points_downsample->GetNumberOfPoints(); i ++)
	{
		double coord[3];
		Points_downsample->GetPoint(i, coord);
		{
			vtkSmartPointer<vtkIdList> ClosePointList = vtkSmartPointer<vtkIdList>::New();
			pointLocator->FindPointsWithinRadius(removeoutliers_radius, coord, ClosePointList);
		//	std::cout << i << ", " << ClosePointList->GetNumberOfIds() << std::endl;
			if (ClosePointList->GetNumberOfIds() < removeoutliers_threshold) continue;
		}
		Points_clean->InsertNextPoint(coord);
		SelectedIDList_out->InsertNextId(SelectedIDList_downsample->GetId(i));
	}
	
	vtkPoints* Points_out = Poly_out->GetPoints();
	Points_out->DeepCopy(Points_clean);

	Points_out->Modified();
	Poly_out->Modified();

	vtkSmartPointer<vtkPointLocator> pointLocator2 = vtkSmartPointer<vtkPointLocator>::New();
	pointLocator2->SetDataSet(Poly_out);
	pointLocator2->AutomaticOn();
	pointLocator2->SetNumberOfPointsPerBucket(2);
	pointLocator2->BuildLocator();

	std::vector< std::vector<vtkIdType> > neighbor;
	for (vtkIdType i = 0; i < Points_out->GetNumberOfPoints(); i ++)
	{
		double coord[3];
		Points_out->GetPoint(i, coord);

		vtkSmartPointer<vtkIdList> ClosePointList = vtkSmartPointer<vtkIdList>::New();
		//pointLocator2->FindClosestNPoints(Smooth_NumberOfClosePoints, coord, ClosePointList);
		pointLocator2->FindPointsWithinRadius(Smooth_radius, coord, ClosePointList);

		std::vector<vtkIdType> neighbor_i;
		for (vtkIdType k = 0; k < ClosePointList->GetNumberOfIds(); k++)
		{
			neighbor_i.push_back(ClosePointList->GetId(k));
		}
		neighbor.push_back(neighbor_i);
	}

	// the smooth algorithm
	{
		vtkSmartPointer<vtkPoints> Points_orig = vtkSmartPointer<vtkPoints>::New();
		vtkSmartPointer<vtkPoints> Points_last = vtkSmartPointer<vtkPoints>::New();
		Points_orig->DeepCopy(Points_out);

		double* b = new double[Points_out->GetNumberOfPoints() * 3];
		double pi[3], qi[3], oi[3];
		const double alpha = 0.1, beta = 0.2;

		for (int iter = 0; iter < SmoothIterNum; iter++)
		{
			Points_last->DeepCopy(Points_out);
			for (vtkIdType pid = 0; pid < Points_out->GetNumberOfPoints(); pid++)
			{
				if (neighbor.at(pid).size() > 0)
				{
					for (int l = 0; l < 3; l++) pi[l] = 0.0;
					for (unsigned int j = 0; j < neighbor.at(pid).size(); j++)
					{
						Points_last->GetPoint(neighbor.at(pid).at(j), qi);
						for (int l = 0; l < 3; l++) pi[l] += qi[l];
					}
					for (int l = 0; l < 3; l++) pi[l] = pi[l] / neighbor.at(pid).size();
					Points_out->SetPoint(pid, pi);
					Points_orig->GetPoint(pid, oi);
					Points_last->GetPoint(pid, qi);

					for (int l = 0; l < 3; l++)	b[pid * 3 + l] = pi[l] - (alpha * oi[l] + (1.0 - alpha) * qi[l]);
				}
			}
			for (vtkIdType pid = 0; pid < Points_out->GetNumberOfPoints(); pid++)
			{
				if (neighbor.at(pid).size() > 0)
				{
					double sumbj[3] = { 0.0, 0.0, 0.0 };

					for (unsigned int j = 0; j < neighbor.at(pid).size(); j++)
					{
						for (int l = 0; l < 3; l++)
							sumbj[l] += b[neighbor.at(pid).at(j) * 3 + l];
						//sumbj[0] += b[adjcent[pid * 10 + j] * 3 + 0];
					}
					for (int l = 0; l < 3; l++)
						sumbj[l] = sumbj[l] / neighbor.at(pid).size();

					Points_out->GetPoint(pid, pi);

					for (int l = 0; l < 3; l++) pi[l] = pi[l] - (beta * b[pid * 3 + l] + (1.0 - beta) * sumbj[l]);

					Points_out->SetPoint(pid, pi);
				}
			}
		}
		delete[] b;
	}
}

void GenerateSmoothedRGBPolyData(vtkUnsignedCharArray* RGB_in, vtkIdList* SelectedIDList_in
	, vtkPolyData* Poly)
{
	vtkSmartPointer<vtkCellArray> CellArray_out = vtkSmartPointer<vtkCellArray>::New();
	Poly->SetVerts(CellArray_out);

	vtkSmartPointer<vtkUnsignedCharArray> RGB_out = vtkSmartPointer<vtkUnsignedCharArray>::New();
	RGB_out->SetName("RGB");
	RGB_out->SetNumberOfComponents(3);
	Poly->GetPointData()->SetScalars(RGB_out);

	{
		vtkIdType pidx = 0;
		for (vtkIdType i = 0; i < SelectedIDList_in->GetNumberOfIds(); i++)
		{
			CellArray_out->InsertNextCell(1, &pidx);

			vtkIdType origid = SelectedIDList_in->GetId(i);
			double thisRGB[3];
			RGB_in->GetTuple(origid, thisRGB);
			RGB_out->InsertNextTuple(thisRGB);
			pidx++;
		}
	}


}


void Delaunay(vtkPoints* Points_in, vtkPolyData* Poly_out
			, const double alpha
			, const double MaxLength)
{
	vtkSmartPointer<vtkDelaunay2D> delaunay = vtkSmartPointer<vtkDelaunay2D>::New();
	//	delaunay->SetTolerance(0.01);
	delaunay->SetAlpha(alpha);
	delaunay->SetProjectionPlaneMode(VTK_BEST_FITTING_PLANE);
	std::cout << "GetAlpha() = " << delaunay->GetAlpha() << std::endl;
	std::cout << "GetTolerance() = " << delaunay->GetTolerance() << std::endl;

	vtkSmartPointer<vtkPolyData> Poly_in = vtkSmartPointer<vtkPolyData>::New();
	Poly_in->SetPoints(Points_in);
//	std::cout << "In Delaunay: Poly_in = " << Poly_in->GetPoints()->GetNumberOfPoints() << std::endl;
	delaunay->SetInputData(Poly_in);
	delaunay->Update();
//	std::cout << "In Delaunay: update finished!" << std::endl;

	Poly_out->DeepCopy(delaunay->GetOutput());
//	std::cout << "In Delaunay: Poly_out = " << Poly_out->GetPoints()->GetNumberOfPoints() << std::endl;

	vtkCellArray* Strips = Poly_out->GetPolys();
	vtkSmartPointer< vtkCellArray > Strips_new = vtkSmartPointer< vtkCellArray >::New();
	vtkIdType CellId = 0;
	vtkIdType npts = 0, * pts = NULL;
	for (CellId = 0, Strips->InitTraversal(); Strips->GetNextCell(npts, pts); CellId++)
	{
		bool flag_good = true;
		for (int i = 0; i < npts; i++)
		{
			double coordi[3];
			Poly_out->GetPoints()->GetPoint(pts[i], coordi);
			for (int j = i + 1; j < npts; j++)
			{
				double coordj[3];
				Poly_out->GetPoints()->GetPoint(pts[j], coordj);

				double d2 = vtkMath::Distance2BetweenPoints(coordi, coordj);
				if (d2 > MaxLength * MaxLength) flag_good = false;
				if (!flag_good) break;
			}
			if (!flag_good) break;
		}
		if (flag_good)
		{
			vtkSmartPointer<vtkIdList> idlist = vtkSmartPointer<vtkIdList>::New();
			for (int i = 0; i < npts; i++)
			{
				idlist->InsertNextId(pts[i]);
			}
			Strips_new->InsertNextCell(idlist);
		}
	}
	Poly_out->SetPolys(Strips_new);
	Poly_out->Modified();

}


void ExtractSurface(vtkPolyData* PointCloud_in, vtkPolyData* Mesh_out)
{
	double bounds[6];
	PointCloud_in->GetBounds(bounds);
	double range[3];
	for (int i = 0; i < 3; ++i)
	{
		range[i] = bounds[2*i + 1] - bounds[2*i];
	}

	int sampleSize = PointCloud_in->GetNumberOfPoints() * .00005;
	if (sampleSize < 10)
    {
    	sampleSize = 10;
	}
	std::cout << "Sample size is: " << sampleSize << std::endl;
	// Do we need to estimate normals?
	vtkSmartPointer<vtkSignedDistance> distance =
		vtkSmartPointer<vtkSignedDistance>::New();
	if (PointCloud_in->GetPointData()->GetNormals())
    {
		std::cout << "Using normals from input file" << std::endl;
		distance->SetInputData (PointCloud_in);
	}
	else
	{
    	std::cout << "Estimating normals using PCANormalEstimation" << std::endl;
		vtkSmartPointer<vtkPCANormalEstimation> normals =
			vtkSmartPointer<vtkPCANormalEstimation>::New();
		normals->SetInputData (PointCloud_in);
		normals->SetSampleSize(sampleSize);
		normals->SetNormalOrientationToGraphTraversal();
		normals->FlipNormalsOn();
		distance->SetInputConnection (normals->GetOutputPort());
    }

	int dimension = 256;
//	double radius = range[0] * .02;
	double radius = range[0] / static_cast<double>(dimension) * 10; // ~15 voxels
//	double radius = 10.0;
//	if (radius < 3.0) radius = 3.0;
	std::cout << "Radius: " << radius << std::endl;

	distance->SetRadius(radius);
	distance->SetDimensions(dimension, dimension, dimension);
	distance->SetBounds(
    bounds[0] - range[0] * .1,
    bounds[1] + range[0] * .1,
    bounds[2] - range[1] * .1,
    bounds[3] + range[1] * .1,
    bounds[4] - range[2] * .1,
    bounds[5] + range[2] * .1);

	vtkSmartPointer<vtkExtractSurface> surface =
		vtkSmartPointer<vtkExtractSurface>::New();
	surface->SetInputConnection (distance->GetOutputPort());
	surface->SetRadius(radius * .99);
	surface->Update();
	  
	Mesh_out->DeepCopy(surface->GetOutput());
	Mesh_out->Modified();
}

/*
void PoissonExtractSurface(vtkPolyData* polyData, vtkPolyData* Mesh_out)
{
	vtkSmartPointer<vtkPoissonReconstruction> surface =
		vtkSmartPointer<vtkPoissonReconstruction>::New();
	surface->SetDepth(12);

	int sampleSize = polyData->GetNumberOfPoints() * .00005;
	if (sampleSize < 10)
	{
		sampleSize = 10;
	}
	std::cout << "sampleSize = " << sampleSize << std::endl;
	if (polyData->GetPointData()->GetNormals())
	{
		std::cout << "Using normals from input file" << std::endl;
		surface->SetInputData (polyData);
	}
	else
	{
		std::cout << "Estimating normals using PCANormalEstimation" << std::endl;
		vtkSmartPointer<vtkPCANormalEstimation> normals =
		vtkSmartPointer<vtkPCANormalEstimation>::New();
		normals->SetInputData (polyData);
		normals->SetSampleSize(sampleSize);
		normals->SetNormalOrientationToGraphTraversal();
		normals->FlipNormalsOff();
		surface->SetInputConnection(normals->GetOutputPort());
    
		surface->SetSamplesPerNode(1.0); 
		surface->SetDepth(10); 
		surface->SetKernelDepth(10); 
		surface->SetSolverDivide(10); 
		surface->SetIsoDivide(10); 
		surface->Update(); 
	}
	Mesh_out->DeepCopy(surface->GetOutput());
	Mesh_out->Modified();
}
*/


int smoothvtkpolydata(vtkPolyData* Poly, int SmoothIterNum)
{
	vtkPoints* Points = Poly->GetPoints();
	vtkCellArray* Strips = Poly->GetPolys();

	std::vector< std::vector<vtkIdType> > neighbor;
	for (vtkIdType i = 0; i < Points->GetNumberOfPoints(); i++)
	{
		std::vector<vtkIdType> neighbor_i;
		neighbor.push_back(neighbor_i);
	}

	vtkIdType CellId = 0;
	vtkIdType npts = 0, * pts = NULL;
	for (CellId = 0, Strips->InitTraversal(); Strips->GetNextCell(npts, pts); CellId++)
	{
		if (npts != 3)
		{
			std::cout << "not triangle, smooth cannot work!" << std::endl;
			return 0;
		}
		for (int i = 0; i < npts; i++)
		{
			int p[2];
			int pidx = 0;
			for (int k = 0; k < npts; k++)
			{
				if (k != i)
				{
					p[pidx] = k;
					pidx ++;
				}
			}
			for (int l = 0; l < 2; l++)
			{
				bool find_pl_in_adjofptsi = false;
				for (unsigned int k = 0; k < neighbor.at(pts[i]).size(); k++)
				{
					if (neighbor.at(pts[i]).at(k) == pts[p[l]])
					{
						find_pl_in_adjofptsi = true;
						break;
					}
				}

				if (find_pl_in_adjofptsi == false)
				{
					neighbor.at(pts[i]).push_back(pts[p[l]]);
				//	adjcent[pts[i] * 10 + num_adjcent[pts[i]]] = pts[p[l]];
				//	num_adjcent[pts[i]] ++;
				}
			}
		}
	}



	// the smooth algorithm
	{
		vtkSmartPointer<vtkPoints> Points_orig = vtkSmartPointer<vtkPoints>::New();
		vtkSmartPointer<vtkPoints> Points_last = vtkSmartPointer<vtkPoints>::New();
		Points_orig->DeepCopy(Points);

		double* b = new double[Points->GetNumberOfPoints() * 3];
		double pi[3], qi[3], oi[3];
		const double alpha = 0.1, beta = 0.2;

		for (int iter = 0; iter < SmoothIterNum; iter++)
		{
			Points_last->DeepCopy(Points);
			for (vtkIdType pid = 0; pid < Points->GetNumberOfPoints(); pid++)
			{
				if (neighbor.at(pid).size() > 0)
				{
					for (int l = 0; l < 3; l++) pi[l] = 0.0;
					for (unsigned int j = 0; j < neighbor.at(pid).size(); j++)
					{
						Points_last->GetPoint(neighbor.at(pid).at(j), qi);
						for (int l = 0; l < 3; l++) pi[l] += qi[l];
					}
					for (int l = 0; l < 3; l++) pi[l] = pi[l] / neighbor.at(pid).size();
					Points->SetPoint(pid, pi);
					Points_orig->GetPoint(pid, oi);
					Points_last->GetPoint(pid, qi);

					for (int l = 0; l < 3; l++)	b[pid * 3 + l] = pi[l] - (alpha * oi[l] + (1.0 - alpha) * qi[l]);
				}
			}
			for (vtkIdType pid = 0; pid < Points->GetNumberOfPoints(); pid++)
			{
				if (neighbor.at(pid).size() > 0)
				{
					double sumbj[3] = { 0.0, 0.0, 0.0 };

					for (unsigned int j = 0; j < neighbor.at(pid).size(); j++)
					{
						for (int l = 0; l < 3; l++)
							sumbj[l] += b[neighbor.at(pid).at(j) * 3 + l];
						//sumbj[0] += b[adjcent[pid * 10 + j] * 3 + 0];
					}
					for (int l = 0; l < 3; l++)	sumbj[l] = sumbj[l] / neighbor.at(pid).size();
					Points->GetPoint(pid, pi);

					for (int l = 0; l < 3; l++) pi[l] = pi[l] - (beta * b[pid * 3 + l] + (1.0 - beta) * sumbj[l]);
					Points->SetPoint(pid, pi);
				}
			}
		}
		delete[] b;
	}
	Points->Modified();
	Poly->Modified();
//	Poly->SetPoints(Points);
	return 1;
}



