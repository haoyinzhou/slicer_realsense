project(vtkSlicer${MODULE_NAME}ModuleLogic)

list( APPEND CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake_modules )
find_package( G2O REQUIRED )
find_package( Cholmod )
find_package( Eigen3 REQUIRED )
find_package(OpenCV REQUIRED)
#find_package(SlicerOpenCV REQUIRED)


set(KIT ${PROJECT_NAME})

set(${KIT}_EXPORT_DIRECTIVE "VTK_SLICER_${MODULE_NAME_UPPER}_MODULE_LOGIC_EXPORT")


set(${KIT}_INCLUDE_DIRECTORIES
  ${ADDITIONAL_INCLUDE_DIRS}
  ${qSlicerMarkupsModuleWidgets_SOURCE_DIR}
  ${qSlicerMarkupsModuleWidgets_BINARY_DIR}
  )

set(${KIT}_SRCS
  vtkSlicer${MODULE_NAME}Logic.cxx
  vtkSlicer${MODULE_NAME}Logic.h
  DataStructure.h 
  DataStructure.cxx 
  InitialMemory.h 
  InitialMemory.cxx 
  common.h 
  common.cxx 
  StereoVision.h 
  StereoVision.cxx 
  G2O_BA.h 
  G2O_BA.cxx 
  RigidMosaicking_CameraMotionEstimation.h 
  RigidMosaicking_CameraMotionEstimation.cxx 
  RigidMosaicking_PointsManagement.h 
  RigidMosaicking_PointsManagement.cxx 
  #RigidMosaicking_ReprojectExistingPoints.h 
  #RigidMosaicking_ReprojectExistingPoints.cxx 
  R1PPnP.h 
  R1PPnP.cxx 
  DualQ.h
  FinalRefinement.h 
  FinalRefinement.cxx 
#  ICP.h 
#  ICP.cxx
  MeshGeneration.h
  MeshGeneration.cxx
  RecordVideo.h
  RecordVideo.cxx
  )

include_directories(include ${realsense2_INCLUDE_DIR})
include_directories(include ${OpenCV_INCLUDE_DIR})
include_directories(${EIGEN3_INCLUDE_DIR}  ${CHOLMOD_INCLUDE_DIR})

MESSAGE(STATUS "EIGEN3_INCLUDE_DIR: ${EIGEN3_INCLUDE_DIR}")


LIST(APPEND G2O_LIBS
  g2o_cli g2o_ext_freeglut_minimal g2o_simulator
  g2o_solver_slam2d_linear g2o_types_icp g2o_types_slam2d
  g2o_core g2o_interface g2o_solver_csparse g2o_solver_structure_only
  g2o_types_sba g2o_types_slam3d g2o_csparse_extension
  g2o_opengl_helper g2o_solver_dense g2o_stuff
  g2o_types_sclam2d g2o_parser g2o_solver_pcg
  g2o_types_data g2o_types_sim3
)
MESSAGE(STATUS "G2O_LIBS: ${G2O_LIBS}")

set(${KIT}_TARGET_LIBRARIES
  ${ITK_LIBRARIES}
  ${realsense2_LIBRARY}
  ${OpenCV_LIBS}
  ${G2O_LIBS}
  ${CHOLMOD_LIBRARIES}
  qSlicerMarkupsModuleWidgets
  )

find_package(VTK REQUIRED)
include(${VTK_USE_FILE})


#-----------------------------------------------------------------------------
SlicerMacroBuildModuleLogic(
  NAME ${KIT}
  EXPORT_DIRECTIVE ${${KIT}_EXPORT_DIRECTIVE}
  INCLUDE_DIRECTORIES ${${KIT}_INCLUDE_DIRECTORIES}
  SRCS ${${KIT}_SRCS}
  TARGET_LIBRARIES ${${KIT}_TARGET_LIBRARIES}
  )
