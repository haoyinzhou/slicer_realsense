#include "RecordVideo.h"

using namespace std;
using namespace cv;

#define fps_record 15

bool CRGBDVideoWriter::Initial(const char* filename_rgb, const char* filename_d, int W, int H)
{
	this->RGBWriter.open(filename_rgb, CV_FOURCC('M','P','E','G'), fps_record, cv::Size(W, H));
    this->DepthWriter.open(filename_d, CV_FOURCC('M','P','E','G'), fps_record, cv::Size(W, H));
                
  	if (!RGBWriter.isOpened() || !DepthWriter.isOpened())
    {
        std::cout  << "Could not open the output video for write!" << std::endl;
        return false;
    }
    return true;
}

void CRGBDVideoWriter::Release()
{
	this->RGBWriter.release();
    this->DepthWriter.release();
}

void CRGBDVideoWriter::WriteRGB(cv::Mat* rgbframe)
{
	this->RGBWriter << (*rgbframe);
}

#define MaxPhysicalDepth 1500.0
#define HalfPhysicalDepth (0.5 * MaxPhysicalDepth)

void CRGBDVideoWriter::WriteDepth(float* depth, int W, int H)
{
	cv::Mat depthmat = cv::Mat::zeros(H, W, CV_8UC3);
	uchar* pointer = (uchar*)(depthmat.data);
	
	for (int y = 0; y < H; y ++)
		for (int x = 0; x < W; x ++)
	{
		int yWx = y * W + x;
		float d = depth[yWx];
		
		if (d > MaxPhysicalDepth) d = 0.0;
		if (d < 0.0) d = 0.0;

		float ratio = 255.0 / HalfPhysicalDepth;

		uchar r = 0;		
		uchar g = 0;		
		uchar b = 0;
		
		if (d < HalfPhysicalDepth)
		{
			r = 0;
			g = (uchar)(ratio * (d - 0.0));
			b = (uchar)(ratio * (HalfPhysicalDepth - d));
		}
		else
		{
			r = (uchar)(ratio * (d - HalfPhysicalDepth));
			g = (uchar)(ratio * (MaxPhysicalDepth - d));
			b = 0;
		}
		
		// debug only
		{
			if (y == 240 && x == 320)
			{
				double d_recovered = 0.0;
				if (r == 0)
					d_recovered = (float)g / ratio;
				else
					d_recovered = MaxPhysicalDepth - (float)g / ratio; 
			
				std::cout << "d = " << d << "rgb = " << (int)r << ", " << (int)g << ", " << (int)b << ", d_recovered = " << d_recovered << std::endl;
			}
		}
	
		pointer[3 * yWx + 0] = r;
		pointer[3 * yWx + 1] = g;
		pointer[3 * yWx + 2] = b;
	}
	this->DepthWriter << depthmat;
}





















