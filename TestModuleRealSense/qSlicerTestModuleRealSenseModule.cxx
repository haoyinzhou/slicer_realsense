/*==============================================================================

  Program: 3D Slicer

  Portions (c) Copyright Brigham and Women's Hospital (BWH) All Rights Reserved.

  See COPYRIGHT.txt
  or http://www.slicer.org/copyright/copyright.txt for details.

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

==============================================================================*/

// TestModuleRealSense Logic includes
#include <vtkSlicerTestModuleRealSenseLogic.h>

// TestModuleRealSense includes
#include "qSlicerTestModuleRealSenseModule.h"
#include "qSlicerTestModuleRealSenseModuleWidget.h"

//-----------------------------------------------------------------------------
/// \ingroup Slicer_QtModules_ExtensionTemplate
class qSlicerTestModuleRealSenseModulePrivate
{
public:
  qSlicerTestModuleRealSenseModulePrivate();
};

//-----------------------------------------------------------------------------
// qSlicerTestModuleRealSenseModulePrivate methods

//-----------------------------------------------------------------------------
qSlicerTestModuleRealSenseModulePrivate::qSlicerTestModuleRealSenseModulePrivate()
{
}

//-----------------------------------------------------------------------------
// qSlicerTestModuleRealSenseModule methods

//-----------------------------------------------------------------------------
qSlicerTestModuleRealSenseModule::qSlicerTestModuleRealSenseModule(QObject* _parent)
  : Superclass(_parent)
  , d_ptr(new qSlicerTestModuleRealSenseModulePrivate)
{
}

//-----------------------------------------------------------------------------
qSlicerTestModuleRealSenseModule::~qSlicerTestModuleRealSenseModule()
{
}

//-----------------------------------------------------------------------------
QString qSlicerTestModuleRealSenseModule::helpText() const
{
  return "This is used for breast reconstructive surgery.";
}

//-----------------------------------------------------------------------------
QString qSlicerTestModuleRealSenseModule::acknowledgementText() const
{
  return "This work was partially funded by NIH grant R01";
}

//-----------------------------------------------------------------------------
QStringList qSlicerTestModuleRealSenseModule::contributors() const
{
  QStringList moduleContributors;
  moduleContributors << QString("Haoyin Zhou, Angela Alarcon, Jayender Jaadeesan (jayender@bwh.harvard.edu)");
  return moduleContributors;
}

//-----------------------------------------------------------------------------
QIcon qSlicerTestModuleRealSenseModule::icon() const
{
  return QIcon(":/Icons/TestModuleRealSense.png");
}

//-----------------------------------------------------------------------------
QStringList qSlicerTestModuleRealSenseModule::categories() const
{
  return QStringList() << "IGT";
}

//-----------------------------------------------------------------------------
QStringList qSlicerTestModuleRealSenseModule::dependencies() const
{
  return QStringList();
}

//-----------------------------------------------------------------------------
void qSlicerTestModuleRealSenseModule::setup()
{
  this->Superclass::setup();
}

//-----------------------------------------------------------------------------
qSlicerAbstractModuleRepresentation* qSlicerTestModuleRealSenseModule
::createWidgetRepresentation()
{
  return new qSlicerTestModuleRealSenseModuleWidget;
}

//-----------------------------------------------------------------------------
vtkMRMLAbstractLogic* qSlicerTestModuleRealSenseModule::createLogic()
{
  return vtkSlicerTestModuleRealSenseLogic::New();
}

/*void qSlicerTestModuleRealSenseModule::liveButton()
{
    std::cout<<"live button pressed" << std::endl;
}
*/
void qSlicerTestModuleRealSenseModuleWidget::loadButton()
{
    std::cout << "load Button pressed" << std::endl;
}
