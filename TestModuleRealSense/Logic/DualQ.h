#ifndef DUALQ_H
#define DUALQ_H


#define Qmult(q0, r0, s0) \
{ \
	double* q = q0; \
	double* r = r0; \
	double* s = s0; \
	s[0] = q[0] * r[0] - q[1] * r[1] - q[2] * r[2] - q[3] * r[3]; \
	s[1] = r[0] * q[1] + q[0] * r[1] - (q[2] * r[3] - q[3] * r[2]); \
	s[2] = r[0] * q[2] + q[0] * r[2] - (q[3] * r[1] - q[1] * r[3]); \
	s[3] = r[0] * q[3] + q[0] * r[3] - (q[1] * r[2] - q[2] * r[1]); \
} \

#define DQmult(dq, dr, ds) \
{ \
		double* dqq = dq; \
		double* drr = dr; \
		double* dss = ds; \
		Qmult(dqq, drr, dss); \
		double ds21[4], ds22[4]; \
		Qmult(dqq, drr + 4, ds21); \
		Qmult(dqq + 4, drr, ds22); \
		for (int l = 0; l < 4; l++) ds[l + 4] = ds21[l] + ds22[l]; \
} \


#define DQmult_W_Pos(dq, dr, ds) \
{ \
		double* dqq = dq; \
		double* drr = dr; \
		for (int l = 0; l < 4; l++) ds[l] = dqq[l]; \
		double ds21[4]; \
		Qmult(dqq, drr + 4, ds21); \
		for (int l = 0; l < 4; l++) ds[l + 4] = ds21[l] + dqq[l + 4]; \
} \

#define DQmult_T_R(dq, dr, ds) \
{ \
		double* dqq = dq; \
		double* drr = dr; \
		for (int l = 0; l < 4; l++) ds[l] = drr[l]; \
		double ds22[4]; \
		Qmult(dqq + 4, drr, ds22); \
		for (int l = 0; l < 4; l++) ds[l + 4] = ds22[l]; \
} \

#define pos2dquat(pos, DQ) \
{ \
	DQ[0] = 1.0; \
	for (int l = 1; l < 5; l++)	DQ[l] = 0.0; \
	for (int l = 5; l < 8; l++)	DQ[l] = pos[l - 5]; \
} \


#define trans2dquat(T, DQ) \
{ \
	DQ[0] = 1.0; \
	for (int l = 1; l < 5; l++)	DQ[l] = 0.0; \
	for (int l = 5; l < 8; l++)	DQ[l] = 0.5 * T[l - 5]; \
} \

#define angle2dquat(angle, DQ) \
{ \
	double t0 = cos(angle[2] * 0.5);\
	double t1 = sin(angle[2] * 0.5);\
	double t2 = cos(angle[1] * 0.5);\
	double t3 = sin(angle[1] * 0.5);\
	double t4 = cos(angle[0] * 0.5);\
	double t5 = sin(angle[0] * 0.5);\
	DQ[0] = t0 * t2 * t4 + t1 * t3 * t5;\
	DQ[1] = t0 * t3 * t4 - t1 * t2 * t5;\
	DQ[2] = t0 * t2 * t5 + t1 * t3 * t4;\
	DQ[3] = t1 * t2 * t4 - t0 * t3 * t5;\
	for (int l = 4; l < 8; l++) DQ[l] = 0.0;\
}\

#define NormlizeDQ(DQ) \
{ \
	double s = DQ[0] * DQ[0] + DQ[1] * DQ[1] + DQ[2] * DQ[2] + DQ[3] * DQ[3]; \
	s = sqrt(s); \
	s = 1.0 / s; \
	for (int l = 0; l < 8; l++)	DQ[l] = s * DQ[l]; \
} \

#define DQnorm(DQ, normDQ) \
{ \
	double* dqq = DQ; \
	normDQ[0] = sqrt(dqq[0] * dqq[0] + dqq[1] * dqq[1] + dqq[2] * dqq[2] + dqq[3] * dqq[3]); \
	double q0_conj[4] = {dqq[0], -dqq[1], -dqq[2], -dqq[3]}; \
	double q1_conj[4] = {dqq[4], -dqq[5], -dqq[6], -dqq[7]}; \
	double temp1[4]; \
	Qmult(q0_conj, dqq + 4, temp1); \
	double temp2[4]; \
	Qmult(q1_conj, dqq, temp2); \
	normDQ[1] = (temp1[0] + temp2[0]) / (2 * normDQ[0]); \
} \


#define Ddiv(dn0, dn1, dn) \
{ \
	dn[0] = dn0[0] / dn1[0]; \
	dn[1] = dn0[1] / dn1[0] - dn0[0] * dn1[1] / (dn1[0] * dn1[0]); \
} \


#define DQinv(DQ, DQ_inv) \
{\
	for (int l = 0; l < 8; l ++) DQ_inv[l] = 0.0;\
	double normdq[2]; \
	DQnorm(DQ, normdq); \
	double normdq_2[2]; \
	normdq_2[0] = normdq[0] * normdq[0]; \
	normdq_2[1] = normdq[0] * normdq[1] + normdq[1] * normdq[0]; \
	double dq_conj[8] = {DQ[0], -DQ[1], -DQ[2], -DQ[3], DQ[4], -DQ[5], -DQ[6], -DQ[7]};\
	for (int i = 0; i < 8; i ++)\
	{\
		if (i < 4) \
		{ \
			double res[2]; \
			double temp[2] = {dq_conj[i], 0.0};\
			Ddiv(temp, normdq_2, res); \
			DQ_inv[i] += res[0];\
			DQ_inv[i + 4] += res[1]; \
		} \
		else \
		{ \
			double res[2];\
			double temp[2] = {0.0, dq_conj[i]};\
			Ddiv(temp, normdq_2, res); \
			DQ_inv[i] += res[1];\
			DQ_inv[i - 4] += res[0]; \
		} \
	}\
}\


#define RTC2nodedq(R, T, C, nodedq) \
{ \
	double QminusC[8] = {1.0, 0.0, 0.0, 0.0, 0.0, -0.5 * C[0], -0.5 * C[1], -0.5 * C[2]}; \
	double QR[8]; \
	angle2dquat(R, QR); \
	double TC[3]; \
	for (int l = 0; l < 3; l++) TC[l] = T[l] + C[l]; \
	double QTC[8] = {1.0, 0.0, 0.0, 0.0, 0.0, 0.5 * TC[0], 0.5 * TC[1], 0.5 * TC[2]};	\
	double warp_temp[8]; \
	DQmult_T_R(QminusC, QR, warp_temp); \
	DQmult_W_Pos(warp_temp, QTC, nodedq); \
}\


#define NodeEffects2DeformedPointCoord(nodeeffects, pointorigcoord, deformedpointcoord) \
{ \
	double nodeseffect_conj[8] = {nodeeffects[0], -nodeeffects[1], -nodeeffects[2], -nodeeffects[3], -nodeeffects[4], nodeeffects[5], nodeeffects[6], nodeeffects[7]}; \
	double pointcoorddq[8] = {1.0, 0.0, 0.0, 0.0, 0.0, pointorigcoord[0], pointorigcoord[1], pointorigcoord[2]}; \
	double Qtemp[8], Q[8]; \
	DQmult(nodeseffect_conj, pointcoorddq, Qtemp); \
	DQmult(Qtemp, nodeeffects, Q); \
	NormlizeDQ(Q); \
	for (int l = 0; l < 3; l ++) deformedpointcoord[l] = Q[l + 5]; \
} \


#define NodeEffectsInv_LiveCoord2OrigCoord(nodeeffects, livecoord, origcoord) \
{\
	double nodeseffectlocal[8];\
	for (int l = 0; l < 8; l ++) nodeseffectlocal[l] = nodeeffects[l];\
	double nodeseffectlocal_inv[8];\
	DQinv(nodeseffectlocal, nodeseffectlocal_inv);\
	for (int l = 1; l < 5; l ++ ) nodeseffectlocal[l] = -nodeseffectlocal[l];\
	double nodeseffect_conj_inv[8];\
	DQinv(nodeseffectlocal, nodeseffect_conj_inv);\
	double dq_live[8] = {1.0, 0.0, 0.0, 0.0, 0.0, livecoord[0], livecoord[1], livecoord[2]};\
	double dq[8];\
	DQmult(dq_live, nodeseffectlocal_inv, nodeseffectlocal);\
	DQmult(nodeseffect_conj_inv, nodeseffectlocal, dq);\
	NormlizeDQ(dq);\
	for (int l = 0; l < 3; l++) origcoord[l] = dq[l + 5];\
}\


#define RotateDir(R, inputdir, outputdir) \
{\
	double QR[8];\
	angle2dquat(R, QR);\
	double QR_conj[8] =  {QR[0], -QR[1], -QR[2], -QR[3], -QR[4], QR[5], QR[6], QR[7]};\
	double inputdirDQ[8] = {1.0, 0.0, 0.0, 0.0, 0.0, inputdir[0], inputdir[1], inputdir[2]};\
	double Qtemp[8], Q[8];\
	DQmult(QR_conj, inputdirDQ, Qtemp);\
	DQmult(Qtemp, QR, Q);\
	NormlizeDQ(Q);\
	for (int l = 0; l < 3; l ++) outputdir[l] = Q[l + 5];\
}\


#define CrossProduct(u1, u2, u3, v1, v2, v3, out1, out2, out3) \
{ \
	out1 = u2 * v3 - v2 * u3; \
	out2 = v1 * u3 - u1 * v3; \
	out3 = u1 * v2 - v1 * u2; \
} \

#define DCM2Angle(dcm, angle)\
{\
	angle[0] = asin(-dcm[2]);\
	angle[1] = atan2(dcm[5], dcm[8]);\
	angle[2] = atan2(dcm[1], dcm[0]);\
}\


#define Angle2DCM(angle, dcm)\
{\
	double sx = sin(angle[2]);\
	double sy = sin(angle[0]);\
	double sz = sin(angle[1]);\
	double cx = cos(angle[2]);\
	double cy = cos(angle[0]);\
	double cz = cos(angle[1]);\
	dcm[0] = cy*cx;\
	dcm[1] = cy*sx;\
	dcm[2] = -sy;\
	dcm[3] = sz*sy*cx - cz*sx;\
	dcm[4] = sz*sy*sx + cz*cx;\
	dcm[5] = sz*cy;\
	dcm[6] = cz*sy*cx + sz*sx;\
	dcm[7] = cz*sy*sx - sz*cx;\
	dcm[8] = cz*cy;\
}\


#define DCMT_OrigCoord_2_LiveCoord(dcm, T, origcoord, livecoord)\
{\
	double dcm_origcoord[3];\
	dcm_origcoord[0] = dcm[0] * origcoord[0] + dcm[1] * origcoord[1] + dcm[2] * origcoord[2];\
	dcm_origcoord[1] = dcm[3] * origcoord[0] + dcm[4] * origcoord[1] + dcm[5] * origcoord[2];\
	dcm_origcoord[2] = dcm[6] * origcoord[0] + dcm[7] * origcoord[1] + dcm[8] * origcoord[2];\
	for (int l = 0; l < 3; l ++) livecoord[l] = dcm_origcoord[l] + T[l];\
}\


#define DCMT_LiveCoord_2_OrigCoord(dcm, T, livecoord, origcoord)\
{\
	double livecoord_T[3];\
	for (int l = 0; l < 3; l ++) livecoord_T[l] = livecoord[l] - T[l];\
	origcoord[0] = dcm[0] * livecoord_T[0] + dcm[3] * livecoord_T[1] + dcm[6] * livecoord_T[2];\
	origcoord[1] = dcm[1] * livecoord_T[0] + dcm[4] * livecoord_T[1] + dcm[7] * livecoord_T[2];\
	origcoord[2] = dcm[2] * livecoord_T[0] + dcm[5] * livecoord_T[1] + dcm[8] * livecoord_T[2];\
}\

#define MultiplyMatrix3x3(dcm1, dcm2, dcm_out)\
{\
	dcm_out[0] = dcm1[0] * dcm2[0] + dcm1[1] * dcm2[3] + dcm1[2] * dcm2[6];\
	dcm_out[1] = dcm1[0] * dcm2[1] + dcm1[1] * dcm2[4] + dcm1[2] * dcm2[7];\
	dcm_out[2] = dcm1[0] * dcm2[2] + dcm1[1] * dcm2[5] + dcm1[2] * dcm2[8];\
	dcm_out[3] = dcm1[3] * dcm2[0] + dcm1[4] * dcm2[3] + dcm1[5] * dcm2[6];\
	dcm_out[4] = dcm1[3] * dcm2[1] + dcm1[4] * dcm2[4] + dcm1[5] * dcm2[7];\
	dcm_out[5] = dcm1[3] * dcm2[2] + dcm1[4] * dcm2[5] + dcm1[5] * dcm2[8];\
	dcm_out[6] = dcm1[6] * dcm2[0] + dcm1[7] * dcm2[3] + dcm1[8] * dcm2[6];\
	dcm_out[7] = dcm1[6] * dcm2[1] + dcm1[7] * dcm2[4] + dcm1[8] * dcm2[7];\
	dcm_out[8] = dcm1[6] * dcm2[2] + dcm1[7] * dcm2[5] + dcm1[8] * dcm2[8];\
}\

#define MultiplyMatrixAndVector3(dcm, t, dcmt)\
{\
	dcmt[0] = dcm[0] * t[0] + dcm[1] * t[1] + dcm[2] * t[2];\
	dcmt[1] = dcm[3] * t[0] + dcm[4] * t[1] + dcm[5] * t[2];\
	dcmt[2] = dcm[6] * t[0] + dcm[7] * t[1] + dcm[8] * t[2];\
}\


#endif
