#ifndef COMMON_H
#define COMMON_H


#include "DataStructure.h"
//#include "Visualization.h"
//#include "InitialMemory.h"
//#include "StereoVision.h"
//#include "RigidMosaicking_CameraMotionEstimation.h"

#include <vtkSmartPointer.h>
#include <vtkPolyData.h>
#include <vtkPoints.h>
#include <vtkCellArray.h>
#include <vtkIntArray.h>
#include <vtkUnsignedCharArray.h>
#include <vtkAbstractArray.h>
#include <vtkPointData.h>
#include <vtkXMLPolyDataWriter.h>
#include <vtkXMLPolyDataReader.h>

#include <string>
#include <stdio.h>


bool ReadPolyData(vtkPolyData *poly, const char* fileName);
void SavePolyData(vtkPolyData *poly, const char* fileName);


void PrintDCMandT(double* dcm, double* T);



#endif
