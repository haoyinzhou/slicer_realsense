#include "R1PPnP.h"

using namespace std;

struct Ccoord3
{
	double coord[3];
};

struct Ccoord2
{
	double coord[2];
};


void Initial_DCM_T_PnP(double dcm[9], double T[3])
{
	memset(dcm, 0, 9 * sizeof(double));
	for (int l = 0; l < 3; l ++) dcm[3 * l + l] = 1.0;
	memset(T, 0, 3 * sizeof(double));						
}


void R33xCoord3(double R[3][3], double coord[3], double out[3])
{
	out[0] = R[0][0] * coord[0] + R[0][1] * coord[1] + R[0][2] * coord[2];
	out[1] = R[1][0] * coord[0] + R[1][1] * coord[1] + R[1][2] * coord[2];
	out[2] = R[2][0] * coord[0] + R[2][1] * coord[1] + R[2][2] * coord[2];
}



double R1PPnP(CTrackingFrame* TrackingFrame_this
			, CTrackingFrame* TrackingFrame_prev
			, double dcm_out[9], double T_out[3]
			, unsigned int& NumofGoodMatches, double& GoodMatchesRate
			, double inlierErrorPixelTH
			, double FocalLength, double ImgCenterU, double ImgCenterV, unsigned int MaxNumberOfFeaturePointsPerFrame)
{
	std::vector<int> ratedidx;
	std::vector<Ccoord3> Xw;
	std::vector<Ccoord3> uv;
	
	unsigned int TotalMatchingCount = 0;
	{
		std::vector<float> dis2imgcenter;
		for (size_t i = 0; i < TrackingFrame_this->ORBkeypoints.size(); i ++)
		{
			if (i >= MaxNumberOfFeaturePointsPerFrame) break;
			int MatchedID = TrackingFrame_this->MatchedID[i];
			if (MatchedID < 0) continue;
	
			float disx = TrackingFrame_this->ORBkeypoints.at(i).pt.x - ImgCenterU;
			float disy = TrackingFrame_this->ORBkeypoints.at(i).pt.y - ImgCenterV;
			dis2imgcenter.push_back(sqrt(disx * disx + disy * disy));
			
		//	matcheddistance.push_back((float)(FeaturePoints_live[i].MatchedDistance));
			
			Ccoord3 imgcoord3;
			imgcoord3.coord[0] = ((double)(TrackingFrame_this->ORBkeypoints.at(i).pt.x) - ImgCenterU) / FocalLength;
			imgcoord3.coord[1] = ((double)(TrackingFrame_this->ORBkeypoints.at(i).pt.y) - ImgCenterV) / FocalLength;
			imgcoord3.coord[2] = 1.0;
			uv.push_back(imgcoord3);
			
			Ccoord3 Phycoord3;
			for (int l = 0; l < 3; l ++) Phycoord3.coord[l] = TrackingFrame_prev->coord[3 * MatchedID + l];
			Xw.push_back(Phycoord3);
			
			TotalMatchingCount ++;
		}		
		
		if (TotalMatchingCount < 8)
		{
			NumofGoodMatches = 0;
			GoodMatchesRate = 0.0;
			
			for (int l = 0; l < 3; l ++) dcm_out[3 * l + l] = 1.0;			
			printf("R1PPnP failed because TotalMatchingCount = %d!!!!!!!!!\n", TotalMatchingCount);
			return 10000.0;
		}
				
		cv::sortIdx(dis2imgcenter, ratedidx, cv::SORT_ASCENDING);
	//	cv::sortIdx(matcheddistance, ratedidx, CV_SORT_ASCENDING);
	}
	
	unsigned int Count = TotalMatchingCount - 1;
//	double meanerror = 0.0;	

	int bestinliers_npts = 0;
	double punish = 1e+5;
	double bestsumerror = 10000.0;
	double BestR[3][3], BestT[3];
	memset(BestR, 0, 9 * sizeof(double));
	memset(BestT, 0, 3 * sizeof(double));
	
//	for (unsigned int tryidx = 0; tryidx < Count; tryidx ++)
	for (unsigned int tryidx = 0; tryidx < 100; tryidx ++) // only for debug
	{
		if (tryidx > ratedidx.size() - 1) break;
		unsigned int benchmarkid = ratedidx.at(tryidx);
	
		vector<Ccoord3> uv_getridofbench;
		vector<Ccoord3> Xw_getridofbench;
		vector<double> uv_getridofbench_norm2vector;

		for (unsigned int i = 0; i < TotalMatchingCount; i ++)
		{
			if (i == benchmarkid) continue;
				
			Ccoord3 Phycoord3;
			for (int l = 0; l < 3; l ++) Phycoord3.coord[l] = Xw[i].coord[l];
			Xw_getridofbench.push_back(Phycoord3);
						
			Ccoord3 imgcoord3;
			for (int l = 0; l < 3; l ++) imgcoord3.coord[l] = uv[i].coord[l];
			uv_getridofbench.push_back(imgcoord3);
			
			double norm2 = imgcoord3.coord[0] * imgcoord3.coord[0] + imgcoord3.coord[1] * imgcoord3.coord[1] + imgcoord3.coord[2] * imgcoord3.coord[2];
			norm2 = 1.0 / norm2;
			uv_getridofbench_norm2vector.push_back(norm2);		
			
		//	printf("benchmarkid = %d, Xw_getridofbench = %.4f %.4f %.4f, uv_getridofbench = %.4f %.4f %.4f, uv_getridofbench_norm2vector = %.4f\n", benchmarkid,  Phycoord3.coord[0], Phycoord3.coord[1], Phycoord3.coord[2], imgcoord3.coord[0], imgcoord3.coord[1], imgcoord3.coord[2], norm2);
		}

		vector<Ccoord3> S;
		for (unsigned int idx = 0; idx < Count; idx ++)
		{
			Ccoord3 Phycoord3;
			for (int l = 0; l < 3; l ++) Phycoord3.coord[l] = Xw_getridofbench[idx].coord[l] - Xw[benchmarkid].coord[l];
			S.push_back(Phycoord3);

		//	printf("S = %.4f %.4f %.4f\n", Phycoord3.coord[0], Phycoord3.coord[1], Phycoord3.coord[2]);
		}
		
		double mu = 0.001;
		
		double R[3][3];
		memset(R, 0.0, 9 * sizeof(double));
		for (int l = 0; l < 3; l ++) R[l][l] = 1.0;	
		
		vector<Ccoord3> p;
		vector<Ccoord3> q;
		vector<Ccoord3> wS_centered;
		vector<Ccoord3> wS_q;
		vector<Ccoord2> p_proj;
		vector<double> costvector;
		vector<double> weights;
		vector<double> lambda;

		p.resize(Count);
		q.resize(Count);
		wS_centered.resize(Count);
		wS_q.resize(Count);		
		p_proj.resize(Count);		
		costvector.resize(Count);		
		weights.resize(Count);
		lambda.resize(Count);
		
		int inliers_npts = 0;
		int inliers_npts_record[500];
		double R_last[3][3];
		int iter = 0;
		for (iter = 0; iter < 200; iter ++)
		{
			for (unsigned int idx = 0; idx < Count; idx ++)
			{
				R33xCoord3(R, S[idx].coord, p[idx].coord);
				for (int l = 0; l < 3; l ++) p[idx].coord[l] *= mu;
				for (int l = 0; l < 3; l ++) p[idx].coord[l] += uv[benchmarkid].coord[l];
				
				lambda[idx] = (uv_getridofbench[idx].coord[0] * p[idx].coord[0] + uv_getridofbench[idx].coord[1] * p[idx].coord[1] + uv_getridofbench[idx].coord[2] * p[idx].coord[2]);
				lambda[idx] *= uv_getridofbench_norm2vector[idx];
	
				for (int l = 0; l < 3; l ++) q[idx].coord[l] = lambda[idx] * uv_getridofbench[idx].coord[l];
	
				for (int l = 0; l < 2; l ++) p_proj[idx].coord[l] = p[idx].coord[l] / p[idx].coord[2];

				double projdiff[2];
				for (int l = 0; l < 2; l ++) projdiff[l] = p_proj[idx].coord[l] - uv_getridofbench[idx].coord[l];
				
				costvector[idx] = FocalLength * sqrt(projdiff[0] * projdiff[0] + projdiff[1] * projdiff[1]);
				if (costvector[idx] < 0.0) costvector[idx] = -costvector[idx];
			}	

			for (unsigned int idx = 0; idx < Count; idx ++)
			{
				weights[idx] = 	1.5 * inlierErrorPixelTH / costvector[idx];	
				if (weights[idx] > 1.0) weights[idx] = 1.0;
			
				for (int l = 0; l < 3; l ++) wS_centered[idx].coord[l] = weights[idx] * S[idx].coord[l];
				for (int l = 0; l < 3; l ++) wS_q[idx].coord[l] = weights[idx] * (q[idx].coord[l] - uv[benchmarkid].coord[l]);
			}

			double norm_ws_weighted = 0.0, norm_ws_q = 0.0;
			for (unsigned int idx = 0; idx < Count; idx ++)
			{
				for (int l = 0; l < 3; l ++) norm_ws_weighted += wS_centered[idx].coord[l] * wS_centered[idx].coord[l];
				for (int l = 0; l < 3; l ++) norm_ws_q += wS_q[idx].coord[l] * wS_q[idx].coord[l];
			}	
			norm_ws_weighted = sqrt(norm_ws_weighted);
			norm_ws_q = sqrt(norm_ws_q);
			
			mu = norm_ws_q / norm_ws_weighted;			
			
			double A[3][3];
			for (int y = 0; y < 3; y ++)
				for (int x = 0; x < 3; x ++)
				{
					A[y][x] = 0.0;			
					for (unsigned int idx = 0; idx < Count; idx ++)
						A[y][x] += wS_q[idx].coord[y] * wS_centered[idx].coord[x];
				}
			
			double U[3][3], sigma[3], VT[3][3];
			vtkMath::SingularValueDecomposition3x3(A, U, sigma, VT);

			for (int i = 0; i < 3; i ++)
			{
				if (sigma[i] < 0)
				{
					sigma[i] = -sigma[i];
					for (int l = 0; l < 3; l ++) U[l][i] = -U[l][i];
				}
			}
			
			vtkMath::Multiply3x3(U, VT, R);
			
			if ( vtkMath::Determinant3x3(R[0][0], R[0][1], R[0][2], R[1][0], R[1][1], R[1][2], R[2][0], R[2][1], R[2][2]) < 0.0 )
			{
				for (int l = 0; l < 3; l ++) R[2][l] = -R[2][l];
			}
			
			inliers_npts = 0;
			for (unsigned int idx = 0; idx < Count; idx ++)
			{
				if (costvector[idx] > inlierErrorPixelTH) continue;			
				inliers_npts ++;
			}
			
			// should break?
			if (iter > 30)
			{
				if (inliers_npts - inliers_npts_record[iter - 20] < 1) break;				
				double R_diff[9];
				for (int l = 0; l < 9; l ++) R_diff[l] =  R[l] - R_last[l];
				double R_diff_norm = 0.0;
				for (int l = 0; l < 9; l ++) R_diff_norm += R_diff[l] * R_diff[l];
				if (R_diff_norm < 1e-8) break;
			}
			
			inliers_npts_record[iter] = inliers_npts;
			for (int l1 = 0; l1 < 3; l1 ++)
				for (int l2 = 0; l2 < 3; l2 ++) R_last[l1][l2] = R[l1][l2];
		}
		
		double sumerror = 0.0;
		for (unsigned int idx = 0; idx < Count; idx ++)
		{
			if (costvector[idx] > inlierErrorPixelTH) continue;
			sumerror += costvector[idx];			
		}	
		
//		printf("benchmarkid = %d, iternum = %d, inliers_npts = %d, bestinliers_npts = %d, TotalMatchingCount = %d\n", benchmarkid, iter, inliers_npts, bestinliers_npts, TotalMatchingCount);
	
		if (inliers_npts == 0) continue;

		double fracinliers = 0.0;
		if (inliers_npts > bestinliers_npts)
		{
			bestinliers_npts = inliers_npts;
			bestsumerror = sumerror;

			for (int y = 0; y < 3; y ++)
				for (int x = 0; x < 3; x ++)
					BestR[y][x] = R[y][x];
		
			double T_raw[3] = {0.0, 0.0, 0.0};
			for (unsigned int idx = 0; idx < Count; idx ++)
			{
				if (costvector[idx] > inlierErrorPixelTH) continue;
			
				double realq[3];
				for (int l = 0; l < 3; l ++) realq[l] = 1.0 / mu * lambda[idx] * uv_getridofbench[idx].coord[l];
				
				//printf("idx = %d, realq = %.4f %.4f %.4f\n", idx, realq[0], realq[1], realq[2]);
				
				double RXw[3];
				R33xCoord3(BestR, Xw_getridofbench[idx].coord, RXw);
				//printf("idx = %d, RXw = %.4f %.4f %.4f\n", idx, RXw[0], RXw[1], RXw[2]);
				
				for (int l = 0; l < 3; l ++) T_raw[l] += (realq[l] - RXw[l]);
			}
		
			for (int l = 0; l < 3; l ++) BestT[l] = T_raw[l] / (double)bestinliers_npts;
			
		/*	printf("BestR = \n");
			for (int i = 0; i < 3; i ++)
			{
				for (int j = 0; j < 3; j ++)
					printf("%.4f, ", BestR[i][j]);
				printf("\n");
			}
			printf("BestT = %.4f %.4f %.4f\n", BestT[0], BestT[1], BestT[2]);
		*/	
			
			fracinliers = ((double)inliers_npts + 1.0) / (double)TotalMatchingCount;
			if (fracinliers < 1e-5) fracinliers = 1e-5;

		//	printf("fracinliers = %.4f\n", fracinliers);
			if (fracinliers > .55)
				punish = 0.0;
			else				
				punish = -4.6 / log(1.0 - fracinliers);
		}	

	//	printf("inliers_npts = %d, fracinliers = %lf, punish = %lf\n", inliers_npts, fracinliers, punish);

		if (punish < tryidx + 1.0)
		{
			for (int y = 0; y < 3; y ++)
				for (int x = 0; x < 3; x ++)
					dcm_out[3 * y + x] = BestR[y][x];
	
			for (int l = 0; l < 3; l ++) T_out[l] = BestT[l];
			
			NumofGoodMatches = bestinliers_npts;
			GoodMatchesRate = bestinliers_npts / (double)TotalMatchingCount;
			return bestsumerror / (double)bestinliers_npts;	
		}
	}
	
	for (int y = 0; y < 3; y ++)
		for (int x = 0; x < 3; x ++)
			dcm_out[3 * y + x] = BestR[y][x];
	
	for (int l = 0; l < 3; l ++) T_out[l] = BestT[l];
		
	NumofGoodMatches = bestinliers_npts;
	GoodMatchesRate = bestinliers_npts / (double)TotalMatchingCount;
	return bestsumerror / (double)bestinliers_npts;		
}



double R1PPnP(std::vector<cv::KeyPoint>* ORBkeypoints, std::vector<double>* coord3d, int* MatchedID
			, double dcm_out[9], double T_out[3]
			, unsigned int& NumofGoodMatches, double& GoodMatchesRate
			, double inlierErrorPixelTH
			, double FocalLength, double ImgCenterU, double ImgCenterV, unsigned int MaxNumberOfFeaturePointsPerFrame)
{
	std::vector<int> ratedidx;
	std::vector<Ccoord3> Xw;
	std::vector<Ccoord3> uv;
	
	unsigned int TotalMatchingCount = 0;
	{
		std::vector<float> dis2imgcenter;
		for (size_t i = 0; i < ORBkeypoints->size(); i ++)
		{
		//	printf("i = %d, MatchedID[i] = %d\n", i, MatchedID[i]);
		//	std::cout << "ORBkeypoints = " << ORBkeypoints->size() << ", coord3d = " << coord3d->size() << std::endl;
			if (MatchedID[i] < 0) continue;
			if (MatchedID[i] >= coord3d->size() / 3) continue;
			
			float disx = ORBkeypoints->at(i).pt.x - ImgCenterU;
			float disy = ORBkeypoints->at(i).pt.y - ImgCenterV;
			dis2imgcenter.push_back(sqrt(disx * disx + disy * disy));
			
		//	matcheddistance.push_back((float)(FeaturePoints_live[i].MatchedDistance));
			
			Ccoord3 imgcoord3;
			imgcoord3.coord[0] = ((double)(ORBkeypoints->at(i).pt.x - ImgCenterU)) / FocalLength;
			imgcoord3.coord[1] = ((double)(ORBkeypoints->at(i).pt.y - ImgCenterV)) / FocalLength;
			imgcoord3.coord[2] = 1.0;
			uv.push_back(imgcoord3);
			
			Ccoord3 Phycoord3;
			for (int l = 0; l < 3; l ++) Phycoord3.coord[l] = coord3d->at(3 * MatchedID[i] + l);
			Xw.push_back(Phycoord3);
			
			TotalMatchingCount ++;
		}
		if (TotalMatchingCount < 8)
		{
			NumofGoodMatches = 0;
			GoodMatchesRate = 0.0;
		
			for (int l = 0; l < 3; l ++) dcm_out[3 * l + l] = 1.0;			
			std::cout << "R1PPnP failed because TotalMatchingCount = " << TotalMatchingCount << std::endl;
			return 10000.0;
		}
		cv::sortIdx(dis2imgcenter, ratedidx, cv::SORT_ASCENDING);
	}	
	

	
	unsigned int Count = TotalMatchingCount - 1;
//	double meanerror = 0.0;	

//	std::cout << "TotalMatchingCount = " << TotalMatchingCount << ", uv = " << uv.size() << ", Xw = " << Xw.size() << std::endl;
	
	int bestinliers_npts = 0;
	double punish = 1e+5;
	double bestsumerror = 10000.0;
	double BestR[3][3], BestT[3];
	memset(BestR, 0, 9 * sizeof(double));
	memset(BestT, 0, 3 * sizeof(double));
	
//	for (unsigned int tryidx = 0; tryidx < Count; tryidx ++)
	for (unsigned int tryidx = 0; tryidx < 36; tryidx ++) // only for debug
	{
		if (tryidx > ratedidx.size() - 1) break;
		unsigned int benchmarkid = ratedidx.at(tryidx);
	
		vector<Ccoord3> uv_getridofbench;
		vector<Ccoord3> Xw_getridofbench;
		vector<double> uv_getridofbench_norm2vector;

		for (unsigned int i = 0; i < TotalMatchingCount; i ++)
		{
			if (i == benchmarkid) continue;
				
			Ccoord3 Phycoord3;
			for (int l = 0; l < 3; l ++) Phycoord3.coord[l] = Xw[i].coord[l];
			Xw_getridofbench.push_back(Phycoord3);
						
			Ccoord3 imgcoord3;
			for (int l = 0; l < 3; l ++) imgcoord3.coord[l] = uv[i].coord[l];
			uv_getridofbench.push_back(imgcoord3);
			
			double norm2 = imgcoord3.coord[0] * imgcoord3.coord[0] + imgcoord3.coord[1] * imgcoord3.coord[1] + imgcoord3.coord[2] * imgcoord3.coord[2];
			norm2 = 1.0 / norm2;
			uv_getridofbench_norm2vector.push_back(norm2);		
			
		//	printf("benchmarkid = %d, Xw_getridofbench = %.4f %.4f %.4f, uv_getridofbench = %.4f %.4f %.4f, uv_getridofbench_norm2vector = %.4f\n", benchmarkid,  Phycoord3.coord[0], Phycoord3.coord[1], Phycoord3.coord[2], imgcoord3.coord[0], imgcoord3.coord[1], imgcoord3.coord[2], norm2);
		}

		vector<Ccoord3> S;
		for (unsigned int idx = 0; idx < Count; idx ++)
		{
			Ccoord3 Phycoord3;
			for (int l = 0; l < 3; l ++) Phycoord3.coord[l] = Xw_getridofbench[idx].coord[l] - Xw[benchmarkid].coord[l];
			S.push_back(Phycoord3);

		//	printf("S = %.4f %.4f %.4f\n", Phycoord3.coord[0], Phycoord3.coord[1], Phycoord3.coord[2]);
		}
		
		double mu = 0.001;
		
		double R[3][3];
		memset(R, 0.0, 9 * sizeof(double));
		for (int l = 0; l < 3; l ++) R[l][l] = 1.0;	
		
		vector<Ccoord3> p;
		vector<Ccoord3> q;
		vector<Ccoord3> wS_centered;
		vector<Ccoord3> wS_q;
		vector<Ccoord2> p_proj;
		vector<double> costvector;
		vector<double> weights;
		vector<double> lambda;

		p.resize(Count);
		q.resize(Count);
		wS_centered.resize(Count);
		wS_q.resize(Count);		
		p_proj.resize(Count);		
		costvector.resize(Count);		
		weights.resize(Count);
		lambda.resize(Count);
		
		int inliers_npts = 0;
		int inliers_npts_record[500];
		double R_last[3][3];
		int iter = 0;
		for (iter = 0; iter < 500; iter ++)
		{
			for (unsigned int idx = 0; idx < Count; idx ++)
			{
				R33xCoord3(R, S[idx].coord, p[idx].coord);
				for (int l = 0; l < 3; l ++) p[idx].coord[l] *= mu;
				for (int l = 0; l < 3; l ++) p[idx].coord[l] += uv[benchmarkid].coord[l];
				
				lambda[idx] = (uv_getridofbench[idx].coord[0] * p[idx].coord[0] + uv_getridofbench[idx].coord[1] * p[idx].coord[1] + uv_getridofbench[idx].coord[2] * p[idx].coord[2]);
				lambda[idx] *= uv_getridofbench_norm2vector[idx];
	
				for (int l = 0; l < 3; l ++) q[idx].coord[l] = lambda[idx] * uv_getridofbench[idx].coord[l];
	
				for (int l = 0; l < 2; l ++) p_proj[idx].coord[l] = p[idx].coord[l] / p[idx].coord[2];

				double projdiff[2];
				for (int l = 0; l < 2; l ++) projdiff[l] = p_proj[idx].coord[l] - uv_getridofbench[idx].coord[l];
				
				costvector[idx] = FocalLength * sqrt(projdiff[0] * projdiff[0] + projdiff[1] * projdiff[1]);
				if (costvector[idx] < 0.0) costvector[idx] = -costvector[idx];
			}	

			for (unsigned int idx = 0; idx < Count; idx ++)
			{
				weights[idx] = 	1.5 * inlierErrorPixelTH / costvector[idx];	
				if (weights[idx] > 1.0) weights[idx] = 1.0;
			
				for (int l = 0; l < 3; l ++) wS_centered[idx].coord[l] = weights[idx] * S[idx].coord[l];
				for (int l = 0; l < 3; l ++) wS_q[idx].coord[l] = weights[idx] * (q[idx].coord[l] - uv[benchmarkid].coord[l]);
			}

			double norm_ws_weighted = 0.0, norm_ws_q = 0.0;
			for (unsigned int idx = 0; idx < Count; idx ++)
			{
				for (int l = 0; l < 3; l ++) norm_ws_weighted += wS_centered[idx].coord[l] * wS_centered[idx].coord[l];
				for (int l = 0; l < 3; l ++) norm_ws_q += wS_q[idx].coord[l] * wS_q[idx].coord[l];
			}	
			norm_ws_weighted = sqrt(norm_ws_weighted);
			norm_ws_q = sqrt(norm_ws_q);
			
			mu = norm_ws_q / norm_ws_weighted;			
			
			double A[3][3];
			for (int y = 0; y < 3; y ++)
				for (int x = 0; x < 3; x ++)
				{
					A[y][x] = 0.0;			
					for (unsigned int idx = 0; idx < Count; idx ++)
						A[y][x] += wS_q[idx].coord[y] * wS_centered[idx].coord[x];
				}
			
			double U[3][3], sigma[3], VT[3][3];
			vtkMath::SingularValueDecomposition3x3(A, U, sigma, VT);

			for (int i = 0; i < 3; i ++)
			{
				if (sigma[i] < 0)
				{
					sigma[i] = -sigma[i];
					for (int l = 0; l < 3; l ++) U[l][i] = -U[l][i];
				}
			}
			
			vtkMath::Multiply3x3(U, VT, R);
			
			if ( vtkMath::Determinant3x3(R[0][0], R[0][1], R[0][2], R[1][0], R[1][1], R[1][2], R[2][0], R[2][1], R[2][2]) < 0.0 )
			{
				for (int l = 0; l < 3; l ++) R[2][l] = -R[2][l];
			}
			
			inliers_npts = 0;
			for (unsigned int idx = 0; idx < Count; idx ++)
			{
				if (costvector[idx] > inlierErrorPixelTH) continue;			
				inliers_npts ++;
			}
			
			// should break?
			if (iter > 30)
			{
				if (inliers_npts - inliers_npts_record[iter - 20] < 1) break;				
				double R_diff[9];
				for (int l = 0; l < 9; l ++) R_diff[l] =  R[l] - R_last[l];
				double R_diff_norm = 0.0;
				for (int l = 0; l < 9; l ++) R_diff_norm += R_diff[l] * R_diff[l];
				if (R_diff_norm < 1e-8) break;
			}
			
			inliers_npts_record[iter] = inliers_npts;
			for (int l1 = 0; l1 < 3; l1 ++)
				for (int l2 = 0; l2 < 3; l2 ++) R_last[l1][l2] = R[l1][l2];
		}
		
		double sumerror = 0.0;
		for (unsigned int idx = 0; idx < Count; idx ++)
		{
			if (costvector[idx] > inlierErrorPixelTH) continue;
			sumerror += costvector[idx];			
		}	
		
		//printf("benchmarkid = %d, iternum = %d, inliers_npts = %d, bestinliers_npts = %d, TotalMatchingCount = %d\n", benchmarkid, iter, inliers_npts, bestinliers_npts, TotalMatchingCount);
	
		if (inliers_npts == 0) continue;

		double fracinliers = 0.0;
		if (inliers_npts > bestinliers_npts)
		{
			bestinliers_npts = inliers_npts;
			bestsumerror = sumerror;

			for (int y = 0; y < 3; y ++)
				for (int x = 0; x < 3; x ++)
					BestR[y][x] = R[y][x];
		
			double T_raw[3] = {0.0, 0.0, 0.0};
			for (unsigned int idx = 0; idx < Count; idx ++)
			{
				if (costvector[idx] > inlierErrorPixelTH) continue;
			
				double realq[3];
				for (int l = 0; l < 3; l ++) realq[l] = 1.0 / mu * lambda[idx] * uv_getridofbench[idx].coord[l];
				
				//printf("idx = %d, realq = %.4f %.4f %.4f\n", idx, realq[0], realq[1], realq[2]);
				
				double RXw[3];
				R33xCoord3(BestR, Xw_getridofbench[idx].coord, RXw);
				//printf("idx = %d, RXw = %.4f %.4f %.4f\n", idx, RXw[0], RXw[1], RXw[2]);
				
				for (int l = 0; l < 3; l ++) T_raw[l] += (realq[l] - RXw[l]);
			}
		
			for (int l = 0; l < 3; l ++) BestT[l] = T_raw[l] / (double)bestinliers_npts;
			
		/*	printf("BestR = \n");
			for (int i = 0; i < 3; i ++)
			{
				for (int j = 0; j < 3; j ++)
					printf("%.4f, ", BestR[i][j]);
				printf("\n");
			}
			printf("BestT = %.4f %.4f %.4f\n", BestT[0], BestT[1], BestT[2]);
		*/	
			
			fracinliers = ((double)inliers_npts + 1.0) / (double)TotalMatchingCount;
			if (fracinliers < 1e-5) fracinliers = 1e-5;

		//	printf("fracinliers = %.4f\n", fracinliers);
			if (fracinliers > .55)
				punish = 0.0;
			else				
				punish = -4.6 / log(1.0 - fracinliers);
		}	

	//	printf("inliers_npts = %d, fracinliers = %lf, punish = %lf\n", inliers_npts, fracinliers, punish);

		if (punish < tryidx + 1.0)
		{
			for (int y = 0; y < 3; y ++)
				for (int x = 0; x < 3; x ++)
					dcm_out[3 * y + x] = BestR[y][x];
	
			for (int l = 0; l < 3; l ++) T_out[l] = BestT[l];
			
			NumofGoodMatches = bestinliers_npts;
			GoodMatchesRate = bestinliers_npts / (double)TotalMatchingCount;
			return bestsumerror / (double)bestinliers_npts;	
		}
	}
	
	for (int y = 0; y < 3; y ++)
		for (int x = 0; x < 3; x ++)
			dcm_out[3 * y + x] = BestR[y][x];
	
	for (int l = 0; l < 3; l ++) T_out[l] = BestT[l];
		
	NumofGoodMatches = bestinliers_npts;
	GoodMatchesRate = bestinliers_npts / (double)TotalMatchingCount;
	return bestsumerror / (double)bestinliers_npts;		
}



double GetPoseDifference(double dcm1[9], double dcm2[9], double T1[3], double T2[3])
{
	double angle1[3], angle2[3];
	DCM2Angle(dcm1, angle1);
	DCM2Angle(dcm2, angle2);

	double angledifference[3], Tdifference[3];
	for (int l = 0; l < 3; l ++) angledifference[l] = angle1[l] - angle2[l];
	for (int l = 0; l < 3; l ++) Tdifference[l] = T1[l] - T2[l];

	double angledifferencenorm = sqrt(angledifference[0] * angledifference[0] + angledifference[1] * angledifference[1] + angledifference[2] * angledifference[2]);
	double Tdifferencenorm = sqrt(Tdifference[0] * Tdifference[0] + Tdifference[1] * Tdifference[1] + Tdifference[2] * Tdifference[2]);

	angledifferencenorm = (angledifferencenorm < 2 * CV_PI - angledifferencenorm) ? angledifferencenorm : 2 * CV_PI - angledifferencenorm;
	
	return 300.0 * angledifferencenorm + Tdifferencenorm;
}



/*
void UpdateNodeDCM_angle_T(CNode* Nodes_GPU, CNode* Nodes)
{
	GetGPUData2CPU(Nodes_GPU, Nodes, sizeof(CNode));
	DCM2Angle(Nodes[0].dcm, Nodes[0].angle);	
	GetCPUData2GPU(Nodes_GPU, Nodes, sizeof(CNode));	
//	printf("PnP estimation: angle0 = %.3f %.3f %.3f, T = %.3f %.3f %.3f\n", Nodes[0].angle[0], Nodes[0].angle[1], Nodes[0].angle[2], Nodes[0].T[0], Nodes[0].T[1], Nodes[0].T[2]);
}
*/

/*

void KalmanFilter_Prediction(CNode* Nodes)
{
//	double angle[3];
//	DCM2Angle(Nodes[0].dcm, angle);

	double dt = 0.1;
	for (int l = 0; l < 3; l ++) Nodes[0].angle_filtered[l] = Nodes[0].angle_prev[l] + dt * Nodes[0].dangle_prev[l];
	for (int l = 0; l < 3; l ++) Nodes[0].T_filtered[l]     = Nodes[0].T_prev[l]     + dt * Nodes[0].dT_prev[l];
}

void KalmanFilter_MovePrediction2Node(CNode* Nodes)
{
	Angle2DCM(Nodes[0].angle_filtered, Nodes[0].dcm);
	for (int l = 0; l < 3; l ++) Nodes[0].T[l] = Nodes[0].T_filtered[l];
	
//	printf("InKalman: dcm_updated = %.3f %.3f %.3f\n", Nodes[0].dcm[0][0], Nodes[0].dcm[0][1], Nodes[0].dcm[0][2]);			
//	printf("dcm_updated = %.3f %.3f %.3f\n", Nodes[0].dcm[1][0], Nodes[0].dcm[1][1], Nodes[0].dcm[1][2]);			
//	printf("dcm_updated = %.3f %.3f %.3f\n", Nodes[0].dcm[2][0], Nodes[0].dcm[2][1], Nodes[0].dcm[2][2]);			
//	printf("T_updated = %.3f %.3f %.3f\n", Nodes[0].T[0], Nodes[0].T[1], Nodes[0].T[2]);
//	printf("angle_filtered = %.3f %.3f %.3f\n", Nodes[0].angle_filtered[0], Nodes[0].angle_filtered[1], Nodes[0].angle_filtered[2]);

}


void KalmanFilter_Merge_Prediction_Obseration_R(CNode* Nodes, double dcm_PnP[3][3], double Rnoise)
{
	const double dt = 0.1;
	const double Q = 10.0;	

	double P_1 = Nodes[0].P_angle[0];
	double P_2 = Nodes[0].P_angle[1];
	double P_3 = Nodes[0].P_angle[2];
	double P_4 = Nodes[0].P_angle[3];

	double P1_1 = P_1 + dt * P_3 + dt * P_2 + dt * dt * P_4 + Q;
	double P1_2 = P_2 + dt * P_4 + Q;
	double P1_3 = P_3 + dt * P_4 + Q;
	double P1_4 = P_4 + Q;

	double S = P1_1 + Rnoise; //10

	double K1 = P1_1 / S;
	double K2 = P1_3 / S;

	Nodes[0].P_angle[0] = P1_1 - K1 * P1_1;
	Nodes[0].P_angle[1] = P1_2 - K1 * P1_2;
	Nodes[0].P_angle[2] = P1_3 - K2 * P1_1;
	Nodes[0].P_angle[3] = P1_4 - K2 * P1_2;

	double angle_PnP[3];
	DCM2Angle(dcm_PnP, angle_PnP);
	
//	printf("In KalmanFilter_Merge_Prediction_Obseration_R: dcm_PnP = %.3f %.3f %.3f\n", dcm_PnP[0][0], dcm_PnP[0][1], dcm_PnP[0][2]);			
//	printf("dcm_PnP = %.3f %.3f %.3f\n", dcm_PnP[1][0], dcm_PnP[1][1], dcm_PnP[1][2]);			
//	printf("dcm_PnP = %.3f %.3f %.3f\n", dcm_PnP[2][0], dcm_PnP[2][1], dcm_PnP[2][2]);			

	double angle_current[3], dangle_current[3];
	for (int l = 0; l < 3; l ++)
	{
		double Z = angle_PnP[l];	
			
		double dangle_prev   = Nodes[0].dangle_prev[l];	
		double angle_predict = Nodes[0].angle_filtered[l];
	
		angle_current[l]  = angle_predict + K1 * (Z - angle_predict);
		dangle_current[l] = dangle_prev   + K2 * (Z - angle_predict);
		
	//	printf("l = %d, angle_predict = %.3f, K1 = %.3f, Z = %.3f, angle_predict = %.3f, dangle_prev = %.3f, K2 = %.3f\n", l, angle_predict, K1, Z, angle_predict, dangle_prev, K2);
		
		Nodes[0].angle_filtered[l] = angle_current[l];
		
		Nodes[0].angle_prev[l]  = angle_current[l];
		Nodes[0].dangle_prev[l] = dangle_current[l];
	}	

//	Angle2DCM(angle_current, Nodes[0].dcm); // if using Kalman filter result
}


void KalmanFilter_Merge_Prediction_Obseration_T(CNode* Nodes, double T_PnP[3], double Tnoise)
{
	const double dt = 0.1;
	const double Q = 10.0;	
	double P_1 = Nodes[0].P_T[0];
	double P_2 = Nodes[0].P_T[1];
	double P_3 = Nodes[0].P_T[2];
	double P_4 = Nodes[0].P_T[3];
	double P1_1 = P_1 + dt * P_3 + dt * P_2 + dt * dt * P_4 + Q;
	double P1_2 = P_2 + dt * P_4 + Q;
	double P1_3 = P_3 + dt * P_4 + Q;
	double P1_4 = P_4 + Q;
	
	double S = P1_1 + Tnoise;//10.0;

	double K1 = P1_1 / S;
	double K2 = P1_3 / S;
	Nodes[0].P_T[0] = P1_1 - K1 * P1_1;
	Nodes[0].P_T[1] = P1_2 - K1 * P1_2;
	Nodes[0].P_T[2] = P1_3 - K2 * P1_1;
	Nodes[0].P_T[3] = P1_4 - K2 * P1_2;

	double T_current[3], dT_current[3];
	for (int l = 0; l < 3; l ++)
	{
		double Z = T_PnP[l];	
			
		double dT_prev   = Nodes[0].dT_prev[l];	
		double T_predict = Nodes[0].T_filtered[l];
	
		T_current[l]  = T_predict + K1 * (Z - T_predict);
		dT_current[l] = dT_prev   + K2 * (Z - T_predict);

		Nodes[0].T_filtered[l] = T_current[l];

		Nodes[0].T_prev[l]  = T_current[l];
		Nodes[0].dT_prev[l] = dT_current[l];
	}
		
//	for (int l = 0; l < 3; l ++) Nodes[0].T[l] = T_current[l];
}
*/







