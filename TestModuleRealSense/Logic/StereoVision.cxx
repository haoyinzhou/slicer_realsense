#include "StereoVision.h"

using namespace cv;
using namespace std;

void GenerateImgPhyCoord(uchar* Mask, float* disparity, double* coord, vtkPoints* Points
						, int W, int H
						, double FocalLength, double ImgCenterU, double ImgCenterV)
{
	Points->Reset();
	for (int y = 0; y < H; y++)
		for (int x = 0; x < W; x++)
		{
			if (Mask[y * W + x] < 64)
			{
				for (int l = 0; l < 3; l++) coord[3 * (y * W + x) + l] = 0.0;
				continue;
			}

			double disparitycoord[3], phycoord[3];
			disparitycoord[0] = (double)x;
			disparitycoord[1] = (double)y;
			disparitycoord[2] = (double)disparity[y * W + x];
			DisparityCoord2PhyCoord_double(phycoord, disparitycoord, FocalLength, ImgCenterU, ImgCenterV);

			Points->InsertNextPoint(phycoord);
			for (int l = 0; l < 3; l++) coord[3 * (y * W + x) + l] = phycoord[l];
		}
}

/*
void GenerateImgNormal(uchar* Mask, double* coord, double* normal
										, int W, int H)
{
	for (int y = 0; y < H; y++)
		for (int x = 0; x < W; x++)
		{
			if (Mask[y * W + x] == 0) continue;

			double coord0[3];
			for (int l = 0; l < 3; l++) coord0[l] = coord[3 * (y * W + x) + l];

			int idy[4] = { 0, 1, -1, 0 };
			int idx[4] = { 1, 0, 0, -1 };

			double dir2neighorpoints[2][3];
			int count = 0;
			for (int i = 0; i < 4; i++)
			{
				for (int r = 1; r < 10; r++)
				{
					int ynew = y + r * idy[i];
					int xnew = x + r * idx[i];
					if (ynew < 0 || ynew > H - 1 || xnew < 0 || xnew > W - 1) continue;
					if (Mask[ynew * W + xnew] == 0) continue;

					for (int l = 0; l < 3; l++)
						dir2neighorpoints[count][l] = coord[3 * (ynew * W + xnew) + l] - coord0[l];
					count++;
					break;
				}
				if (count > 1) break;
			}

			if (count < 2)
			{
				normal[3 * (y * W + x) + 0] = 0.0;
				normal[3 * (y * W + x) + 1] = 0.0;
				normal[3 * (y * W + x) + 2] = 1.0;
				return;
			}

			double sum = 0.0;
			for (int l = 0; l < 3; l++) sum += dir2neighorpoints[0][l] * dir2neighorpoints[0][l];
			sum = sqrt(sum);
			for (int l = 0; l < 3; l++) dir2neighorpoints[0][l] = dir2neighorpoints[0][l] / (sum + 1e-8);
			sum = 0.0;
			for (int l = 0; l < 3; l++) sum += dir2neighorpoints[1][l] * dir2neighorpoints[1][l];
			sum = sqrt(sum);
			for (int l = 0; l < 3; l++) dir2neighorpoints[1][l] = dir2neighorpoints[1][l] / (sum + 1e-8);

			double normalx, normaly, normalz;
			CrossProduct(dir2neighorpoints[0][0], dir2neighorpoints[0][1], dir2neighorpoints[0][2], dir2neighorpoints[1][0], dir2neighorpoints[1][1], dir2neighorpoints[1][2], normalx, normaly, normalz);

			normal[3 * (y * W + x) + 0] = (normalz > 0.0) ? normalx : -normalx;
			normal[3 * (y * W + x) + 1] = (normalz > 0.0) ? normaly : -normaly;
			normal[3 * (y * W + x) + 2] = (normalz > 0.0) ? normalz : -normalz;
		}
}
*/

// kernel functions

/*
__global__ void HoleFilling_kernel(float* disparitymap_GPU, uchar* Mask_GPU, uchar* Mask_temp_GPU
									, int W, int H
									, float MaxDisparityDiff, int LeastPixelCount, int LeftEdge)
{
	int y = blockIdx.x;
	int x = threadIdx.x;
	if (y < WinSize || y > H - WinSize - 1) return;
	if (x < LeftEdge     || x > W - WinSize - 1) return;
	if (Mask_temp_GPU[y * W + x] > 0) return;

	int findcount = 0;
	float sumdisparity = 0.0;
	
	float MinDis =  10000.0;
	float MaxDis = -10000.0;
	
	for (int tryy = y - 10; tryy < y + 11; tryy += 2)
		for (int tryx = x - 10; tryx < x + 11; tryx += 2)
		{
			if (tryy < WinSize || tryy > H - WinSize - 1) continue;
			if (tryx < WinSize || tryx > W - WinSize - 1) continue;
		
			if (Mask_temp_GPU[tryy * W + tryx] > 128)
			{
				float disparity_neighor = disparitymap_GPU[tryy * W + tryx];
				
				MinDis = (MinDis > disparity_neighor) ? disparity_neighor : MinDis;
				MaxDis = (MaxDis < disparity_neighor) ? disparity_neighor : MaxDis;
				
				sumdisparity += disparity_neighor;
				findcount ++;
				
				if (findcount > LeastPixelCount)
				{
					if (MaxDis - MinDis > MaxDisparityDiff) return;

					disparitymap_GPU[y * W + x] = sumdisparity / (float)findcount;
					Mask_GPU[y * W + x] = 128; 
					return;							
				}

			}
		}
}

__global__ void GetRidofSmallFragment_kernel(float* disparitymap_GPU, uchar* Mask_GPU, uchar* Mask_GPU_temp
							, int W, int H
							, int Radius, int LeftEdge)
{
	int y = blockIdx.x;
	int x = threadIdx.x;

	if (y < 0 || y > H - 1 || x < LeftEdge || x > W - 1) return;
	if (Mask_GPU_temp[y * W + x] == 0) return;
	
	float disparity0 = disparitymap_GPU[y * W + x];
	
	int dirx[8] = {1, 1, 0, -1, -1, -1, 0, 1};
	int diry[8] = {0, 1, 1, 1, 0, -1, -1, -1};
	
	uchar count_findgooddir = 0;
	
	for (int dirid = 0; dirid < 8; dirid ++)
	{
		float disparity_last = disparity0;
		uchar flag_thisdirisgood = 255;
		
		for (int range = 2; range < Radius; range += 2)
		{
			int tryx = x + range * dirx[dirid];
			int tryy = y + range * diry[dirid];
			
			if (tryx < 0 || tryx > W - 1 || tryy < 0 || tryy > H - 1)
			{
				flag_thisdirisgood = 0;
				break;
			}
			
			if (Mask_GPU_temp[tryy * W + tryx] == 0)
			{
				flag_thisdirisgood = 0;
				break;
			}

			float disparity_this = disparitymap_GPU[tryy * W + tryx];			
			if (disparity_this - disparity_last > 0.1 || disparity_this - disparity_last < -0.1)
			{
				flag_thisdirisgood = 0;
				break;
			}
			disparity_last = disparity_this;
		}
		
		if (flag_thisdirisgood == 255)
		{
			count_findgooddir ++;
			if (count_findgooddir > 1)
				break;
		}
	}
	
	if (count_findgooddir <= 1)
	{
		Mask_GPU[y * W + x] = 0;
	}
	
}


__global__ void HoleFilling_8Directions_kernel(float* disparitymap_GPU
											, uchar* Mask_GPU, uchar* Mask_GPU_temp
											, int W, int H
											, int Radius, int LeastGoodDirs, float MostDisparityDiff, int LeftEdge)
{
	int y = blockIdx.x;
	int x = threadIdx.x;
	if (y < WinSize || y > H - WinSize - 1) return;
	if (x < LeftEdge     || x > W - WinSize - 1) return;
	
	if (Mask_GPU_temp[y * W + x] > 0) return;

	int dirx[8] = {1, 1, 0, -1, -1, -1, 0, 1};
	int diry[8] = {0, 1, 1, 1, 0, -1, -1, -1};
	
	int findcount = 0;
	float dirweight[8] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
	float dirdisparity[8] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
//	float dirweight_normalized[8] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
		
	float maxdisparity = -1000.0;
	float mindisparity =  1000.0;
	for (int dirid = 0; dirid < 8; dirid ++)
	{
		//for (int range = 1; range < 50; range += 3)
		for (int range = 1; range < Radius; range += 3)
		{
			int tryx = x + range * dirx[dirid];
			int tryy = y + range * diry[dirid];
		
			if (tryy < WinSize || tryy > H - WinSize - 1) break;
			if (tryx < WinSize || tryx > W - WinSize - 1) break;
		
			if (Mask_GPU_temp[tryy * W + tryx] > 0)
			{
				float disparity_neighor = disparitymap_GPU[tryy * W + tryx];
			//	if (disparity_neighor > -10.0) continue; 
				
				dirweight[dirid] = 1.0 / range;				
				dirdisparity[dirid] = disparity_neighor;
				
				maxdisparity = (maxdisparity < disparity_neighor) ? disparity_neighor : maxdisparity;
				mindisparity = (mindisparity > disparity_neighor) ? disparity_neighor : mindisparity;
				
				findcount ++;
				break;
			}
		}
	}
	
	if (findcount > LeastGoodDirs && maxdisparity - mindisparity < MostDisparityDiff)
	{
		float sumweight = 0.0;
		for (int l = 0; l < 8; l ++) sumweight = sumweight + dirweight[l];
		sumweight = 1.0 / sumweight;
		for (int l = 0; l < 8; l ++) dirweight[l] = dirweight[l] * sumweight;
	
		float meandisparity = 0.0;
		for (int l = 0; l < 8; l ++) meandisparity += dirweight[l] * dirdisparity[l];
		
		disparitymap_GPU[y * W + x] = meandisparity;
		Mask_GPU[y * W + x] = 160; 
	}
}
*/

/*
void SmoothDepths_HCalgorithm_part1(float* disparitymap, uchar* Maskmap
							, float* disparitymap_orignal, float* disparitymap_last, float* b
							, int W, int H
							, int step, float MaxDisparityDiff, int SmoothingRadius)
{	
	for (int y = 0; y < H; y++)
		for (int x = 0; x < W; x++)
		{
			int yWx = y * W + x;
			if (Maskmap[yWx] == 0) continue;;

			float disparity0 = disparitymap_last[yWx];
			float disparitysmoothed = 0.0;
			int NeigborCount = 0;
			for (int offsety = -SmoothingRadius; offsety < SmoothingRadius + 1; offsety += step)
				for (int offsetx = -SmoothingRadius; offsetx < SmoothingRadius + 1; offsetx += step)
				{
					int _y = y + offsety;
					int _x = x + offsetx;

					if (_y < 0 || _y > H - 1 || _x < 0 || _x > W - 1) continue;
					if (Maskmap[_y * W + _x] == 0) continue;

					float neighordisparity = disparitymap_last[_y * W + _x];
					float diff = disparity0 - neighordisparity;
					diff = (diff < 0.0) ? -diff : diff;

					if (diff < MaxDisparityDiff)
					{
						disparitysmoothed += neighordisparity;
						NeigborCount++;
					}
				}

			disparitysmoothed = (NeigborCount == 0) ? disparity0 : disparitysmoothed / (float)NeigborCount;
			disparitymap[yWx] = disparitysmoothed;

			b[yWx] = disparitysmoothed - (0.1 * disparitymap_orignal[yWx] + 0.9 * disparitymap_last[yWx]);
		}	
}

void SmoothDepths_HCalgorithm_part2(float* disparitymap, uchar* Maskmap
							, float* disparitymap_orignal, float* disparitymap_last, float* b
							, int W, int H
							, int step, float MaxDisparityDiff, int SmoothingRadius)
{	
	for (int y = 0; y < H; y++)
		for (int x = 0; x < W; x++)
		{
			int yWx = y * W + x;
			if (Maskmap[yWx] == 0) continue;

			float disparity0 = disparitymap_last[y * W + x];
			float bsmoothed = 0.0;
			int NeigborCount = 0;
			for (int offsety = -SmoothingRadius; offsety < SmoothingRadius + 1; offsety += step)
				for (int offsetx = -SmoothingRadius; offsetx < SmoothingRadius + 1; offsetx += step)
				{
					if (offsetx == 0 && offsety == 0) continue;
					int _y = y + offsety;
					int _x = x + offsetx;

					if (_y < 0 || _y > H - 1 || _x < 0 || _x > W - 1) continue;
					if (Maskmap[_y * W + _x] == 0) continue;

					float diff = disparity0 - disparitymap_last[_y * W + _x];
					diff = (diff < 0.0) ? -diff : diff;

					if (diff < MaxDisparityDiff)
					{
						bsmoothed += b[_y * W + _x];
						NeigborCount++;
					}
				}
			bsmoothed = (NeigborCount == 0) ? b[yWx] : bsmoothed / (float)NeigborCount;

			disparitymap[yWx] = disparitymap[yWx] - (0.2 * b[yWx] + 0.8 * bsmoothed);
		}
}
*/

// RGBD-related
bool ReadRGBDDataFromFiles(const char* RGBFileName, const char* DepthFileName
	, CStereoMatchingData* StereoMatchingData
	, CParams* Params)
{
	int W = Params->W;
	int H = Params->H;

	// read the RGB image to RGB_raw
	// and downsample it to StereoMatchingData->img1
	Mat RGB_raw;
	RGB_raw = imread(RGBFileName);

	cv::resize(RGB_raw, RGB_raw, cv::Size(2 * W, 2 * H));
    
	for (int y = 0; y < H; y++)
		for (int x = 0; x < W; x++)
		{
			int yWx = y * W + x;
			for (int l = 0; l < 3; l++) StereoMatchingData->img1_RGB[3 * yWx + l] = RGB_raw.data[12 * y * W + 6 * x + l];
			StereoMatchingData->img1[yWx] = (uchar)(.2126 * (float)(StereoMatchingData->img1_RGB[3 * yWx]) + .7152 * (float)(StereoMatchingData->img1_RGB[3 * yWx + 1]) + .0722 * (float)(StereoMatchingData->img1_RGB[3 * yWx + 2]));
		}

	// read the depth (.csv) file 
	// and downsample it to 
	std::ifstream file(DepthFileName);
	vector<float> DepthMat_raw;
	if (file.is_open())
	{
		std::string line;
		while (getline(file, line))
		{
			std::istringstream stream(line);
			//	std::cout << "line = " << line << std::endl;
			int w = 0;
			while (stream)
			{
				string s;
				if (!std::getline(stream, s, ',')) break;
				DepthMat_raw.push_back(1000.0 * std::stof(s)); // !!!!!!,because the input file are in meters, we want mm.
				//	std::cout << s << ", w = " << w << std::endl;
				w++;
				if (w >= 2*W) break;
			}
		}
		file.close();
	}

	for (int y = 0; y < H; y++)
		for (int x = 0; x < W; x++)
		{
			int yWx = y * W + x;
			double d = DepthMat_raw.at(4 * y * W + 2 * x);
			StereoMatchingData->disparitymap[yWx] = d;

			if (d < Params->RGBDMinDistance || d > Params->RGBDMaxDistance)
				StereoMatchingData->Mask[yWx] = 0;
			else
				StereoMatchingData->Mask[yWx] = 255;
		}
	return true;
}


void RealSenseData2StereoMatchingData(uchar* RGB_in, float* depth_in
                                    , CStereoMatchingData* StereoMatchingData
                                    , CParams* Params)
{
    int W = Params->W;
    int H = Params->H;

 //   std::cout << "W = " << W << ", H = " << H << std::endl;
    
    for (int y = 0; y < H; y++)
        for (int x = 0; x < W; x++)
        {
            int yWx = y * W + x;
      //      for (int l = 0; l < 3; l++) StereoMatchingData->img1_RGB[3 * yWx + l] = RGB_in[3 * yWx + 2 - l];
            for (int l = 0; l < 3; l++) StereoMatchingData->img1_RGB[3 * yWx + l] = RGB_in[3 * yWx + l];
            StereoMatchingData->img1[yWx] = (uchar)(.2126 * (float)(StereoMatchingData->img1_RGB[3 * yWx]) + .7152 * (float)(StereoMatchingData->img1_RGB[3 * yWx + 1]) + .0722 * (float)(StereoMatchingData->img1_RGB[3 * yWx + 2]));
        }
    
    for (int y = 0; y < H; y++)
        for (int x = 0; x < W; x++)
        {
            int yWx = y * W + x;
            float d = depth_in[yWx];
            StereoMatchingData->disparitymap[yWx] = d;

            if (d < Params->RGBDMinDistance || d > Params->RGBDMaxDistance)
                StereoMatchingData->Mask[yWx] = 0;
            else
                StereoMatchingData->Mask[yWx] = 255;
        }

   for (int y = 0; y < H; y++)
        for (int x = 0; x < W; x++)
 	{
 		if (y < 20 || y > H - 20 || x < 20 || x > W - 20) // cut the edge, because of image distortion.
               StereoMatchingData->Mask[y * W + x] = 0;
 	}

/*    cv::Mat GreyMat = Mat::zeros(H, W, CV_8UC1);
    for (int y = 0; y < H; y++)
          for (int x = 0; x < W; x++)
          {
              int yWx = y * W + x;
              GreyMat.data[yWx] = StereoMatchingData->img1[yWx];
          //    temp.data[yWx] = StereoMatchingData->Mask[yWx];
          }
 //   namedWindow("Grey", WINDOW_AUTOSIZE );
 //   imshow("Grey", GreyMat);
*/
/*
    cv::Mat DepthMat = Mat::zeros(H, W, CV_8UC1);
    for (int y = 0; y < H; y++)
          for (int x = 0; x < W; x++)
          {
              int yWx = y * W + x;
              double d = depth_in[yWx];

              DepthMat.data[yWx] = (uchar)(255.0 / (1200.0) * (d - 300.0));
          //    temp.data[yWx] = StereoMatchingData->Mask[yWx];
          }
    namedWindow("depth", WINDOW_AUTOSIZE );
    imshow("depth", DepthMat);
*/
}


/*	for (int iter = 0; iter < Params->HoleFillingIterNum; iter ++)
	{
		int radius = Params->OutliersRemovalBaseRadius + Params->OutliersRemovalRadiusIncreaseStep * iter;
		cudaMemcpy(StereoMatchingData_GPU->Mask_temp, StereoMatchingData_GPU->Mask, W * H * sizeof(uchar), cudaMemcpyDeviceToDevice);
		GetRidofSmallFragment_kernel<<< H, W >>>(StereoMatchingData_GPU->disparitymap
												, StereoMatchingData_GPU->Mask, StereoMatchingData_GPU->Mask_temp
												, W, H
												, radius, Params->LeftEdge);

		cudaMemcpy(StereoMatchingData_GPU->Mask_temp, StereoMatchingData_GPU->Mask, W * H * sizeof(uchar), cudaMemcpyDeviceToDevice);
		HoleFilling_8Directions_kernel<<<H, W>>>(StereoMatchingData_GPU->disparitymap
											, StereoMatchingData_GPU->Mask, StereoMatchingData_GPU->Mask_temp
											, W, H
											, Params->HoleFillingRadius, Params->HoleFillingLeastNumofGoodDirs, Params->AllowedHoleFillingMaxDisparityDiff, Params->LeftEdge);

		cudaMemcpy(StereoMatchingData_GPU->Mask_temp, StereoMatchingData_GPU->Mask, W * H * sizeof(uchar), cudaMemcpyDeviceToDevice);
		HoleFilling_kernel<<<H, W>>>(StereoMatchingData_GPU->disparitymap, StereoMatchingData_GPU->Mask, StereoMatchingData_GPU->Mask_temp, W, H, Params->AllowedHoleFillingMaxDisparityDiff, Params->HoleFillingLeastPixelCount, Params->LeftEdge);
	}
*/


/*
bool PostProcessingDepthData(CStereoMatchingData* StereoMatchingData
	, CParams* Params)
{
	int W = Params->W;
	int H = Params->H;

	// post-processing


	memcpy(StereoMatchingData->disparitymap_forsmooth1, StereoMatchingData->disparitymap, W * H * sizeof(float));
	for (int iter2 = 0; iter2 < Params->SmoothingNum; iter2++)
	{
		memcpy(StereoMatchingData->disparitymap_forsmooth2, StereoMatchingData->disparitymap, W * H * sizeof(float));
		SmoothDepths_HCalgorithm_part1(StereoMatchingData->disparitymap, StereoMatchingData->Mask
			, StereoMatchingData->disparitymap_forsmooth1, StereoMatchingData->disparitymap_forsmooth2, StereoMatchingData->disparitymap_forsmooth3
			, W, H
			, Params->SmoothingStep, Params->AllowedSmoothingMaxDisparityDiff, Params->SmoothingRadius);

		SmoothDepths_HCalgorithm_part2(StereoMatchingData->disparitymap, StereoMatchingData->Mask
			, StereoMatchingData->disparitymap_forsmooth1, StereoMatchingData->disparitymap_forsmooth2, StereoMatchingData->disparitymap_forsmooth3
			, W, H
			, Params->SmoothingStep, Params->AllowedSmoothingMaxDisparityDiff, Params->SmoothingRadius);
	}

	return true;
}
*/

void RGBDData2Results(CStereoMatchingData* StereoMatchingData
	, CStereoMatchingResult* StereoMatchingResult
	, CParams* Params)
{
	int W = Params->W;
	int H = Params->H;

	memcpy(StereoMatchingResult->Mask, StereoMatchingData->Mask, W * H * sizeof(uchar));
	//memcpy(StereoMatchingResult->intense, StereoMatchingData->img1, W * H * sizeof(uchar));
	memcpy(StereoMatchingResult->RGB, StereoMatchingData->img1_RGB, 3 * W * H * sizeof(uchar));
	memcpy(StereoMatchingResult->disparity, StereoMatchingData->disparitymap, W * H * sizeof(float));

	GenerateImgPhyCoord(StereoMatchingResult->Mask, StereoMatchingResult->disparity, StereoMatchingResult->coord, StereoMatchingResult->Points, W, H, Params->FocalLength, Params->ImgCenterU, Params->ImgCenterV);
//	GenerateImgNormal(StereoMatchingResult->Mask, StereoMatchingResult->coord, StereoMatchingResult->normal, W, H);

}

























