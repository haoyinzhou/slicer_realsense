#ifndef R1PPnP_H
#define R1PPnP_H


#include "DataStructure.h"
#include <vtkMath.h>
#include "DualQ.h"
#include "opencv2/opencv.hpp"

using namespace std;
using namespace cv;

//#include "CUDArelated//SynImgMatching.h" // temp

void Initial_DCM_T_PnP(double dcm[9], double T[3]);


double R1PPnP(CTrackingFrame* TrackingFrame_this
			, CTrackingFrame* TrackingFrame_prev
			, double dcm_out[9], double T_out[3]
			, unsigned int& NumofGoodMatches, double& GoodMatchesRate
			, double inlierErrorPixelTH
			, double FocalLength, double ImgCenterU, double ImgCenterV
			, unsigned int MaxNumberOfFeaturePointsPerFrame);

double R1PPnP(std::vector<cv::KeyPoint>* ORBkeypoints, std::vector<double>* coord3d, int* MatchedID
			, double dcm_out[9], double T_out[3]
			, unsigned int& NumofGoodMatches, double& GoodMatchesRate
			, double inlierErrorPixelTH
			, double FocalLength, double ImgCenterU, double ImgCenterV, unsigned int MaxNumberOfFeaturePointsPerFrame);

double GetPoseDifference(double dcm1[9], double dcm2[9], double T1[3], double T2[3]);



#endif
