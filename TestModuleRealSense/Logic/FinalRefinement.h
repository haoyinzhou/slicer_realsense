#ifndef FinalRefinement_H
#define FinalRefinement_H

#include "DataStructure.h"
#include "RigidMosaicking_CameraMotionEstimation.h"
//#include "Visualization.h"

#include <stdio.h>
#include <iostream>     // std::cout
#include <fstream>      // std::ifstream




void FinalRefinement(vtkPoints* Points, vtkPoints* Points_live, vtkCellArray* CellArray, vtkUnsignedCharArray* RGBArray, vtkFloatArray* MergingWeight
					, CStereoMatchingResult* StereoMatchingResult
					, uchar* Mask_Uncovered
					, std::vector<CFeatureVector>* GlobalFeatureVector, std::vector<CKeyFrame>* KeyFrame
					, CTrackingFrame* TrackingFrame_this, CTrackingFrame* TrackingFrame_prev
					, CG2OParameters* G2OParameters
					, CParams* Params);

#endif
