cmake_minimum_required(VERSION 3.9.1)
#cmake_minimum_required(VERSION 3.13.4)

project(TestModuleRealSense)

#-----------------------------------------------------------------------------
# Extension meta-information
set(EXTENSION_HOMEPAGE "http://slicer.org/slicerWiki/index.php/Documentation/Nightly/Extensions/TestModuleRealSense")
set(EXTENSION_CATEGORY "IGT")
set(EXTENSION_CONTRIBUTORS "Haoyin Zhou, Angela Alarcon, Jayender Jaadeesan (jayender@bwh.harvard.edu)")
set(EXTENSION_DESCRIPTION "This is used for breast reconstructive surgery.")
set(EXTENSION_ICONURL "http://www.example.com/Slicer/Extensions/TestModuleRealSense.png")
set(EXTENSION_SCREENSHOTURLS "http://www.example.com/Slicer/Extensions/TestModuleRealSense/Screenshots/1.png")
#set(EXTENSION_DEPENDS "NA") # Specified as a space separated string, a list or 'NA' if any
set(EXTENSION_DEPENDS "SlicerOpenCV")

#-----------------------------------------------------------------------------
# Extension dependencies
find_package(Slicer REQUIRED)
include(${Slicer_USE_FILE})

# Add the CMake subdirectory which contains a FindSlicerOpenCV.cmake file that will help find the package automatically
set(CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/CMake" ${CMAKE_MODULE_PATH})
message(STATUS "CMAKE_MODULE_PATH = ${CMAKE_MODULE_PATH}")

find_package(SlicerOpenCV REQUIRED)

find_package(realsense2 REQUIRED)
#find_package(OpenCV REQUIRED)
#find_package(SlicerOpenCV REQUIRED)

#-----------------------------------------------------------------------------
# Extension modules
add_subdirectory(TestModuleRealSense)
## NEXT_MODULE

#-----------------------------------------------------------------------------
include(${Slicer_EXTENSION_GENERATE_CONFIG})
include(${Slicer_EXTENSION_CPACK})
