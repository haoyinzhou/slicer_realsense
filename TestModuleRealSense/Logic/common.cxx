#include "common.h"

using namespace cv;
using namespace std;


bool ReadPolyData(vtkPolyData *poly, const char* fileName)
{
	vtkSmartPointer<vtkXMLPolyDataReader> reader = vtkSmartPointer<vtkXMLPolyDataReader>::New();
	reader->SetFileName(fileName);
	try
	{
		reader->Update();
	}
	catch(...)
	{
		std::cerr << "Error occurs when reading " << fileName << std::endl;
		return false;
	}
	poly->DeepCopy(reader->GetOutput());
	return true;
}


void SavePolyData(vtkPolyData *poly, const char* fileName)
{
	if(!poly) return;
	vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
	writer->SetInputData(poly);
	writer->SetFileName(fileName);
	writer->SetDataModeToBinary();
	try
	{
		std::cout << "Save the polydata to " << fileName << std::endl;
		writer->Write();
	}
	catch(...)
	{
		std::cerr << "Error occurs when writing " << fileName << std::endl;
		return;
	}
}



/*
bool ReadXYmapFiles(int* Xmap1, int* Ymap1, int* Xmap2,	int* Ymap2
				, int H, int W
				, String* filename_Xmap1, String* filename_Ymap1, String* filename_Xmap2, String* filename_Ymap2
				, CStereoMatchingData* StereoMatchingData_GPU)
{
 	{
	 	std::ifstream file_Xmap1(filename_Xmap1->c_str());
	 	if (!file_Xmap1) return false;
		std::cout << *filename_Xmap1 << " is opened!" << std::endl;
 	 	std::string line;
	 	int y = 0;
	 	while (std::getline(file_Xmap1, line))
	 	{
	 		std::istringstream stream(line);
	 		int x = 0;
	 		while(stream)
	 		{
	 			string s;
	 			if (!std::getline(stream, s, ' ')) break;
	 			Xmap1[y * W + x] = std::stoi(s);
	 			x ++;
	 		}
	 		y ++;
	 	}
 	}
	
 	{
	 	std::ifstream file_Ymap1(filename_Ymap1->c_str());
	 	if (!file_Ymap1) return false;
		std::cout << *filename_Ymap1 << " is opened!" << std::endl;
	 	std::string line;
	 	int y = 0;
	 	while (std::getline(file_Ymap1, line))
	 	{
	 		std::istringstream stream(line);
	 		int x = 0;
	 		while(stream)
	 		{
	 			string s;
	 			if (!std::getline(stream, s, ' ')) break;
	 			Ymap1[y * W + x] = std::stoi(s);
	 			x ++;
	 		}
	 		y ++;
	 	}
	}
	
  	{
	 	std::ifstream file_Xmap2(filename_Xmap2->c_str());
	 	if (!file_Xmap2) return false;
		std::cout << *filename_Xmap2 << " is opened!" << std::endl;
	 	std::string line;
	 	int y = 0;
	 	while (std::getline(file_Xmap2, line))
	 	{
	 		std::istringstream stream(line);
	 		int x = 0;
	 		while(stream)
	 		{
	 			string s;
	 			if (!std::getline(stream, s, ' ')) break;
	 			Xmap2[y * W + x] = std::stoi(s);
	 			x ++;
	 		}
	 		y ++;
	 	}
 	}
 	
  	{
	 	std::ifstream file_Ymap2(filename_Ymap2->c_str());
	 	if (!file_Ymap2) return false;
		std::cout << *filename_Ymap2 << " is opened!" << std::endl;
	 	std::string line;
	 	int y = 0;
	 	while (std::getline(file_Ymap2, line))
	 	{
	 		std::istringstream stream(line);
	 		int x = 0;
	 		while(stream)
	 		{
	 			string s;
	 			if (!std::getline(stream, s, ' ')) break;
	 			Ymap2[y * W + x] = std::stoi(s);
	 			x ++;
	 		}
	 		y ++;
	 	}
 	}		
 	
	GetCPUData2GPU(StereoMatchingData_GPU->Xmap1, Xmap1, W * H * sizeof(int));
	GetCPUData2GPU(StereoMatchingData_GPU->Ymap1, Ymap1, W * H * sizeof(int));
	GetCPUData2GPU(StereoMatchingData_GPU->Xmap2, Xmap2, W * H * sizeof(int));
	GetCPUData2GPU(StereoMatchingData_GPU->Ymap2, Ymap2, W * H * sizeof(int));

	return true;
}
*/

void PrintDCMandT(double* dcm, double* T)
{
//	std::cout << Text << std::endl;
	std::cout << "dcm = ";
	for (int l1 = 0; l1 < 3; l1 ++)
	{
		for (int l2 = 0; l2 < 3; l2 ++)
			std::cout << dcm[3 * l1 + l2] << " ";
		std::cout << std::endl;
	}
	std::cout << "T = " << T[0] << ", " << T[1] << ", " << T[2] << std::endl;
}








