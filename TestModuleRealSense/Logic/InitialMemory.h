#ifndef InitialMemory_H
#define InitialMemory_H

#include <vector>
#include "DataStructure.h"

//using namespace std;
//using namespace cv;

void InitialMemory_StereoVision(CStereoMatchingData* StereoMatchingData
	, CStereoMatchingResult* StereoMatchingResult
	, int W, int H);

void ClearMemory_StereoVision(CStereoMatchingData* StereoMatchingData
	, CStereoMatchingResult* StereoMatchingResult);


void InitialMemory_DataManagement(uchar*& Mask_Uncovered
	, int W, int H);


//void ClearMemory_DataManagement(CPoint* Points);

void InitialMemory_FeaturePoints(CTrackingFrame* TrackingFrame, cv::Mat* img1, unsigned int MaxNumberOfFeaturePointsPerFrame, int W, int H);
void ClearMemory_FeaturePoints(CTrackingFrame* TrackingFrame, std::vector<CKeyFrame>* KeyFrame);



#endif
