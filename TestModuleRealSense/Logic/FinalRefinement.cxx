#include "FinalRefinement.h"

#include <opencv2/opencv.hpp>

#include <cstring>
#include <string>
#include <stdio.h>
#include "common.h"
#include "StereoVision.h"

#include "RigidMosaicking_PointsManagement.h"
#include "G2O_BA.h"
#include "R1PPnP.h"
//#include "ICP.h"

using namespace std;
using namespace cv;

String NumberToString(unsigned int Number)
{
	ostringstream ss;
	ss << Number;
	return ss.str();
}


void RemoveTooTiltPoints(uchar* Mask, float* disparity
						, CParams* Params
						, int W, int H)
{
	uchar* Mask_temp = new uchar[W * H];
	memcpy(Mask_temp, Mask, W * H * sizeof(uchar));

	float* gradient = new float[W * H];

	const int r = 5;	
	for (int y = 0; y < H; y++)
		for (int x = 0; x < W; x++)
		{
			if (Mask_temp[y * W + x] == 0)
			{
				gradient[y * W + x] = 1000.0;
				continue;
			}	
			if (y < r || y > H - r - 1 || x < r || x > W - r - 1)
			{
				Mask[y * W + x] = 0;
				gradient[y * W + x] = 1000.0;
				continue;
			}
			if (Mask_temp[(y - r) * W + x] == 0 || Mask_temp[(y + r) * W + x] == 0
			 || Mask_temp[y * W + x - r] == 0 || Mask_temp[y * W + x + r] == 0)
			{
				Mask[y * W + x] = 0;
				gradient[y * W + x] = 1000.0;
				continue;
			}

			float diffx = disparity[y * W + x - r] - disparity[y * W + x + r];
			float diffy = disparity[(y - r) * W + x] - disparity[(y + r) * W + x];
			gradient[y * W + x] = sqrt(diffx * diffx + diffy * diffy);
			
			if (gradient[y * W + x] > Params->RemoveTooTiltPointsThreshold)
				Mask[y * W + x] = 0;
		}

/*	cv::Mat gradientmap = cv::Mat::zeros(H, W, CV_8UC1);
	for (int y = 0; y < H; y ++)
		for (int x = 0; x < W; x ++)
	{
		gradientmap.data[y * W + x] = (uchar)(gradient[y * W + x] / 250.0 * 255);
	}
	imshow("gradientmap", gradientmap);

	cv::Mat Maskmap = cv::Mat::zeros(H, W, CV_8UC1);
	for (int y = 0; y < H; y ++)
		for (int x = 0; x < W; x ++)
	{
		Maskmap.data[y * W + x] = Mask[y * W + x];
	}
	imshow("input mask", Maskmap);
*/
		
	delete[] Mask_temp;	
	delete[] gradient;
}


void GenerateBackStereoMatchingResult(CKeyFrame* KeyFrame_this, CStereoMatchingResult* StereoMatchingResult
									, CParams* Params)
{
	int W = Params->W;
	int H = Params->H;
	memcpy(StereoMatchingResult->Mask, KeyFrame_this->Mask, W * H * sizeof(uchar));
	memcpy(StereoMatchingResult->RGB, KeyFrame_this->RGB, 3 * W * H * sizeof(uchar));
	memcpy(StereoMatchingResult->disparity, KeyFrame_this->depth, W * H * sizeof(float));
	
//	RemoveTooTiltPoints(StereoMatchingResult->Mask, StereoMatchingResult->disparity, Params, W, H);

	GenerateImgPhyCoord(StereoMatchingResult->Mask, StereoMatchingResult->disparity, StereoMatchingResult->coord,  StereoMatchingResult->Points, W, H, Params->FocalLength, Params->ImgCenterU, Params->ImgCenterV);
//	GenerateImgNormal(StereoMatchingResult->Mask, StereoMatchingResult->coord, StereoMatchingResult->normal, W, H);
}

void GenerateBackTrackingFrame(CKeyFrame* KeyFrame_this, CTrackingFrame* TrackingFrame_this)
{
	memcpy(TrackingFrame_this->dcm, KeyFrame_this->dcm , 9 * sizeof(double));
	memcpy(TrackingFrame_this->T,   KeyFrame_this->T   , 3 * sizeof(double));

	TrackingFrame_this->ORBkeypoints.clear();

	size_t FeatureCount_this = KeyFrame_this->ORBkeypoints.size();

	for (size_t i = 0; i < FeatureCount_this; i++)
	{
		TrackingFrame_this->ORBkeypoints.push_back(KeyFrame_this->ORBkeypoints.at(i));
		TrackingFrame_this->globalpid[i] = KeyFrame_this->globalpid.at(i);
		TrackingFrame_this->flag_coordisvalid[i] = 255;
		for (int l = 0; l < 3; l ++) TrackingFrame_this->coord[3 * i + l] = KeyFrame_this->coord.at(3 * i + l);
		for (int l = 0; l < 3; l ++) TrackingFrame_this->coord_live[3 * i + l] = KeyFrame_this->coord.at(3 * i + l);
		TrackingFrame_this->MatchedID[i] = -1;
	}
	
	TrackingFrame_this->Discriptor = Mat::zeros(FeatureCount_this, 32, CV_8UC1);
	memcpy(TrackingFrame_this->Discriptor.data, KeyFrame_this->Discriptor.data, FeatureCount_this * 32 * sizeof(uchar));
}

void UpdateGlobalFeatureVector(std::vector<CFeatureVector>* GlobalFeatureVector
					, CTrackingFrame* TrackingFrame_this, CTrackingFrame* TrackingFrame_other
					, int KeyFrameID)
{
	size_t FeatureCount_this = TrackingFrame_this->ORBkeypoints.size();
	
	for (size_t i = 0; i < FeatureCount_this; i++)
	{
		int MatchedID = TrackingFrame_this->MatchedID[i];	
		if (MatchedID < 0) continue;
		
		int pid = TrackingFrame_other->globalpid[MatchedID];
		
		bool flag_pidhasKeyFrameID = false;
		for (size_t j = 0; j < GlobalFeatureVector->at(pid).ObservedGlobalKeyFrameID.size(); j ++)
		{
			if (GlobalFeatureVector->at(pid).ObservedGlobalKeyFrameID.at(j) == KeyFrameID)
			{
				flag_pidhasKeyFrameID = true;
				break;			
			}
		}		
		if (flag_pidhasKeyFrameID == true) continue;
		
		GlobalFeatureVector->at(pid).ObservedGlobalKeyFrameID.push_back(KeyFrameID);
		GlobalFeatureVector->at(pid).u.push_back(TrackingFrame_this->ORBkeypoints.at(i).pt.x);
		GlobalFeatureVector->at(pid).v.push_back(TrackingFrame_this->ORBkeypoints.at(i).pt.y);
	}
}


void FinalRefinement(vtkPoints* Points, vtkPoints* Points_live, vtkCellArray* CellArray, vtkUnsignedCharArray* RGBArray, vtkFloatArray* MergingWeight
					, CStereoMatchingResult* StereoMatchingResult
					, uchar* Mask_Uncovered
					, std::vector<CFeatureVector>* GlobalFeatureVector, std::vector<CKeyFrame>* KeyFrame
					, CTrackingFrame* TrackingFrame_this, CTrackingFrame* TrackingFrame_other
					, CG2OParameters* G2OParameters
					, CParams* Params)
{
	std::cout << "FinalRefinement start!, KeyFrame->size() = " << KeyFrame->size() << std::endl;

	int H = Params->H;
	int W = Params->W;

//	BundleAdjustment_G2O(KeyFrame, GlobalFeatureVector, NULL, G2OParameters, KeyFrame->size(), Params->MinNumberOfPointsForBA, 10, false);

	// add more connections between poses and points
	for (size_t fid = 0; fid < KeyFrame->size(); fid += 2)
	{
		CKeyFrame* KeyFrame_this = &(KeyFrame->at(fid));
		GenerateBackTrackingFrame(KeyFrame_this, TrackingFrame_this);
		
		for (size_t fid2 = 0; fid2 < KeyFrame->size(); fid2 += 2)
		{
		//	if (fid - fid2 > -3 && fid - fid2 < 3) continue;
		
			CKeyFrame* KeyFrame_other = &(KeyFrame->at(fid2));
			if (GetPoseDifference(KeyFrame_this->dcm, KeyFrame_other->dcm, KeyFrame_this->T, KeyFrame_other->T)
			 > 2.0 * Params->MinPoseDifference_2_lastkeyframeForBeingKeyFrame)
				continue;

			GenerateBackTrackingFrame(KeyFrame_other, TrackingFrame_other);

			bool Flag_TrackingIsGood, Flag_IsKeyFrame; 
			MotionTracking(TrackingFrame_this, TrackingFrame_other
						, TrackingFrame_other->dcm, TrackingFrame_other->T
						, Flag_TrackingIsGood, Flag_IsKeyFrame
						, Params
						, 10000);
			
			std::cout << "fid = " << fid << "/(" << KeyFrame->size()-1 << "), fid2 = " << fid2 << ", Flag_TrackingIsGood = " << Flag_TrackingIsGood << std::endl;
			if (Flag_TrackingIsGood)
			{
				UpdateGlobalFeatureVector(GlobalFeatureVector
					, TrackingFrame_this, TrackingFrame_other
					, fid);
			}
		}
	}
	
	BundleAdjustment_G2O(KeyFrame, GlobalFeatureVector, NULL, G2OParameters, KeyFrame->size(), Params->MinNumberOfPointsForBA, 15, false);

	unsigned int lastusedkeyframeid = 0;
	for (unsigned int fid = 0; fid < KeyFrame->size(); fid ++)
	{
		CKeyFrame* KeyFrame_this = &(KeyFrame->at(fid));	
		GenerateBackStereoMatchingResult(KeyFrame_this, StereoMatchingResult, Params);

		if (fid == 0)
		{
			InitialAddPoints(Points, Points_live, CellArray, RGBArray, MergingWeight
						, StereoMatchingResult
						, W, H);
		//	VisualizationPolyData->VisualizePoly(Points, PointCount, 1);
		//	SavePolyData(VisualizationPolyData->Poly, "//home//slam-lab//work//Align_advanced//data//PolyData_inprocess.vtp");
 		}
		else
		{
			double diff = GetPoseDifference(KeyFrame_this->dcm, KeyFrame->at(lastusedkeyframeid).dcm
				, KeyFrame_this->T, KeyFrame->at(lastusedkeyframeid).T);
			//std::cout << "fid = "<< fid << ", lastusedkeyframeid = " << lastusedkeyframeid << ", diff = " << diff << std::endl;
			if (diff < 1.0 * Params->MinPoseDifference_2_lastkeyframeForBeingKeyFrame
				&& fid < KeyFrame->size() - 1) continue;				 
			lastusedkeyframeid = fid;

			GenerateLivePoints(Points, Points_live, KeyFrame_this->dcm, KeyFrame_this->T);
			MergeLivePointsAndUpdateOrigCoord(Points, Points_live, MergingWeight
				, KeyFrame_this->dcm, KeyFrame_this->T
				, StereoMatchingResult
				, Params
				, W, H);
		
			RuntimeAddNewPointsMainFunction_new(Points, Points_live, CellArray, RGBArray, MergingWeight
				, StereoMatchingResult
				, KeyFrame_this->dcm, KeyFrame_this->T
				, Params
				, W, H
				, true);
		
		/*	VisualizationPolyData->VisualizePoly(Points, PointCount, 1);
			SavePolyData(VisualizationPolyData->Poly, "//home//slam-lab//work//Align_advanced//data//PolyData_inprocess.vtp");	
					
			cv::Mat flag_stereomask_map = cv::Mat::zeros(H, W, CV_8UC1);
			for (int y = 0; y < H; y ++)
				for (int x = 0; x < W; x ++)
			{
				flag_stereomask_map.data[y * W + x] = StereoMatchingResult->Mask[y * W + x];
			}
			imshow("flag_stereomask_map", flag_stereomask_map);
			cv::Mat flag_usedbymerge_map = cv::Mat::zeros(H, W, CV_8UC1);
			for (int y = 0; y < H; y ++)
				for (int x = 0; x < W; x ++)
			{
				flag_usedbymerge_map.data[y * W + x] = StereoMatchingResult->flag_usedbymerge[y * W + x];
			}
			imshow("flag_usedbymerge_map", flag_usedbymerge_map);
			cv::Mat flag_usedbyadd_map = cv::Mat::zeros(H, W, CV_8UC1);
			for (int y = 0; y < H; y ++)
				for (int x = 0; x < W; x ++)
			{
				flag_usedbyadd_map.data[y * W + x] = StereoMatchingResult->flag_usedbyadd[y * W + x];
			}
			imshow("flag_usedbyadd_map", flag_usedbyadd_map);
			cvWaitKey(-1);
 		*/
 		}
	
	//	OldPointCount = PointCount;
		std::cout << "fid = " << fid << "/(" << KeyFrame->size()-1 << "), PointCount = " << Points->GetNumberOfPoints()  << std::endl;
	}

}


