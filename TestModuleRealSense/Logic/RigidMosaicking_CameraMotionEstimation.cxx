#include "RigidMosaicking_CameraMotionEstimation.h"
#include "RigidMosaicking_PointsManagement.h"
#include "R1PPnP.h"
#include "G2O_BA.h"
//#include "ICP.h"
//#include "omp.h"
#include <time.h>

using namespace cv;
using namespace std;

#define SWAP(a, b, temp)\
{\
	temp = a;\
	a = b;\
	b = temp;\
}\

void DetectFastCorner_kernel(uchar* img, int* CornerResponse, int MinPixelBetweenFeatures, int PixelTH, int W, int H)
{
	int x_offset[16] = {0, 1, 2, 3, 3, 3, 2, 1, 0, -1, -2, -3, -3, -3, -2, -1};
	int y_offset[16] = {-3, -3, -2, -1, 0, 1, 2, 3, 3, 3, 2, 1, 0, -1, -2, -3};
	
//	#pragma omp parallel
	{
		for (int y = MinPixelBetweenFeatures; y < H - MinPixelBetweenFeatures; y ++)
		{
//			#pragma omp for
			for (int x = MinPixelBetweenFeatures; x < W - MinPixelBetweenFeatures; x ++)
			{
				//if (y == 50) std::cout << x << std::endl;
				int pixel = img[y * W + x];

				int Num_brighter = 0;
				int Num_darker = 0;
				int Num_similar = 0;

				int BrighterResponse = 0;
				int DarkerResponse = 0;

				{
					int nx = x + x_offset[0];
					int ny = y + y_offset[0];
					int pixeldiff = img[ny * W + nx] - pixel;
		
					Num_brighter  = (pixeldiff > PixelTH) ? Num_brighter + 1 : Num_brighter;
					Num_darker 	  = (pixeldiff < -PixelTH)? Num_darker   + 1 : Num_darker;
					Num_similar   = (pixeldiff < PixelTH && pixeldiff > -PixelTH) ? Num_similar + 1 : Num_similar;
		
					BrighterResponse = (pixeldiff > PixelTH) ? BrighterResponse + (pixeldiff - PixelTH) : BrighterResponse;
					DarkerResponse   = (pixeldiff < -PixelTH)? DarkerResponse - (pixeldiff + PixelTH)   : DarkerResponse;
				}
				{
					int nx = x + x_offset[8];
					int ny = y + y_offset[8];
					int pixeldiff = img[ny * W + nx] - pixel;
		
					Num_brighter  = (pixeldiff > PixelTH) ? Num_brighter + 1 : Num_brighter;
					Num_darker 	  = (pixeldiff < -PixelTH)? Num_darker   + 1 : Num_darker;
					Num_similar   = (pixeldiff < PixelTH && pixeldiff > -PixelTH) ? Num_similar + 1 : Num_similar;

					BrighterResponse = (pixeldiff > PixelTH) ? BrighterResponse + (pixeldiff - PixelTH) : BrighterResponse;
					DarkerResponse   = (pixeldiff < -PixelTH)? DarkerResponse - (pixeldiff + PixelTH)   : DarkerResponse;
				}
				if (Num_similar > 1) continue;
	
				{
					int nx = x + x_offset[4];
					int ny = y + y_offset[4];
					int pixeldiff = img[ny * W + nx] - pixel;
		
					Num_brighter  = (pixeldiff > PixelTH) ? Num_brighter + 1 : Num_brighter;
					Num_darker 	  = (pixeldiff < -PixelTH)? Num_darker   + 1 : Num_darker;
				//	Num_similar   = (pixeldiff < PixelTH && pixeldiff > -PixelTH) ? Num_similar + 1 : Num_similar;

					BrighterResponse = (pixeldiff > PixelTH) ? BrighterResponse + (pixeldiff - PixelTH) : BrighterResponse;
					DarkerResponse   = (pixeldiff < -PixelTH)? DarkerResponse - (pixeldiff + PixelTH)   : DarkerResponse;
				}
				{
					int nx = x + x_offset[12];
					int ny = y + y_offset[12];
					int pixeldiff = img[ny * W + nx] - pixel;
		
					Num_brighter  = (pixeldiff > PixelTH) ? Num_brighter + 1 : Num_brighter;
					Num_darker 	  = (pixeldiff < -PixelTH)? Num_darker   + 1 : Num_darker;
				//	Num_similar   = (pixeldiff < PixelTH && pixeldiff > -PixelTH) ? Num_similar + 1 : Num_similar;

					BrighterResponse = (pixeldiff > PixelTH) ? BrighterResponse + (pixeldiff - PixelTH) : BrighterResponse;
					DarkerResponse   = (pixeldiff < -PixelTH)? DarkerResponse - (pixeldiff + PixelTH)   : DarkerResponse;
				}
				if (Num_brighter < 3 && Num_darker < 3) continue;

				for (int i = 0; i < 4; i ++)
					for (int j = 1; j < 4; j ++)
				{
					int nx = x + x_offset[i * 4 + j];
					int ny = y + y_offset[i * 4 + j];
		
					int pixeldiff = img[ny * W + nx] - pixel;
	
					Num_brighter  = (pixeldiff > PixelTH) ? Num_brighter + 1 : Num_brighter;
					Num_darker 	  = (pixeldiff < -PixelTH)? Num_darker   + 1 : Num_darker;
				//	Num_similar   = (pixeldiff < PixelTH && pixeldiff > -PixelTH) ? Num_similar + 1 : Num_similar;

					BrighterResponse = (pixeldiff > PixelTH) ? BrighterResponse + (pixeldiff - PixelTH) : BrighterResponse;
					DarkerResponse   = (pixeldiff < -PixelTH)? DarkerResponse - (pixeldiff + PixelTH)   : DarkerResponse;
				}
				if (Num_brighter > 11 )
				{
					CornerResponse[y * W + x] = BrighterResponse;
					continue;
				}	
				if (Num_darker > 11)
				{
					CornerResponse[y * W + x] = DarkerResponse;
					continue;
				}
			}	
		}
	}
}

void CleanCornerResponse(int* CornerResponse, int* CornerResponse_temp
									, int MinPixelBetweenFeatures
									, int W, int H)
{
//	#pragma omp parallel
	{
		for (int y = MinPixelBetweenFeatures; y < H - MinPixelBetweenFeatures; y += 1)
		{
//			#pragma omp for
			for (int x = MinPixelBetweenFeatures; x < W - MinPixelBetweenFeatures; x += 1)
			{
				int thisresponse = CornerResponse[y * W + x];
				if (thisresponse < 0) continue;
	
				for (int y_offset = -MinPixelBetweenFeatures; y_offset <= MinPixelBetweenFeatures; y_offset ++)
					for (int x_offset = -MinPixelBetweenFeatures; x_offset <= MinPixelBetweenFeatures; x_offset ++)
					{
						int ny = y + y_offset;
						int nx = x + x_offset;			
						if (CornerResponse_temp[ny * W + nx] > thisresponse)
						{
							CornerResponse[y * W + x] = -1;
							continue;
						}
					}
			}
		}
	}	
}

void DetectORBFeaturesAndGenerateTrackingFrameKeyPoints(CTrackingFrame* TrackingFrame_this
							, CParams* Params
							, int W, int H)
{
	int* CornerResponseMap = new int[W * H];
	int* CornerResponseMap_temp = new int[W * H];
	
	memset(CornerResponseMap, -1, W * H * sizeof(int));
	DetectFastCorner_kernel(TrackingFrame_this->img->data, CornerResponseMap
					, Params->MinPixelBetweenFeatures, Params->PixelTH, W, H);

	memcpy(CornerResponseMap_temp, CornerResponseMap, W * H * sizeof(int));
	CleanCornerResponse(CornerResponseMap, CornerResponseMap_temp, Params->MinPixelBetweenFeatures, W, H);

	TrackingFrame_this->ORBkeypoints.clear();
	for (int y = 0; y < H; y ++)
		for (int x = 0; x < W; x ++)
	{
		int response_this = CornerResponseMap[y * W + x];
		if (response_this < 0.001) continue;	
	
		cv::KeyPoint keypoint_this((float)x, (float)y, 1.0);
		keypoint_this.response = response_this;
		TrackingFrame_this->ORBkeypoints.push_back(keypoint_this);
	}
	
	delete[] CornerResponseMap;
	delete[] CornerResponseMap_temp;
}

//								, Ptr<FeatureDetector>* ORBdetector, Ptr<DescriptorExtractor>* ORBextractor

int GenerateTrackingFrame_from_OpenCVDetection(CTrackingFrame* TrackingFrame_this
								, CStereoMatchingResult* StereoMatchingResult
								, CParams* Params
								, int W, int H)
{
	DetectORBFeaturesAndGenerateTrackingFrameKeyPoints(TrackingFrame_this, Params, W, H);
//	(*ORBdetector)->detect(*(TrackingFrame_this->img), TrackingFrame_this->ORBkeypoints);	

//	imwrite("C://work_3Dreconstruction//IntelRealsenseReconstruction//data//temp.jpg", *(TrackingFrame_this->img));
	while (TrackingFrame_this->ORBkeypoints.size() > Params->MaxNumberOfFeaturePointsPerFrame)
		TrackingFrame_this->ORBkeypoints.pop_back();
	
	{
//		std::cout << "TrackingFrame_this->ORBkeypoints.size() = " << TrackingFrame_this->ORBkeypoints.size() << std::endl;
//		Mat img_keypoints;
//		drawKeypoints(*(TrackingFrame_this->img), TrackingFrame_this->ORBkeypoints, img_keypoints);
//		imshow("img_keypoints", img_keypoints);
//		cvWaitKey(1);
//		imwrite( "..//images//img_keypoints.jpg", img_keypoints );
	}
	
    cv::Ptr<cv::DescriptorExtractor> ORBextractor_new = cv::ORB::create();
 	//(*ORBextractor)->compute(*(TrackingFrame_this->img), TrackingFrame_this->ORBkeypoints, TrackingFrame_this->Discriptor);
	ORBextractor_new->compute(*(TrackingFrame_this->img), TrackingFrame_this->ORBkeypoints, TrackingFrame_this->Discriptor);
	
//	TrackingFrame->flag_coordisvalid.resize(TrackingFrame->ORBkeypoints.size());
//	TrackingFrame->coord.resize(3 * TrackingFrame->ORBkeypoints.size());
//	TrackingFrame->coord_live.resize(3 * TrackingFrame->ORBkeypoints.size());	
//	TrackingFrame->MatchedID.resize(TrackingFrame->ORBkeypoints.size());
//	TrackingFrame->globalpid.resize(TrackingFrame->ORBkeypoints.size());

	int Valide3DFeatureCount = 0;
	for (size_t i = 0; i < TrackingFrame_this->ORBkeypoints.size(); i ++)
	{
		if (i >= Params->MaxNumberOfFeaturePointsPerFrame) break;
		
		int x = std::round(TrackingFrame_this->ORBkeypoints.at(i).pt.x);
		int y = std::round(TrackingFrame_this->ORBkeypoints.at(i).pt.y);

		TrackingFrame_this->MatchedID[i] = -1;
		TrackingFrame_this->globalpid[i] = -1;		
		TrackingFrame_this->localpid[i] = -1;		
		TrackingFrame_this->flag_coordisvalid[i] = 0;		
		if (StereoMatchingResult->Mask[y * W + x] > 0)
		{
			TrackingFrame_this->flag_coordisvalid[i] = 255;
			for (int l = 0; l < 3; l ++) TrackingFrame_this->coord[3 * i + l] = StereoMatchingResult->coord[3 * (y * W + x) + l];
			for (int l = 0; l < 3; l ++) TrackingFrame_this->coord_live[3 * i + l] = StereoMatchingResult->coord[3 * (y * W + x) + l];
			Valide3DFeatureCount ++;
		}
	//	printf("i = %d, u = %d, v = %d, flag_coordisvalid = %d, coord = %.3f %.3f %.3f, coord_live = %.3f %.3f %.3f\n", i, x, y, TrackingFrame_this->flag_coordisvalid[i], TrackingFrame_this->coord[3 * i + 0], TrackingFrame_this->coord[3 * i + 1], TrackingFrame_this->coord[3 * i + 2], TrackingFrame_this->coord_live[3 * i + 0], TrackingFrame_this->coord_live[3 * i + 1], TrackingFrame_this->coord_live[3 * i + 2]);		
	}
	
	return Valide3DFeatureCount;
}



void ClearGlobalFeaturesAndKeyFrames(std::vector<CFeatureVector>* GlobalFeatureVector
									, std::vector<CKeyFrame>* KeyFrame)
{
	for (unsigned int i = 0; i < KeyFrame->size(); i ++)
	{
		KeyFrame->at(i).globalpid.clear();
		KeyFrame->at(i).ORBkeypoints.clear();
		KeyFrame->at(i).coord.clear();
		delete[] KeyFrame->at(i).Mask;
		delete[] KeyFrame->at(i).RGB;
		delete[] KeyFrame->at(i).depth;		
	}		
	KeyFrame->clear();

	for (unsigned int i = 0; i < GlobalFeatureVector->size(); i ++)
	{
		GlobalFeatureVector->at(i).ObservedGlobalKeyFrameID.clear();
		GlobalFeatureVector->at(i).u.clear();
		GlobalFeatureVector->at(i).v.clear();
	}	
	GlobalFeatureVector->clear();
}


void GenerateMatchedID_from_OpenCVMatching(CTrackingFrame* TrackingFrame_this, CTrackingFrame* TrackingFrame_prev
	, vector<DMatch>* matches, CParams* Params)
{
	for (size_t i = 0; i < matches->size(); i++)
	{
		size_t pid_this = matches->at(i).queryIdx;
		size_t pid_prev = matches->at(i).trainIdx;

		if (pid_this >= TrackingFrame_this->ORBkeypoints.size() || pid_prev >= TrackingFrame_prev->ORBkeypoints.size()) continue;
		if (pid_this >= Params->MaxNumberOfFeaturePointsPerFrame || pid_prev >= Params->MaxNumberOfFeaturePointsPerFrame) continue;

		if (TrackingFrame_prev->flag_coordisvalid[pid_prev] != 255)
		{
			TrackingFrame_this->MatchedID[pid_this] = -1;
			continue;
		}
		TrackingFrame_this->MatchedID[pid_this] = pid_prev;
	}
}

size_t FeatureMatching(CTrackingFrame* TrackingFrame_this, CTrackingFrame* TrackingFrame_prev
				, CParams* Params)
{
	std::vector< cv::DMatch > matches;
	// step 1
    cv::BFMatcher ORBmatcher_new(cv::NORM_HAMMING, true);
 	//ORBmatcher->match(TrackingFrame_this->Discriptor, TrackingFrame_prev->Discriptor, matches);
 	ORBmatcher_new.match(TrackingFrame_this->Discriptor, TrackingFrame_prev->Discriptor, matches);
	// step 2
	std::vector< cv::DMatch > matches_smalldistance;
	for (unsigned int i = 0; i < matches.size(); i++)
	{
		if (matches.at(i).distance > 100) continue;
		matches_smalldistance.push_back(matches.at(i));
	}
	
	GenerateMatchedID_from_OpenCVMatching(TrackingFrame_this, TrackingFrame_prev, &matches_smalldistance, Params);
	return matches_smalldistance.size();
}


void GenerateMatchedID_from_OpenCVMatching(CTrackingFrame* TrackingFrame_this, CKeyFrame* KeyFrame_recent
	, vector<DMatch>* matches, CParams* Params)
{
	for (size_t i = 0; i < matches->size(); i++)
	{
		size_t pid_this = matches->at(i).queryIdx;
		size_t pid_prev = matches->at(i).trainIdx;

		if (pid_this >= TrackingFrame_this->ORBkeypoints.size() || pid_prev >= KeyFrame_recent->ORBkeypoints.size()) continue;
		if (pid_this >= Params->MaxNumberOfFeaturePointsPerFrame || pid_prev >= Params->MaxNumberOfFeaturePointsPerFrame) continue;

		TrackingFrame_this->MatchedID[pid_this] = pid_prev;
		TrackingFrame_this->globalpid[pid_this] = KeyFrame_recent->globalpid.at(pid_prev);
	}
}


void GetExistingGlobalFeaturePidforTrackingFrameThis(CTrackingFrame* TrackingFrame_this, CTrackingFrame* TrackingFrame_prev, CParams* Params)
{
	for (size_t i = 0; i < TrackingFrame_this->ORBkeypoints.size(); i++)
	{
		if (i >= Params->MaxNumberOfFeaturePointsPerFrame) break;
		int MatchedID = TrackingFrame_this->MatchedID[i];
		if (MatchedID < 0) continue;
		if (TrackingFrame_prev->globalpid[MatchedID] < 0) continue;

		TrackingFrame_this->globalpid[i] = TrackingFrame_prev->globalpid[MatchedID];
	}
}

void GetExistingGlobalFeaturePidforTrackingFrameThis(CTrackingFrame* TrackingFrame_this, CKeyFrame* KeyFrame_recent, CParams* Params)
{
	for (size_t i = 0; i < TrackingFrame_this->ORBkeypoints.size(); i++)
	{
	//	std::cout << i << "MatchedID = " << TrackingFrame_this->MatchedID[i] << std::endl;
	
		if (i >= Params->MaxNumberOfFeaturePointsPerFrame) break;
		int MatchedID = TrackingFrame_this->MatchedID[i];
		if (MatchedID < 0) continue;
	//	if (TrackingFrame_prev->globalpid[MatchedID] < 0) continue;

		TrackingFrame_this->globalpid[i] = KeyFrame_recent->globalpid[MatchedID];
	}
}



void AddNewGlobalFeatureVectorAndKeyFrame_runtime(CTrackingFrame* TrackingFrame_this
	, std::vector<CFeatureVector>* GlobalFeatureVector
	, std::vector<CKeyFrame>* KeyFrame
	, CParams* Params)
{
	CKeyFrame KeyFrame_this;
	int CurrentFrameID = KeyFrame->size();

	memcpy(KeyFrame_this.dcm, TrackingFrame_this->dcm, 9 * sizeof(double));
	memcpy(KeyFrame_this.T, TrackingFrame_this->T, 3 * sizeof(double));

	int ValidCount = 0;
	for (size_t i = 0; i < TrackingFrame_this->ORBkeypoints.size(); i++)
	{
		if (i >= Params->MaxNumberOfFeaturePointsPerFrame) break;
		if (TrackingFrame_this->flag_coordisvalid[i] != 255) continue;
		
		//	int MatchedID = TrackingFrame_this->MatchedID[i];
		int pid = TrackingFrame_this->globalpid[i];

		if (pid > -1) // if this feature point already exists in the global feature points vector
		{
			GlobalFeatureVector->at(pid).ObservedGlobalKeyFrameID.push_back(CurrentFrameID);
			GlobalFeatureVector->at(pid).u.push_back(TrackingFrame_this->ORBkeypoints.at(i).pt.x);
			GlobalFeatureVector->at(pid).v.push_back(TrackingFrame_this->ORBkeypoints.at(i).pt.y);

			KeyFrame_this.globalpid.push_back(pid);
			//KeyFrame_this.u.push_back(TrackingFrame_this->ORBkeypoints.at(i).pt.x);
			//KeyFrame_this.v.push_back(TrackingFrame_this->ORBkeypoints.at(i).pt.y);
		}
		else
		{
			CFeatureVector FeatureVector_this;
			for (int l = 0; l < 3; l++)  FeatureVector_this.coord[l] = TrackingFrame_this->coord[3 * i + l];
			for (int l = 0; l < 32; l++) FeatureVector_this.Discriptor[l] = TrackingFrame_this->Discriptor.data[32 * i + l];
			FeatureVector_this.ObservedGlobalKeyFrameID.push_back(CurrentFrameID);
			FeatureVector_this.u.push_back(TrackingFrame_this->ORBkeypoints.at(i).pt.x);
			FeatureVector_this.v.push_back(TrackingFrame_this->ORBkeypoints.at(i).pt.y);
			GlobalFeatureVector->push_back(FeatureVector_this);

			pid = GlobalFeatureVector->size() - 1;
			KeyFrame_this.globalpid.push_back(pid);
			//KeyFrame_this.u.push_back(TrackingFrame_this->ORBkeypoints.at(i).pt.x);
			//KeyFrame_this.v.push_back(TrackingFrame_this->ORBkeypoints.at(i).pt.y);

			TrackingFrame_this->globalpid[i] = pid;
		}
		KeyFrame_this.ORBkeypoints.push_back(TrackingFrame_this->ORBkeypoints.at(i));
		for (int l = 0; l < 3; l ++) KeyFrame_this.coord.push_back(TrackingFrame_this->coord[3 * i + l]);
		
		//printf("i = %d, ValidCount = %d, coord = %.2f %.2f %.2f\n", i, ValidCount, TrackingFrame_this->coord[3 * i + 0], TrackingFrame_this->coord[3 * i + 1], TrackingFrame_this->coord[3 * i + 2]);
		ValidCount ++;
	}

	KeyFrame_this.Discriptor = Mat::zeros(ValidCount, 32, CV_8UC1);
	ValidCount = 0;
	for (size_t i = 0; i < TrackingFrame_this->ORBkeypoints.size(); i++)
	{
		if (i >= Params->MaxNumberOfFeaturePointsPerFrame) break;
		if (TrackingFrame_this->flag_coordisvalid[i] != 255) continue;
		
		for (int l = 0; l < 32; l ++)
			KeyFrame_this.Discriptor.data[32 * ValidCount + l] = TrackingFrame_this->Discriptor.data[32 * i + l];

		ValidCount ++;
	}
	
//	memcpy(KeyFrame_this.Discriptor.data, TrackingFrame_this->Discriptor.data, TrackingFrame_this->ORBkeypoints.size() * 32 * sizeof(uchar));

	KeyFrame->push_back(KeyFrame_this);
//	printf("Numbe of Frame = %ld, KeyFrame_this.globalpid = %ld,  GlobalFeatureVectorSize = %ld\n", KeyFrame->size(), KeyFrame_this.globalpid.size(), GlobalFeatureVector->size());
}


void AddRGBandDepth2RecentKeyFrame_runtime(std::vector<CKeyFrame>* KeyFrame
	, CStereoMatchingResult* StereoMatchingResult
	, int W, int H)
{
	if (KeyFrame->size() == 0) return;

	CKeyFrame* KeyFrame_recent = &(KeyFrame->at(KeyFrame->size() - 1));

	KeyFrame_recent->Mask = new uchar[W * H];
	KeyFrame_recent->RGB = new uchar[3 * W * H];
	KeyFrame_recent->depth = new float[W * H];
	
	memcpy(KeyFrame_recent->Mask, StereoMatchingResult->Mask, W * H * sizeof(uchar));
	memcpy(KeyFrame_recent->RGB, StereoMatchingResult->RGB, 3 * W * H * sizeof(uchar));
	memcpy(KeyFrame_recent->depth, StereoMatchingResult->disparity, W * H * sizeof(float));
}


void UpdateMatchingInliers_FromPnPResults(CTrackingFrame* TrackingFrame_this
	, CTrackingFrame* TrackingFrame_prev
	, double inlierErrorPixelTH
	, double FocalLength, double ImgCenterU, double ImgCenterV
	, unsigned int MaxNumberOfFeaturePointsPerFrame)
{
	for (size_t i = 0; i < TrackingFrame_this->ORBkeypoints.size(); i++)
	{
		if (i >= MaxNumberOfFeaturePointsPerFrame) break;
		int MatchedID = TrackingFrame_this->MatchedID[i];
		if (MatchedID < 0) continue;

		if (TrackingFrame_prev->flag_coordisvalid[MatchedID] != 255)
		{
			TrackingFrame_this->MatchedID[i] = -1;
			continue;
		}

		double phycoord[3], phycoord_live[3], coord_reproj[2];
		for (int l = 0; l < 3; l++) phycoord[l] = TrackingFrame_prev->coord[3 * MatchedID + l];

		DCMT_OrigCoord_2_LiveCoord(TrackingFrame_this->dcm, TrackingFrame_this->T, phycoord, phycoord_live);
		coord_reproj[0] = phycoord_live[0] / phycoord_live[2] * FocalLength + ImgCenterU; //  * FocalLengthSuppliment
		coord_reproj[1] = phycoord_live[1] / phycoord_live[2] * FocalLength + ImgCenterV; //  * FocalLengthSuppliment

		double projdiff[2];
		projdiff[0] = coord_reproj[0] - TrackingFrame_this->ORBkeypoints.at(i).pt.x;//u[i];
		projdiff[1] = coord_reproj[1] - TrackingFrame_this->ORBkeypoints.at(i).pt.y;//v[i];

		double cost = sqrt(projdiff[0] * projdiff[0] + projdiff[1] * projdiff[1]);

		//	printf("i = %d, coord_reproj = %.2f %.2f, realuv = %.2f, %.2f, cost = %.3f\n", i, coord_reproj[0], coord_reproj[1], TrackingFrame_this->u[i], TrackingFrame_this->v[i], cost);
		if (cost > inlierErrorPixelTH) TrackingFrame_this->MatchedID[i] = -1;
	}
}

void UpdateMatchingInliers_FromPnPResults(CTrackingFrame* TrackingFrame_this
	, CKeyFrame* KeyFrame_recent
	, double inlierErrorPixelTH
	, double FocalLength, double ImgCenterU, double ImgCenterV
	, unsigned int MaxNumberOfFeaturePointsPerFrame)
{
	for (size_t i = 0; i < TrackingFrame_this->ORBkeypoints.size(); i++)
	{
		if (i >= MaxNumberOfFeaturePointsPerFrame) break;
		int MatchedID = TrackingFrame_this->MatchedID[i];
		if (MatchedID < 0) continue;

		double phycoord[3], phycoord_live[3], coord_reproj[2];
		for (int l = 0; l < 3; l++) phycoord[l] = KeyFrame_recent->coord[3 * MatchedID + l];

		DCMT_OrigCoord_2_LiveCoord(TrackingFrame_this->dcm, TrackingFrame_this->T, phycoord, phycoord_live);
		coord_reproj[0] = phycoord_live[0] / phycoord_live[2] * FocalLength + ImgCenterU; //  * FocalLengthSuppliment
		coord_reproj[1] = phycoord_live[1] / phycoord_live[2] * FocalLength + ImgCenterV; //  * FocalLengthSuppliment

		double projdiff[2];
		projdiff[0] = coord_reproj[0] - TrackingFrame_this->ORBkeypoints.at(i).pt.x;//u[i];
		projdiff[1] = coord_reproj[1] - TrackingFrame_this->ORBkeypoints.at(i).pt.y;//v[i];

		double cost = sqrt(projdiff[0] * projdiff[0] + projdiff[1] * projdiff[1]);

		//	printf("i = %d, coord_reproj = %.2f %.2f, realuv = %.2f, %.2f, cost = %.3f\n", i, coord_reproj[0], coord_reproj[1], TrackingFrame_this->u[i], TrackingFrame_this->v[i], cost);
		if (cost > inlierErrorPixelTH) TrackingFrame_this->MatchedID[i] = -1;
	}
}


void GenerateOrigCoord_TrackingFrame(CTrackingFrame* TrackingFrame_this, unsigned int MaxNumberOfFeaturePointsPerFrame)
{
	for (size_t i = 0; i < TrackingFrame_this->ORBkeypoints.size(); i++)
	{
		if (i >= MaxNumberOfFeaturePointsPerFrame) break;
		if (TrackingFrame_this->flag_coordisvalid[i] != 255) continue;

		double coord[3], coord_live[3];
		for (int l = 0; l < 3; l++) coord_live[l] = TrackingFrame_this->coord_live[3 * i + l];

		DCMT_LiveCoord_2_OrigCoord(TrackingFrame_this->dcm, TrackingFrame_this->T, coord_live, coord);
		for (int l = 0; l < 3; l++) TrackingFrame_this->coord[3 * i + l] = coord[l];
	}
}

//								, cv::BFMatcher* ORBmatcher

int MotionTracking(CTrackingFrame* TrackingFrame_this, CTrackingFrame* TrackingFrame_prev
								, double dcm_lastKeyFrame[9], double T_lastKeyFrame[3]
								, bool& Flag_TrackingIsGood, bool& Flag_IsKeyFrame
								, CParams* Params								
								, int NumberOfTrackingLostFrame)
{
//	clock_t t = clock();
	FeatureMatching(TrackingFrame_this, TrackingFrame_prev, Params);
//	t = clock() - t;

//	Initial_DCM_T_PnP(Node->dcm, Node->T);
	memcpy(TrackingFrame_this->dcm, TrackingFrame_prev->dcm, 9 * sizeof(double));
	memcpy(TrackingFrame_this->T  , TrackingFrame_prev->T  , 3 * sizeof(double));

//	t = clock();
	unsigned int NumofGoodMatches = 0;
	double GoodMatchesRate = 0.0;				
	double meanerror = 
	R1PPnP(TrackingFrame_this, TrackingFrame_prev
				, TrackingFrame_this->dcm, TrackingFrame_this->T
				, NumofGoodMatches, GoodMatchesRate
				, Params->inlierErrorPixelTH
				, Params->FocalLength, Params->ImgCenterU, Params->ImgCenterV
				, Params->MaxNumberOfFeaturePointsPerFrame);
				
//	t = clock() - t;
//	printf("time R1PPnP = %.2f(ms)\n", 1000.0 * (float)t / CLOCKS_PER_SEC);
	
/*	printf("dcm_PnP = %.3f %.3f %.3f\n", TrackingFrame_this->dcm[0], TrackingFrame_this->dcm[1], TrackingFrame_this->dcm[2]);			
	printf("dcm_PnP = %.3f %.3f %.3f\n", TrackingFrame_this->dcm[3], TrackingFrame_this->dcm[4], TrackingFrame_this->dcm[5]);			
	printf("dcm_PnP = %.3f %.3f %.3f\n", TrackingFrame_this->dcm[6], TrackingFrame_this->dcm[7], TrackingFrame_this->dcm[8]);			
	printf("T_PnP = %.3f %.3f %.3f\n", TrackingFrame_this->T[0], TrackingFrame_this->T[1], TrackingFrame_this->T[2]);	
*/

	double PoseDifference_2_previousframe = GetPoseDifference(TrackingFrame_this->dcm, TrackingFrame_prev->dcm, TrackingFrame_this->T, TrackingFrame_prev->T);
	double PoseDifference_2_lastkeyframe  = GetPoseDifference(TrackingFrame_this->dcm, dcm_lastKeyFrame, TrackingFrame_this->T, T_lastKeyFrame);
	
//	*Flag_TooCloseToPreviousFrame = (NumofGoodMatches > 50 && PoseDifference_2_previousframe < 0.01)  ? true : false;
//|| (GoodMatchesRate > Params->MinGoodRateForGoodTracking && NumofGoodMatches > 15)

	if (NumberOfTrackingLostFrame < 5) // normal tracking
	{
		Flag_TrackingIsGood     = ( NumofGoodMatches > Params->MinNumberOfGoodMatchesForGoodTracking
						 && PoseDifference_2_previousframe < Params->MaxPoseDifference_2_previousframeForGoodTracking) ? true : false;
	}					 
	else // try to get back the tracking
	{
		Flag_TrackingIsGood     = ( NumofGoodMatches > Params->MinNumberOfGoodMatchesForGoodTracking
						 && PoseDifference_2_previousframe < 2.0 * Params->MaxPoseDifference_2_previousframeForGoodTracking) ? true : false;
	}
	
	Flag_IsKeyFrame              = (Flag_TrackingIsGood == true && PoseDifference_2_lastkeyframe > Params->MinPoseDifference_2_lastkeyframeForBeingKeyFrame) ? true : false;

//	printf("PoseDifference_2_previousframe = %.2f, PoseDifference_2_lastkeyframe = %.2f\n", PoseDifference_2_previousframe, PoseDifference_2_lastkeyframe);

	std::cout << "NumofGoodMatches = " << NumofGoodMatches << ", GoodMatchesRate = " << 100.0 * GoodMatchesRate << "%" << std::endl;
//	printf("Flag_TrackingIsGood = %d, Flag_IsKeyFrame = %d\n", *Flag_TrackingIsGood, *Flag_IsKeyFrame);
	
//	*Flag_TrackingIsGood = (GoodMatchesRate < 0.3) ? false : true;
	if (Flag_TrackingIsGood == true )
	{
		UpdateMatchingInliers_FromPnPResults(TrackingFrame_this, TrackingFrame_prev, Params->inlierErrorPixelTH, Params->FocalLength, Params->ImgCenterU, Params->ImgCenterV, Params->MaxNumberOfFeaturePointsPerFrame);									
		GenerateOrigCoord_TrackingFrame(TrackingFrame_this, Params->MaxNumberOfFeaturePointsPerFrame);			
	}
	return NumofGoodMatches;
}




/////////////////
//, std::vector<CLocalFeatureVector>* LocalFeatureVector_SinceLastKeyFrame, std::vector<CLocalFrame>* LocalFrame_SinceLastKeyFrame

void ResetRigidMosaicking(std::vector<CFeatureVector>* GlobalFeatureVector, std::vector<CKeyFrame>* KeyFrame
			, vtkPoints* Points, vtkPoints* Points_live, vtkCellArray* CellArray, vtkUnsignedCharArray* RGBArray, vtkFloatArray* MergingWeight)
{
	ClearGlobalFeaturesAndKeyFrames(GlobalFeatureVector, KeyFrame);		
//	ClearLocalFeaturesAndFrames(LocalFeatureVector_SinceLastKeyFrame, LocalFrame_SinceLastKeyFrame);	
	Points->Reset();		
	Points_live->Reset();
	CellArray->Reset();
	RGBArray->Reset();
	MergingWeight->Reset();
			
}
//	, std::vector<CLocalFeatureVector>* LocalFeatureVector_SinceLastKeyFrame, std::vector<CLocalFrame>* LocalFrame_SinceLastKeyFrame
//					, cv::Ptr<cv::FeatureDetector>* ORBdetector, cv::Ptr<cv::DescriptorExtractor>* ORBextractor, cv::BFMatcher* ORBmatcher

void RigidMosaicking(CStereoMatchingResult* StereoMatchingResult, cv::Mat* img1
					, vtkPolyData* Poly, vtkPoints* Points, vtkPoints* Points_live, vtkCellArray* CellArray, vtkUnsignedCharArray* RGBArray, vtkFloatArray* MergingWeight
					, uchar* Mask_Uncovered
					, CTrackingFrame* TrackingFrame0, CTrackingFrame* TrackingFrame1, CTrackingFrame*& TrackingFrame_this, CTrackingFrame*& TrackingFrame_prev
					, bool& Flag_TrackingIsGood, bool& Flag_IsKeyFrame, bool& Flag_TrackingIsGood_last, bool& Flag_IsKeyFrame_last
					, std::vector<CFeatureVector>* GlobalFeatureVector, std::vector<CKeyFrame>* KeyFrame
					, CG2OParameters* G2OParameters
					, CParams* Params
					, unsigned int frameid, unsigned int& LastKeyFrameID, int& NumberOfTrackingLostFrame)
{
	int W = Params->W;
	int H = Params->H;
	bool flag_firstframe = false;
	//if (Points->GetNumberOfPoints() == 0)
	if (KeyFrame->size() == 0)
	{
		ResetRigidMosaicking(GlobalFeatureVector, KeyFrame
				, Points, Points_live, CellArray, RGBArray, MergingWeight);
		flag_firstframe = true;
	}
	
	if (flag_firstframe)
	{
		TrackingFrame_this = TrackingFrame0;//&(TrackingFrame[0]);
		TrackingFrame_prev = TrackingFrame1;//&(TrackingFrame[1]);
		memset((TrackingFrame_this)->dcm, 0, 9 * sizeof(double));
		for (int l = 0; l < 3; l++) (TrackingFrame_this)->dcm[3 * l + l] = 1.0;
		memset((TrackingFrame_this)->T, 0, 3 * sizeof(double));
	}
	else
	{
		CTrackingFrame* TrackingFrame_for_swap;
		if (Flag_TrackingIsGood_last) SWAP(TrackingFrame_this, TrackingFrame_prev, TrackingFrame_for_swap);
	}
	//TrackingFrame_this = TrackingFrame0;

 // initial data managment
	if (flag_firstframe)
	{
		InitialAddPoints(Points, Points_live, CellArray, RGBArray, MergingWeight
					, StereoMatchingResult
					, W, H);	
	}

//	clock_t t = clock();
	// feature points detection and set them to TrackingFrame_this
//									, ORBdetector, ORBextractor
	int Valide3DFeatureCount
		 = GenerateTrackingFrame_from_OpenCVDetection(TrackingFrame_this
									, StereoMatchingResult
									, Params
									, W, H);
									
	if (Valide3DFeatureCount < Params->MinNumberOfGoodMatchesForGoodTracking)
	{
		std::cout << "Only detect " << Valide3DFeatureCount << " features in this image, should be at least " << Params->MinNumberOfGoodMatchesForGoodTracking << "!" << Valide3DFeatureCount << std::endl;
		
		Flag_IsKeyFrame = false;
		Flag_TrackingIsGood = false;
		Flag_TrackingIsGood_last = Flag_TrackingIsGood;
		Flag_IsKeyFrame_last = Flag_IsKeyFrame;
		return;
	}
//	t = clock() - t;
//	printf("time GenerateTrackingFrame_from_OpenCVDetection = %.2f(ms)\n", 1000.0 * (float)t / CLOCKS_PER_SEC);

//	t = clock();
	// runtime matching and PnP
//	std::cout << "flag_firstframe = " << flag_firstframe << std::endl;
	Flag_TrackingIsGood = false;
	Flag_IsKeyFrame = false;
	if (flag_firstframe)
	{
		Flag_IsKeyFrame = true;
		Flag_TrackingIsGood = true;
	}
	else
	{
		MotionTracking(TrackingFrame_this, TrackingFrame_prev
					, KeyFrame->at(KeyFrame->size()-1).dcm, KeyFrame->at(KeyFrame->size()-1).T
					, Flag_TrackingIsGood, Flag_IsKeyFrame
					, Params
					, NumberOfTrackingLostFrame);	
//					, ORBmatcher
	}		

	if (Flag_TrackingIsGood && frameid - LastKeyFrameID > Params->MaxAllowedFrameBetweenKeyFrames) Flag_IsKeyFrame = true;				
	if (Flag_TrackingIsGood) NumberOfTrackingLostFrame = 0;
	else NumberOfTrackingLostFrame ++;
		
//	t = clock();
	// global feature points and key frames
	if (Flag_TrackingIsGood)
	{
		GetExistingGlobalFeaturePidforTrackingFrameThis(TrackingFrame_this, TrackingFrame_prev, Params); //
		if (Flag_IsKeyFrame)
		{
			AddNewGlobalFeatureVectorAndKeyFrame_runtime(TrackingFrame_this, GlobalFeatureVector, KeyFrame, Params);
			AddRGBandDepth2RecentKeyFrame_runtime(KeyFrame, StereoMatchingResult, W, H);
			
			LastKeyFrameID = frameid;
			// Bundle Adjustment			
			BundleAdjustment_G2O(KeyFrame, GlobalFeatureVector, TrackingFrame_this, G2OParameters, Params->MaxNumberOfKeyFramesForBA, Params->MinNumberOfPointsForBA, Params->BAIterNumber, true);
		}
	}
	
/*	// ICP
	if (0 && Flag_IsKeyFrame)
	{
		double dcm_icp[9], t_icp[3];
		ICP(Poly, StereoMatchingResult->Poly
			, TrackingFrame_this->dcm, TrackingFrame_this->T
			, dcm_icp, t_icp);
		
		printf("%.2f %.2f %.2f   %.2f %.2f %.2f\n", TrackingFrame_this->dcm[0], TrackingFrame_this->dcm[1], TrackingFrame_this->dcm[2], dcm_icp[0], dcm_icp[1], dcm_icp[2]);
		printf("%.2f %.2f %.2f   %.2f %.2f %.2f\n", TrackingFrame_this->dcm[3], TrackingFrame_this->dcm[4], TrackingFrame_this->dcm[5], dcm_icp[3], dcm_icp[4], dcm_icp[5]);
		printf("%.2f %.2f %.2f   %.2f %.2f %.2f\n", TrackingFrame_this->dcm[6], TrackingFrame_this->dcm[7], TrackingFrame_this->dcm[8], dcm_icp[6], dcm_icp[7], dcm_icp[8]);
		printf("%.2f %.2f %.2f   %.2f %.2f %.2f\n", TrackingFrame_this->T[0], TrackingFrame_this->T[1], TrackingFrame_this->T[2], t_icp[0], t_icp[1], t_icp[2]);
		double diff_icp2pnp = GetPoseDifference(TrackingFrame_prev->dcm, dcm_icp, TrackingFrame_prev->T, t_icp);
		std::cout << "diff_icp2pnp = " << diff_icp2pnp << std::endl;
	
		memcpy(TrackingFrame_this->dcm, dcm_icp, 9 * sizeof(double));
		memcpy(TrackingFrame_this->T  , t_icp  , 3 * sizeof(double));
	}
*/	
	// runtime add new points
	if ( !flag_firstframe && (Flag_TrackingIsGood))
	{
		if (Flag_IsKeyFrame)
		{
			GenerateLivePoints(Points, Points_live, TrackingFrame_this->dcm, TrackingFrame_this->T);
			MergeLivePointsAndUpdateOrigCoord(Points, Points_live, MergingWeight
				, TrackingFrame_this->dcm, TrackingFrame_this->T
				, StereoMatchingResult
				, Params
				, W, H);
		
			RuntimeAddNewPointsMainFunction_new(Points, Points_live, CellArray, RGBArray, MergingWeight
				, StereoMatchingResult
				, TrackingFrame_this->dcm, TrackingFrame_this->T
				, Params
				, W, H
				, false);
		}
	}	
	
	std::cout << "KeyFrame->size() = " << KeyFrame->size() << ", PointCount = " << Points->GetNumberOfPoints() << std::endl;
	Flag_TrackingIsGood_last = Flag_TrackingIsGood;
	Flag_IsKeyFrame_last = Flag_IsKeyFrame;
}


