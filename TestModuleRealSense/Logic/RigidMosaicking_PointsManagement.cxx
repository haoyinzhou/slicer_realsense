#include "RigidMosaicking_PointsManagement.h"


#include <vtkMath.h>
#include <vtkSmartPointer.h>
#include <vtkPointLocator.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkPCANormalEstimation.h>
#include <vtkPointData.h>
#include <vtkFloatArray.h>
#include <vtkUnsignedCharArray.h>

using namespace cv;
using namespace std;



void GenerateLivePoints(vtkPoints* Points, vtkPoints* Points_live
	, double* dcm, double* T)
{
	for (vtkIdType pid = 0; pid < Points->GetNumberOfPoints(); pid++)
	{
		double coord[3];
		Points->GetPoint(pid, coord);
		double coord_live[3];
		DCMT_OrigCoord_2_LiveCoord(dcm, T, coord, coord_live);
		Points_live->SetPoint(pid, coord_live);
	//	for (int l = 0; l < 3; l++) Points->coord_live[3 * pid + l] = coord_live[l];
	}
}



vtkIdType InitialAddPoints(vtkPoints* Points, vtkPoints* Points_live, vtkCellArray* CellArray, vtkUnsignedCharArray* RGBArray, vtkFloatArray* MergingWeight
				, CStereoMatchingResult* StereoMatchingResult
				, int W, int H)
{
	Points->Reset();
	Points_live->Reset();
	CellArray->Reset();
	RGBArray->Reset();
	MergingWeight->Reset();
	
	vtkIdType pidx = Points->GetNumberOfPoints();
	for (int y = 0; y < H; y++)
	{
		for (int x = 0; x < W; x++)
		{
			int yWx = y * W + x;

			if (StereoMatchingResult->Mask[yWx] == 0) continue;

			double coord[3];
			for (int l = 0; l < 3; l++) coord[l] =  StereoMatchingResult->coord[3 * yWx + l];
	//		for (int l = 0; l < 3; l++) Points->coord_live[3 * pid + l] = StereoMatchingResult->coord[3 * yWx + l];
	//		for (int l = 0; l < 3; l++) Points->coord[3 * pid + l] = StereoMatchingResult->coord[3 * yWx + l];//0.0;
			Points->InsertNextPoint(coord);
			Points_live->InsertNextPoint(coord);
			CellArray->InsertNextCell(1, &pidx);
			
			float RGB[3];
			for (int l = 0; l < 3; l++) RGB[l] = StereoMatchingResult->RGB[3 * yWx + l];
			RGBArray->InsertNextTuple(RGB);
	//		for (int l = 0; l < 3; l++) Points->RGB[3 * pid + l] = StereoMatchingResult->RGB[3 * yWx + l];
	
	//		Points->MergingWeight[pid] = 1.0;
			MergingWeight->InsertNextValue(1.0);
			
			pidx ++;
		}
	}
	return Points->GetNumberOfPoints();
}

// Runtime




void MergeLivePoints_kernel(vtkPoints* Points, vtkPoints* Points_live, vtkFloatArray* MergingWeight
				, CStereoMatchingResult* StereoMatchingResult
				, double merging_threshold2
				, int W, int H
				, double FocalLength, double ImgCenterU, double ImgCenterV)
{
	memset(StereoMatchingResult->flag_usedbymerge, 0, W * H * sizeof(uchar));
	for (vtkIdType pid = 0; pid < Points->GetNumberOfPoints(); pid ++)
	{
		double PhyCoord[3], DisparityCoord[3];
//		for (int l = 0; l < 3; l ++) PhyCoord[l] = Points->coord_live[3 * pid + l];
		Points_live->GetPoint(pid, PhyCoord);
		PhyCoord2DisparityCoord_double(PhyCoord, DisparityCoord, FocalLength, ImgCenterU, ImgCenterV);
		int x = round(DisparityCoord[0]);
		int y = round(DisparityCoord[1]);
	
		if (x < 1 || x > W - 2 || y < 1 || y > H - 2) continue;
		int yWx = y * W + x;
		if (StereoMatchingResult->Mask[yWx] == 0) continue;

		float MergingWeight_this = MergingWeight->GetValue(pid);
		MergingWeight_this = (MergingWeight_this < 0.1) ? 0.1 : MergingWeight_this;

		if (MergingWeight_this > 10.0) MergingWeight_this = 10.0;

		double dir[3];
		for (int l = 0; l < 3; l++) dir[l] = PhyCoord[l] - StereoMatchingResult->coord[3 * yWx + l];
		if (dir[0] * dir[0] + dir[1] * dir[1] + dir[2] * dir[2] > merging_threshold2) continue;

		double coord_live[3];
		for (int l = 0; l < 3; l++) coord_live[l] = (MergingWeight_this * PhyCoord[l] + StereoMatchingResult->coord[3 * yWx + l]) / (MergingWeight_this + 1.0);
		Points_live->SetPoint(pid, coord_live);
	//	for (int l = 0; l < 3; l ++) Points->RGB[3 * pid + l] = StereoMatchingResult->RGB[3 * yWx + l];
	//	for (int l = 0; l < 3; l++)	Points->RGB[3 * pid + l] = (uchar)((MergingWeight * (double)(Points->RGB[3 * pid + l]) + (double)StereoMatchingResult->RGB[3 * yWx + l]) / (MergingWeight + 1.0));
	//	Points->MergingWeight[pid] = MergingWeight + 1.0;
		MergingWeight->SetValue(pid, (float)(MergingWeight_this + 1.0));
	
		StereoMatchingResult->flag_usedbymerge[yWx] = 255;
	/*	StereoMatchingResult->flag_usedbymerge[y * W + x - 1] = 255;
		StereoMatchingResult->flag_usedbymerge[y * W + x + 1] = 255;
		StereoMatchingResult->flag_usedbymerge[(y - 1) * W + x] = 255;
		StereoMatchingResult->flag_usedbymerge[(y + 1) * W + x] = 255;
		StereoMatchingResult->flag_usedbymerge[(y - 1) * W + x - 1] = 255;
		StereoMatchingResult->flag_usedbymerge[(y - 1) * W + x + 1] = 255;
		StereoMatchingResult->flag_usedbymerge[(y + 1) * W + x - 1] = 255;
		StereoMatchingResult->flag_usedbymerge[(y + 1) * W + x + 1] = 255;
	*/
	}
}

/*
void MergeLivePoints_kernel_new(CPoint* Points, long OldPointCount
				, CStereoMatchingResult* StereoMatchingResult
				, int W, int H
				, CParams* Params)
{
	memset(StereoMatchingResult->flag_usedbymerge, 0, W * H * sizeof(uchar));

	vtkSmartPointer<vtkPoints> vtkpoints = vtkSmartPointer<vtkPoints>::New();
//	vtkSmartPointer<vtkPoints> vtknormals = vtkSmartPointer<vtkPoints>::New();
	vector<int> relatedid;
	for (int y = 0; y < H; y ++)
		for (int x = 0; x < W; x ++)
		{
			int yWx = y * W + x;
			if (StereoMatchingResult->Mask[yWx] == 0) continue;
			double coord[3];
			for (int l = 0; l < 3; l++) coord[l] = StereoMatchingResult->coord[3 * yWx + l];
			vtkpoints->InsertNextPoint(coord);
		//	double normal[3];
		//	for (int l = 0; l < 3; l++) normal[l] = StereoMatchingResult->normal[3 * yWx + l];
		//	vtknormals->InsertNextPoint(normal);
			relatedid.push_back(yWx);
	}
	vtkSmartPointer<vtkPolyData> poly = vtkSmartPointer<vtkPolyData>::New();
	poly->SetPoints(vtkpoints);
	
	vtkSmartPointer<vtkPCANormalEstimation> normalestimator =
		vtkSmartPointer<vtkPCANormalEstimation>::New();
	normalestimator->SetInputData (poly);
	normalestimator->SetSampleSize(10);
	normalestimator->SetNormalOrientationToGraphTraversal();
	normalestimator->FlipNormalsOn();
	normalestimator->Update();
 	vtkFloatArray* vtknormals = vtkFloatArray::SafeDownCast(normalestimator->GetOutput()->GetPointData()->GetArray(0));

	vtkSmartPointer<vtkPointLocator> pointLocator =	vtkSmartPointer<vtkPointLocator>::New();
	pointLocator->SetDataSet(poly);
	pointLocator->AutomaticOn();
	pointLocator->SetNumberOfPointsPerBucket(2);
	pointLocator->BuildLocator();	
	
	for (long pid = 0; pid < OldPointCount; pid ++)
	{
		if (Points->flag[pid] != 255) continue;

		double coord_live[3];
		for (int l = 0; l < 3; l++) coord_live[l] = Points->coord_live[3 * pid + l];
	
		vtkIdType id = pointLocator->FindClosestPoint(coord_live);
		double coord_neighbor[3];
		vtkpoints->GetPoint(id, coord_neighbor);
		double  normal_neighbor[3];
		vtknormals->GetTuple(id, normal_neighbor);	
		
		double dis_all2 = vtkMath::Distance2BetweenPoints(coord_live, coord_neighbor);
		
		double coord_diff[3];
		for (int l = 0; l < 3; l ++) coord_diff[l] = coord_neighbor[l] - coord_live[l];		
		double dis_normal = 0.0;
		for (int l = 0; l < 3; l ++) dis_normal += coord_diff[l] * normal_neighbor[l];
		if (dis_normal < 0) dis_normal = -dis_normal;
		
		double dis_tangent = sqrt(dis_all2 - dis_normal * dis_normal);	
	
		if (dis_normal < 100.0 && dis_tangent < 10.0)
		{
			for (int l = 0; l < 3; l++) Points->coord_live[3 * pid + l] = (Points->MergingWeight[pid] * coord_live[l] + coord_neighbor[l]) / (Points->MergingWeight[pid] + 1.0);
			Points->MergingWeight[pid] += 1.0;
			
			StereoMatchingResult->flag_usedbymerge[relatedid.at(id)] = 255;
		}	
	}
}
*/
void UpdateOrigCoord_kernel(vtkPoints* Points, vtkPoints* Points_live
							, double* dcm, double* T)
{
	for (vtkIdType pid = 0; pid < Points->GetNumberOfPoints(); pid++)
	{
		double coord[3], coord_live[3];
		Points_live->GetPoint(pid, coord_live);
	//	for (int l = 0; l < 3; l++) coord_live[l] = Points->coord_live[3 * pid + l];
		DCMT_LiveCoord_2_OrigCoord(dcm, T, coord_live, coord);
	//	for (int l = 0; l < 3; l++) Points->coord[3 * pid + l] = coord[l];
		Points->SetPoint(pid, coord);
	}
}

void MergeLivePointsAndUpdateOrigCoord(vtkPoints* Points, vtkPoints* Points_live, vtkFloatArray* MergingWeight
				, double* dcm, double* T
				, CStereoMatchingResult* StereoMatchingResult
				, CParams* Params
				, int W, int H)
{
	MergeLivePoints_kernel(Points, Points_live, MergingWeight
		, StereoMatchingResult
		, Params->merging_threshold2
		, W, H
		, Params->FocalLength, Params->ImgCenterU, Params->ImgCenterV);
		
/*	MergeLivePoints_kernel_new(Points, OldPointCount
				, StereoMatchingResult
				, W, H
				, Params);
*/	
	UpdateOrigCoord_kernel(Points, Points_live, dcm, T);
}





void UpdateOrigCoordOfStereoMatchingResult(CStereoMatchingResult* StereoMatchingResult
										 , double* dcm, double* T
										 , int W, int H
										 , CParams* Params)
{
	for (int y = 0; y < H; y++)
		for (int x = 0; x < W; x++)
		{
			if (StereoMatchingResult->Mask[y * W + x] < 64)
			{
				for (int l = 0; l < 3; l++) StereoMatchingResult->coord[3 * (y * W + x) + l] = 0.0;
				continue;
			}
			double disparitycoord[3], coord_live[3];
			disparitycoord[0] = (double)x;
			disparitycoord[1] = (double)y;
			disparitycoord[2] = (double)(StereoMatchingResult->disparity[y * W + x]);
			DisparityCoord2PhyCoord_double(coord_live, disparitycoord, Params->FocalLength, Params->ImgCenterU, Params->ImgCenterV);

			double coord[3];			
			DCMT_LiveCoord_2_OrigCoord(dcm, T, coord_live, coord);
			for (int l = 0; l < 3; l++) StereoMatchingResult->coord[3 * (y * W + x) + l] = coord[l];
		}
}
/*
void GetBoudariesOfPoints(CPoint* Points, long PointCount
						, double* boundary )
{
	for (int l = 0; l < 6; l += 2) boundary[l] = 1e+8;
	for (int l = 1; l < 6; l += 2) boundary[l] = -1e+8;
	
	for (long pid = 0; pid < PointCount; pid++)
	{
		if (Points->flag[pid] == 0) continue;
		double coord[3];
		for (int l = 0; l < 3; l++) coord[l] = Points->coord[3 * pid + l];
	
		if (coord[0] < boundary[0]) boundary[0] = coord[0];
		if (coord[0] > boundary[1]) boundary[1] = coord[0];
		if (coord[1] < boundary[2]) boundary[2] = coord[1];
		if (coord[1] > boundary[3]) boundary[3] = coord[1];
		if (coord[2] < boundary[4]) boundary[4] = coord[2];
		if (coord[2] > boundary[5]) boundary[5] = coord[2];
	}
}

void GetBoudariesOfStereoMatchingCoord( CStereoMatchingResult* StereoMatchingResult
						, int W, int H
						, double* boundary )
{
	for (int l = 0; l < 6; l += 2) boundary[l] = 1e+8;
	for (int l = 1; l < 6; l += 2) boundary[l] = -1e+8;
	
	for (int y = 0; y < H; y++)
		for (int x = 0; x < W; x++)
		{
			int yWx = y * W + x;
			if (StereoMatchingResult->Mask[yWx] < 64) continue;
			double coord[3];
			for (int l = 0; l < 3; l++) coord[l] = StereoMatchingResult->coord[3 * yWx + l];
	
			if (coord[0] < boundary[0]) boundary[0] = coord[0];
			if (coord[0] > boundary[1]) boundary[1] = coord[0];
			if (coord[1] < boundary[2]) boundary[2] = coord[1];
			if (coord[1] > boundary[3]) boundary[3] = coord[1];
			if (coord[2] < boundary[4]) boundary[4] = coord[2];
			if (coord[2] > boundary[5]) boundary[5] = coord[2];
		}
}
*/

vtkIdType RuntimeAddNewPointsMainFunction_new(vtkPoints* Points, vtkPoints* Points_live, vtkCellArray* CellArray, vtkUnsignedCharArray* RGBArray, vtkFloatArray* MergingWeight
				, CStereoMatchingResult* StereoMatchingResult
				, double* dcm, double* T
				, CParams* Params
				, int W, int H
				, bool flag_usekdtree)
{
	UpdateOrigCoordOfStereoMatchingResult(StereoMatchingResult
										 , dcm, T
										 , W, H
										 , Params);

/*	double boundary_points[6], boundary_stereomatchingresults[6];
	GetBoudariesOfPoints(Points, OldPointCount
						, boundary_points );
		
	GetBoudariesOfStereoMatchingCoord( StereoMatchingResult
						, W, H
						, boundary_stereomatchingresults );
	printf("%.2f, %.2f, %.2f, %.2f, %.2f, %.2f\n", boundary_points[0], boundary_points[1], boundary_points[2], boundary_points[3], boundary_points[4], boundary_points[5]);					
	printf("%.2f, %.2f, %.2f, %.2f, %.2f, %.2f\n", boundary_stereomatchingresults[0], boundary_stereomatchingresults[1], boundary_stereomatchingresults[2], boundary_stereomatchingresults[3], boundary_stereomatchingresults[4], boundary_stereomatchingresults[5]);					
*/

/*	vtkSmartPointer<vtkPoints> vtkpoints = vtkSmartPointer<vtkPoints>::New();
	for (long pid = 0; pid < OldPointCount; pid ++)
	{
		if (Points->flag[pid] == 0) continue;
		double coord[3];
		for (int l = 0; l < 3; l++) coord[l] = Points->coord[3 * pid + l];
		vtkpoints->InsertNextPoint(coord);
	}
	vtkSmartPointer<vtkPolyData> poly = vtkSmartPointer<vtkPolyData>::New();
	poly->SetPoints(vtkpoints);		
	vtkSmartPointer<vtkPointLocator> pointLocator =	vtkSmartPointer<vtkPointLocator>::New();
	pointLocator->SetDataSet(poly);
	pointLocator->AutomaticOn();
	pointLocator->SetNumberOfPointsPerBucket(2);
	pointLocator->BuildLocator();
*/
/*	vtkSmartPointer<vtkPoints> vtkpoints_stereo = vtkSmartPointer<vtkPoints>::New();
	vtkSmartPointer<vtkFloatArray> normals_stereo = vtkSmartPointer<vtkFloatArray>::New();
	vtkSmartPointer<vtkUnsignedCharArray> rgb_stereo = vtkSmartPointer<vtkUnsignedCharArray>::New();
	rgb_stereo->SetNumberOfComponents(3);
	{
		for (int y = 0; y < H; y ++)
			for (int x = 0; x < W; x ++)
			{
				int yWx = y * W + x;
				if (StereoMatchingResult->Mask[yWx] == 0) continue;
				
				double coord[3];
				for (int l = 0; l < 3; l++) coord[l] = StereoMatchingResult->coord[3 * yWx + l];
				vtkpoints_stereo->InsertNextPoint(coord);
				
				float rgb[3];
				for (int l = 0; l < 3; l ++) rgb[l] = (float)(StereoMatchingResult->RGB[3 * yWx + l]);
				rgb_stereo->InsertNextTuple(rgb);
			}
		vtkSmartPointer<vtkPolyData> poly_stereo = vtkSmartPointer<vtkPolyData>::New();
		poly_stereo->SetPoints(vtkpoints_stereo);
	
		vtkSmartPointer<vtkPCANormalEstimation> normalestimator =
			vtkSmartPointer<vtkPCANormalEstimation>::New();
		normalestimator->SetInputData (poly_stereo);
		normalestimator->SetSampleSize(4);
		normalestimator->SetNormalOrientationToGraphTraversal();
		normalestimator->FlipNormalsOn();
		normalestimator->Update();
	 	normals_stereo = vtkFloatArray::SafeDownCast(normalestimator->GetOutput()->GetPointData()->GetArray(0));
	}

	long pid = OldPointCount;
	for (vtkIdType i = 0; i < vtkpoints_stereo->GetNumberOfPoints(); i ++)	
	{
		if (pid > MaxNumberofPoints)
			return MaxNumberofPoints;
		
		double coord_stereo[3], coord_stereo_live[3];			
		vtkpoints_stereo->GetPoint(i, coord_stereo);
		
		double normal_stereo[3];
		normals_stereo->GetTuple(i, normal_stereo);	
		
		vtkIdType id = pointLocator->FindClosestPoint(coord_stereo);
		double coord_points[3];
		vtkpoints->GetPoint(id, coord_points);	
		double dis_all2 = vtkMath::Distance2BetweenPoints(coord_stereo, coord_points);
		double coord_diff[3];
		for (int l = 0; l < 3; l ++) coord_diff[l] = coord_stereo[l] - coord_points[l];		
		double dis_normal2 = 0.0;
		for (int l = 0; l < 3; l ++) dis_normal2 += coord_diff[l] * normal_stereo[l];
		dis_normal2 = dis_normal2 * dis_normal2;
		double dis_tangent2 = (dis_all2 - dis_normal2);	
		if (dis_normal2 < 900.0 && dis_tangent2 < 9.0) continue;
		
		
	//	if (dis_tangent2 < 9.0) continue;
	
	//	if (dis_all2 < 25.0) continue;
		
		DCMT_OrigCoord_2_LiveCoord(dcm, T, coord_stereo, coord_stereo_live);
		for (int l = 0; l < 3; l++) Points->coord_live[3 * pid + l] = coord_stereo_live[l];
		for (int l = 0; l < 3; l++) Points->coord[3 * pid + l] = coord_stereo[l];
		double rgb[3];
		rgb_stereo->GetTuple(i, rgb);
		for (int l = 0; l < 3; l++) Points->RGB[3 * pid + l] = (uchar)rgb[l];
		Points->MergingWeight[pid] = 1.0;
		Points->flag[pid] = 255;

		pid++;
		if (pid > MaxNumberofPoints)
			return MaxNumberofPoints;
	}
*/
	
	memset(StereoMatchingResult->flag_usedbyadd, 0, W * H * sizeof(uchar));
	
	vtkSmartPointer<vtkPoints> vtkusedformergepoints = vtkSmartPointer<vtkPoints>::New();	
	vtkSmartPointer<vtkPolyData> poly_usedformerge = vtkSmartPointer<vtkPolyData>::New();
	poly_usedformerge->SetPoints(vtkusedformergepoints);		
	vtkSmartPointer<vtkPointLocator> pointLocator =	vtkSmartPointer<vtkPointLocator>::New();
	if (flag_usekdtree)
	{
		for (int y = 0; y < H; y ++)
			for (int x = 0; x < W; x ++)
			{
				int yWx = y * W + x;
				if (StereoMatchingResult->Mask[yWx] == 0) continue;
				if (StereoMatchingResult->flag_usedbymerge[yWx] == 255)
				{
					double coord[3];
					for (int l = 0; l < 3; l++) coord[l] = StereoMatchingResult->coord[3 * yWx + l];
					vtkusedformergepoints->InsertNextPoint(coord);
				}
			}
		pointLocator->SetDataSet(poly_usedformerge);
		pointLocator->AutomaticOn();
		pointLocator->SetNumberOfPointsPerBucket(2);
		pointLocator->BuildLocator();
	}
	
	vtkIdType pidx = Points->GetNumberOfPoints();
	for (int y = 0; y < H; y++)
		for (int x = 0; x < W; x++)
		{
			int yWx = y * W + x;
			if (StereoMatchingResult->Mask[yWx] == 0) continue;
			if (StereoMatchingResult->flag_usedbymerge[yWx] == 255) continue;
			double coord_stereo[3], coord_stereo_live[3];			
			for (int l = 0; l < 3; l++) coord_stereo[l] = StereoMatchingResult->coord[3 * yWx + l];
	
		//	vtkSmartPointer<vtkIdList> idlist = vtkSmartPointer<vtkIdList>::New();
		//	pointLocator->FindPointsWithinRadius(3.0, coord_stereo, idlist);
		//	std::cout << idlist->GetNumberOfIds() << std::endl;
		//	if (idlist->GetNumberOfIds() > 3) continue;  

			if (flag_usekdtree)
			{
				vtkIdType closestid = pointLocator->FindClosestPoint(coord_stereo);
				double coord2[3];
				vtkusedformergepoints->GetPoint(closestid, coord2);
				if (vtkMath::Distance2BetweenPoints(coord_stereo, coord2) < Params->MosaicThreshold2) continue;
			}
			DCMT_OrigCoord_2_LiveCoord(dcm, T, coord_stereo, coord_stereo_live);
		
		//	for (int l = 0; l < 3; l++) Points->coord_live[3 * pid + l] = coord_stereo_live[l];
		//	for (int l = 0; l < 3; l++) Points->coord[3 * pid + l] = coord_stereo[l];
		//	for (int l = 0; l < 3; l++) Points->RGB[3 * pid + l] = StereoMatchingResult->RGB[3 * yWx + l];
		//	Points->MergingWeight[pid] = 1.0;
		//	Points->flag[pid] = 255;
			Points_live->InsertNextPoint(coord_stereo_live);
			Points->InsertNextPoint(coord_stereo);
			CellArray->InsertNextCell(1, &pidx);
			
			float RGB[3];
			for (int l = 0; l < 3; l++) RGB[l] = StereoMatchingResult->RGB[3 * yWx + l];
			RGBArray->InsertNextTuple(RGB);
			MergingWeight->InsertNextValue(1.0);			
			pidx ++;

			StereoMatchingResult->flag_usedbyadd[yWx] = 255;
		}
	
	return Points->GetNumberOfPoints();	
}













