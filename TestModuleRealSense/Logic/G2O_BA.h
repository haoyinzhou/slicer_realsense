#ifndef G2O_BA_H
#define G2O_BA_H

#include "opencv2/opencv.hpp"

#include <stdio.h>
#include <string>

//#include "common.h"
#include "DataStructure.h"




//using namespace cv;
//using namespace std;

//, double FocalLength, double ImgCenterU, double ImgCenterV

void BundleAdjustment_G2O(std::vector<CKeyFrame>* KeyFrame, std::vector<CFeatureVector>* GlobalFeatureVector
						, CTrackingFrame* TrackingFrame_this
						, CG2OParameters* G2OParameters						
						, size_t MaxNumberOfKeyFramesForBA
						, size_t MinNumberOfPointsForBA
						, int BAIterNumber
						, bool flag_islocalBA);


#define DCMT_LiveCoord_2_OrigCoord(dcm, T, livecoord, origcoord)\
{\
	double livecoord_T[3];\
	for (int l = 0; l < 3; l ++) livecoord_T[l] = livecoord[l] - T[l];\
	origcoord[0] = dcm[0] * livecoord_T[0] + dcm[3] * livecoord_T[1] + dcm[6] * livecoord_T[2];\
	origcoord[1] = dcm[1] * livecoord_T[0] + dcm[4] * livecoord_T[1] + dcm[7] * livecoord_T[2];\
	origcoord[2] = dcm[2] * livecoord_T[0] + dcm[5] * livecoord_T[1] + dcm[8] * livecoord_T[2];\
}\

#endif
