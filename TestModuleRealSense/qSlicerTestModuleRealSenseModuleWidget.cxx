/*==============================================================================

 Program: 3D Slicer

 Portions (c) Copyright Brigham and Women's Hospital (BWH) All Rights Reserved.

 See COPYRIGHT.txt
 or http://www.slicer.org/copyright/copyright.txt for details.

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 ==============================================================================*/

// Qt includes
#include <QDebug>
#include <QObject>
//#include <QCheckBox>

#include <unistd.h>

#include <opencv2/imgproc/imgproc.hpp>
//#include "opencv2/opencv.hpp"
// SlicerQt includes
#include "qSlicerTestModuleRealSenseModuleWidget.h"
#include "ui_qSlicerTestModuleRealSenseModuleWidget.h"
#include "qSlicerIO.h"
#include "qSlicerIOManager.h"

#include "vtkSlicerTestModuleRealSenseLogic.h"

#include "vtkMRMLVectorVolumeNode.h"
#include "qMRMLLayoutManager.h"
#include "ctkLayoutManager.h"
#include "qSlicerLayoutManager.h"
#include "qMRMLSliceWidget.h"
#include "vtkMRMLSliceCompositeNode.h"
#include "vtkMRMLSliceLogic.h"
#include "vtkMRMLSliceNode.h"
#include "vtkMRMLScene.h"
#include "vtkMRMLLayoutNode.h"
#include "vtkMRMLDisplayNode.h"

//-----------------------------------------------------------------------------
/// \ingroup Slicer_QtModules_ExtensionTemplate
class qSlicerTestModuleRealSenseModuleWidgetPrivate: public Ui_qSlicerTestModuleRealSenseModuleWidget
{
	Q_DECLARE_PUBLIC(qSlicerTestModuleRealSenseModuleWidget);

protected:
	qSlicerTestModuleRealSenseModuleWidget* const q_ptr;
public:

	qSlicerTestModuleRealSenseModuleWidgetPrivate(qSlicerTestModuleRealSenseModuleWidget& object);
	vtkSlicerTestModuleRealSenseLogic* logic() const;
	QTimer* timer_camera;
};

qSlicerTestModuleRealSenseModuleWidgetPrivate::qSlicerTestModuleRealSenseModuleWidgetPrivate(qSlicerTestModuleRealSenseModuleWidget &object) : q_ptr(&object)
{
}

vtkSlicerTestModuleRealSenseLogic* qSlicerTestModuleRealSenseModuleWidgetPrivate::logic() const
{
	Q_Q(const qSlicerTestModuleRealSenseModuleWidget);
	return vtkSlicerTestModuleRealSenseLogic::SafeDownCast(q->logic());
}

//-----------------------------------------------------------------------------
// qSlicerTestModuleRealSenseModuleWidget methods

//-----------------------------------------------------------------------------
qSlicerTestModuleRealSenseModuleWidget::qSlicerTestModuleRealSenseModuleWidget(QWidget* _parent)
: Superclass( _parent )
, d_ptr( new qSlicerTestModuleRealSenseModuleWidgetPrivate(*this) )
{
}

//-----------------------------------------------------------------------------
qSlicerTestModuleRealSenseModuleWidget::~qSlicerTestModuleRealSenseModuleWidget()
{

}


//-----------------------------------------------------------------------------
void qSlicerTestModuleRealSenseModuleWidget::setup()
{
	Q_D(qSlicerTestModuleRealSenseModuleWidget);
	d->setupUi(this);
	//d->cropExtentpushButton->hide();
	//d->croppushButton->hide();
	this->counter = 0;
	this->Superclass::setup();
	connect(d->liveButton, SIGNAL(clicked()), this, SLOT(InitializeCamera()));
	connect(d->stopliveButton, SIGNAL(clicked()), this, SLOT(stopliveButton_clicked()));
	d->timer_camera = new QTimer();
	connect(d->timer_camera, SIGNAL(timeout()), this, SLOT(TimerFunc()));
	d->RGBcheckBox->setChecked(true);
	connect(d->RGBcheckBox, SIGNAL(toggled(bool)), this, SLOT(RGBcheckBox_stateChanged(bool)));
	d->depthcheckBox->setChecked(true);
	connect(d->depthcheckBox, SIGNAL(toggled(bool)), this, SLOT(depthcheckBox_stateChanged()));
	connect(d->mosaiccheckBox, SIGNAL(toggled(bool)), this, SLOT(mosaiccheckBox_stateChanged(bool)));
	connect(d->cropExtentpushButton, SIGNAL(clicked()), this, SLOT(CropExtentButtonPressed()));
	connect(d->refineModelButton, SIGNAL(clicked()), this, SLOT(RefineButtonPressed()));
	connect(d->croppushButton, SIGNAL(clicked()), this, SLOT(CropButtonPressed()));

}
void qSlicerTestModuleRealSenseModuleWidget::CropButtonPressed()
{
	Q_D(qSlicerTestModuleRealSenseModuleWidget);
	vtkSlicerTestModuleRealSenseLogic* logic = d->logic();
	logic->CropReconstructutedSurface();
}


void qSlicerTestModuleRealSenseModuleWidget::TimerFunc()
{
	// std::cout<<"Timer Working from widget.cxx !" << std::endl;
	Q_D(qSlicerTestModuleRealSenseModuleWidget);
	//   d->setupUi(this);
	vtkSlicerTestModuleRealSenseLogic* logic = d->logic();
	logic->AcquireFrames();

	if (this->counter < 1001){
		// Count only upto 1000 for the first 1000 frames
		this->counter ++;
	}
	this->SetupDisplayPanels();
}

void qSlicerTestModuleRealSenseModuleWidget:: SetupDisplayPanels()
{
	Q_D(qSlicerTestModuleRealSenseModuleWidget);
	vtkSlicerTestModuleRealSenseLogic* logic = d->logic();

	// only after 10 images update the displays
	// Also update only once
	if (this->counter == 10)
	{
		// Set the yellow view to the RGB color image
		// Setup the displays
		qMRMLLayoutManager *layoutManager = qSlicerApplication::application()->layoutManager();
		layoutManager->setLayout(vtkMRMLLayoutNode::SlicerLayoutFourUpView);
		vtkMRMLSliceLogic* yellowSliceLogic = layoutManager->sliceWidget(QString("Yellow"))->sliceLogic();
		vtkMRMLSliceCompositeNode* yellowSliceNode = yellowSliceLogic->GetSliceCompositeNode();
		if ((yellowSliceNode != NULL))
		{
			yellowSliceNode->SetBackgroundVolumeID(logic->RGBvolumenode->GetID());
			yellowSliceLogic->GetSliceNode()->SetOrientationToAxial();
			yellowSliceLogic->FitSliceToAll();
		}

		// Set the green view to the depth image
		layoutManager->setLayout(vtkMRMLLayoutNode::SlicerLayoutFourUpView);//SlicerLayoutOneUpGreenSliceView
		vtkMRMLSliceLogic* greenSliceLogic = layoutManager->sliceWidget(QString("Green"))->sliceLogic();
		vtkMRMLSliceCompositeNode* greenSliceNode = greenSliceLogic->GetSliceCompositeNode();
		if (greenSliceNode != NULL)
		{
			usleep(2000);
			greenSliceNode->SetBackgroundVolumeID(logic->Depthvolumenode->GetID());
			greenSliceLogic->GetSliceNode()->SetOrientationToAxial();
			greenSliceLogic->FitSliceToAll();
		}

		// Set the red view to the composite image
		vtkMRMLSliceLogic* redSliceLogic = layoutManager->sliceWidget(QString("Red"))->sliceLogic();
		vtkMRMLSliceCompositeNode* redSliceNode = redSliceLogic->GetSliceCompositeNode();
		if (redSliceNode != NULL) {
			redSliceNode->SetBackgroundVolumeID(logic->Depthvolumenode->GetID());
			redSliceNode->SetForegroundVolumeID(logic->RGBvolumenode->GetID());
			redSliceNode->SetForegroundOpacity(0.6);
			redSliceLogic->GetSliceNode()->SetOrientationToAxial();
			redSliceLogic->FitSliceToAll();
		}
	}

}


void qSlicerTestModuleRealSenseModuleWidget::CropExtentButtonPressed()
{
	Q_D(qSlicerTestModuleRealSenseModuleWidget);
	vtkSlicerTestModuleRealSenseLogic* logic = d->logic();
	logic->SetupCropExtent();
	//d->croppushButton->show();
	//d->refineModelButton->show();
}

void qSlicerTestModuleRealSenseModuleWidget::stopliveButton_clicked()
{
	std::cout<<"stop live view" << std::endl;
	Q_D(qSlicerTestModuleRealSenseModuleWidget);
	
	if (d->timer_camera->isActive() == true)
	{
		d->timer_camera->stop();
		vtkSlicerTestModuleRealSenseLogic* logic = d->logic();
		logic->stopPipe();
	}
}

bool qSlicerTestModuleRealSenseModuleWidget::InitializeCamera()
{
	std::cout << "Initializing the camera and start timer" << std::endl;
	Q_D(qSlicerTestModuleRealSenseModuleWidget);
	vtkSlicerTestModuleRealSenseLogic* logic = d->logic();
	if (logic)
	{
		bool initializeflag = logic->InitializeCamera();
		if (initializeflag )
		{
			// Start the timer and get active frames
			if (d->timer_camera->isActive() == false)
				d->timer_camera->start(30);
		}
	}
	return true;
}

void qSlicerTestModuleRealSenseModuleWidget::depthcheckBox()
{
	//std::cout<<"Depth stream ON" << std::endl;
}

void qSlicerTestModuleRealSenseModuleWidget::depthcheckBox_stateChanged()
{
	Q_D(qSlicerTestModuleRealSenseModuleWidget);
//	vtkSlicerTestModuleRealSenseLogic* logic = d->logic();

	if (d->depthcheckBox->isChecked())
	{
		std::cout<<"Depth stream ON" << std::endl;
	}
	else
	{
		std::cout<<"Depth stream OFF" << std::endl;
	}
}

void qSlicerTestModuleRealSenseModuleWidget::RGBcheckBox()
{
}

void qSlicerTestModuleRealSenseModuleWidget::RGBcheckBox_stateChanged(bool checked)
{
	Q_D(qSlicerTestModuleRealSenseModuleWidget);
	vtkSlicerTestModuleRealSenseLogic* logic = d->logic();

	if (checked)
	{
		std::cout<<"RGB stream ON" << std::endl;
		logic->flag_RGB = true;
	}
	else
	{
		std::cout<<"RGB stream OFF" << std::endl;
		logic->flag_RGB = false;
	}


}

void qSlicerTestModuleRealSenseModuleWidget::mosaiccheckBox()
{
	// std::cout<<"mosaic " << std::endl;
}

void qSlicerTestModuleRealSenseModuleWidget::mosaiccheckBox_stateChanged(bool checked)
{
	std::cout<<"mosaic state changed" << std::endl;
	Q_D(qSlicerTestModuleRealSenseModuleWidget);
	vtkSlicerTestModuleRealSenseLogic* logic = d->logic();
	if (checked)
	{
		std::cout<<"Start mosaicking" << std::endl;
		logic->flag_domosaic = true;
		logic->Reset();
	}
	else
	{
		std::cout<<"Don't mosaic" << std::endl;
		logic->flag_domosaic = false;

	}
}

void qSlicerTestModuleRealSenseModuleWidget::RefineButtonPressed()
{
	std::cout << "RefineButtonPressed" << std::endl;
	// Stop live view
	this->stopliveButton_clicked();
//	this->stopliveButton_clicked();
	Q_D(qSlicerTestModuleRealSenseModuleWidget);
	vtkSlicerTestModuleRealSenseLogic* logic = d->logic();

	d->mosaiccheckBox->setChecked(0);
	logic->flag_domosaic = false;	
	logic->Refinement();
	
	qMRMLLayoutManager *layoutManager = qSlicerApplication::application()->layoutManager();
	layoutManager->setLayout(vtkMRMLLayoutNode::SlicerLayoutOneUp3DView);
}



