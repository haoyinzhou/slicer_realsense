#ifndef DataStructure_H
#define DataStructure_H

#include "opencv2/opencv.hpp"
#include <stdio.h>
#include <time.h>
#include <string>

//G2O includes
#include <g2o/core/sparse_optimizer.h>
#include <g2o/core/block_solver.h>
#include <g2o/core/solver.h>
#include <g2o/core/robust_kernel.h>
#include <g2o/core/robust_kernel_impl.h>
#include <g2o/core/optimization_algorithm_levenberg.h>
#include <g2o/solvers/cholmod/linear_solver_cholmod.h>
#include <g2o/types/slam3d/se3quat.h>
#include <g2o/types/sba/types_six_dof_expmap.h>

#include <vtkSmartPointer.h>
#include <vtkPolyData.h>
#include <vtkPoints.h>
#include <vtkCellArray.h>
#include <vtkIntArray.h>
#include <vtkUnsignedCharArray.h>
#include <vtkAbstractArray.h>
#include <vtkPointData.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkMath.h>
#include <vtkProperty.h>
#include <vtkLookupTable.h>
#include <vtkCamera.h>
#include <vtkXMLPolyDataWriter.h>
#include <vtkXMLPolyDataReader.h>
#include <vtkLine.h>
#include <vtkLabeledDataMapper.h>
#include <vtkActor2D.h>
#include <vtkOrientationMarkerWidget.h>
#include <vtkAxesActor.h>
#include <vtkSphereSource.h>
#include <vtkTransform.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkMatrix4x4.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkAppendPolyData.h>
#include <vtkCleanPolyData.h>
#include <vtkFloatArray.h>

typedef unsigned char uchar;

//#define MaxNumberofPoints 2560000

//#define Scale2mm 100.0


#define DisparityCoord2PhyCoord_double(PhyCoord, DisparityCoord, FocalLength, ImgCenterU, ImgCenterV) \
{ \
	{ \
		double u_center = DisparityCoord[0] - ImgCenterU;\
		double v_center = DisparityCoord[1] - ImgCenterV;\
		PhyCoord[0] = u_center * DisparityCoord[2] / FocalLength;\
		PhyCoord[1] = v_center * DisparityCoord[2] / FocalLength;\
		PhyCoord[2] = DisparityCoord[2];\
	} \
} \


#define PhyCoord2DisparityCoord_double(PhyCoord, DisparityCoord, FocalLength, ImgCenterU, ImgCenterV) \
{ \
	{ \
		double u_center = FocalLength * PhyCoord[0] / PhyCoord[2];\
		double v_center = FocalLength * PhyCoord[1] / PhyCoord[2];\
		DisparityCoord[0] = u_center + ImgCenterU;\
		DisparityCoord[1] = v_center + ImgCenterV;\
		DisparityCoord[2] = PhyCoord[2];\
	} \
} \

class CParams
{
public:
//	int startframeid;
//	int endframeid;	
//	int visualizationstep;

	double RGBDMinDistance;
	double RGBDMaxDistance;
	
public: // OpenCV ORB (not used)
/*	float ORB_scaleFactor;
	int nlevels;
	int edgeThreshold;
	int fastthreshold ;	
*/	
public: // camera parameters
//	int UndistortMapID;

	double FocalLength;
	double ImgCenterU;
	double ImgCenterV;
	int W;
	int H;

public: // stereo matching
/*	float AllowedSmoothingMaxDisparityDiff;
	int SmoothingRadius;
	int SmoothingNum;
	int SmoothingStep;
*/	
	float RemoveTooTiltPointsThreshold;
	
public: // rigid mosaicking 
	
	double merging_threshold2;

	double MinPoseDifference_2_lastkeyframeForBeingKeyFrame;
	double MaxPoseDifference_2_previousframeForGoodTracking;
	unsigned int MinNumberOfGoodMatchesForGoodTracking;
//	double MinGoodRateForGoodTracking;
	int MaxAllowedFrameBetweenKeyFrames;

	double inlierErrorPixelTH;
	
	size_t MaxNumberOfKeyFramesForBA;
	size_t MinNumberOfPointsForBA;
	int BAIterNumber;
	
	double MosaicThreshold2;
	
public: //ORB feature
	unsigned int SuggestedNumberOfOpenCVFeaturePointsPerFrame;
	unsigned int MaxNumberOfFeaturePointsPerFrame;
	int MinPixelBetweenFeatures;
	int PixelTH;

public:
	CParams();
};

struct CStereoMatchingData
{
	uchar* img1;
	uchar* img1_RGB;
	
	float* disparitymap;
	uchar* Mask;
	uchar* Mask_temp;
	
	float* disparitymap_forsmooth1;
	float* disparitymap_forsmooth2;
	float* disparitymap_forsmooth3;
};

class CStereoMatchingResult
{
public:
	uchar* Mask;
	float* disparity;
	uchar* RGB;
	
	double* coord;
	vtkSmartPointer<vtkPoints> Points;
	vtkSmartPointer<vtkPolyData> Poly;
//	double* normal;
	
	uchar* flag_usedbymerge;
	uchar* flag_usedbyadd;
};

/*
class CPoint
{
public:
	uchar* flag;
	double* coord;
	double* coord_live;
	
//	uchar* intense;
	uchar* RGB;
	double* MergingWeight;	
};
*/

class CFeatureVector
{
public:
	double coord[3];
	uchar Discriptor[32];
	
	std::vector<int> ObservedGlobalKeyFrameID;
	std::vector<double> u;
	std::vector<double> v;
};

class CKeyFrame
{
public:
// observed features
	std::vector<int> globalpid;
	std::vector<cv::KeyPoint> ORBkeypoints;
	cv::Mat Discriptor;
	std::vector<double> coord;
	
	double dcm[9];
	double T[3];
	
public:
	uchar* Mask;
	uchar* RGB;
	float* depth;	

public:
	CKeyFrame();
};

class CTrackingFrame
{
public:
	std::vector<cv::KeyPoint> ORBkeypoints;
	cv::Mat Discriptor;
	
	uchar* flag_coordisvalid;
	double* coord;
	double* coord_live;	
	int* MatchedID;
	int* globalpid;
	int* localpid;
	
	double* dcm;
	double* T;
	
public:
	cv::Mat* img;
};

class CG2OParameters
{
public:
	g2o::SparseOptimizer optimizer;
	std::unique_ptr<g2o::BlockSolver_6_3::LinearSolverType> linearSolver;
	g2o::OptimizationAlgorithmLevenberg* solver;
	g2o::CameraParameters* camera;

public:
	void Initialization(double FocalLength, double ImgCenterU, double ImgCenterV);
};


class CMosaicAlgorithm
{
public:	
	CParams Params;
	cv::Mat img1;
	CStereoMatchingData StereoMatchingData;
	CStereoMatchingResult StereoMatchingResult;

//	CPoint Points;
//	long PointCount, OldPointCount;
	
	vtkSmartPointer<vtkPolyData> Poly;
	vtkSmartPointer<vtkPoints> Points;
	vtkSmartPointer<vtkPoints> Points_live;
	vtkSmartPointer<vtkCellArray> CellArray;
	vtkSmartPointer<vtkUnsignedCharArray> RGBArray;
	vtkSmartPointer<vtkFloatArray> MergingWeight;
	
	vtkSmartPointer<vtkPolyData> Mesh;
	
	uchar* Mask_Uncovered;

	CTrackingFrame TrackingFrame[2];
	CTrackingFrame* TrackingFrame_this, * TrackingFrame_prev, * TrackingFrame_for_swap;
	CTrackingFrame* TrackingFrame_keyframe;

	std::vector<CFeatureVector> GlobalFeatureVector;
	std::vector<CKeyFrame> KeyFrame;
	
	std::vector< cv::DMatch > ORBmatches;
	std::vector<uchar> ORBinliersMask;

	CG2OParameters G2OParameters;
	bool Flag_TrackingIsGood, Flag_IsKeyFrame, Flag_TrackingIsGood_last, Flag_IsKeyFrame_last;

//	cv::Ptr<cv::FeatureDetector>* ORBdetector;
//	cv::Ptr<cv::DescriptorExtractor>* ORBextractor;
//	cv::BFMatcher* ORBmatcher;

	unsigned int frameid;
	unsigned int LastKeyFrameID;
	int NumberOfTrackingLostFrame;

public:
	CMosaicAlgorithm();
	void Initial();
	void Clear();

	//const char* RGBFileName, const char* DepthFileName
	bool Compute();
	void Reset();
	void Refine();
	void MeshGeneration(vtkPolyData*);
};


#endif
