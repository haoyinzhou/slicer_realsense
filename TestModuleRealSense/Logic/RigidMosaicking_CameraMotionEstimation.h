#ifndef RigidMosaicking_CameraMotionEstimation_H
#define RigidMosaicking_CameraMotionEstimation_H


//#include "common.h"
#include "DataStructure.h"

//using namespace cv::xfeatures2d;

int MotionTracking(CTrackingFrame* TrackingFrame_this, CTrackingFrame* TrackingFrame_prev
								, double dcm_lastKeyFrame[9], double T_lastKeyFrame[3]
								, bool& Flag_TrackingIsGood, bool& Flag_IsKeyFrame
								, CParams* Params
								, int NumberOfLostFrame);

void ResetRigidMosaicking(std::vector<CFeatureVector>* GlobalFeatureVector, std::vector<CKeyFrame>* KeyFrame
			, vtkPoints* Points, vtkPoints* Points_live, vtkCellArray* CellArray, vtkUnsignedCharArray* RGBArray, vtkFloatArray* MergingWeight);


void RigidMosaicking(CStereoMatchingResult* StereoMatchingResult, cv::Mat* img1
	, vtkPolyData* Poly, vtkPoints* Points, vtkPoints* Points_live, vtkCellArray* CellArray, vtkUnsignedCharArray* RGBArray, vtkFloatArray* MergingWeight
	, uchar* Mask_Uncovered
	, CTrackingFrame* TrackingFrame0, CTrackingFrame* TrackingFrame1, CTrackingFrame*& TrackingFrame_this, CTrackingFrame*& TrackingFrame_prev
	, bool& Flag_TrackingIsGood, bool& Flag_IsKeyFrame, bool& Flag_TrackingIsGood_last, bool& Flag_IsKeyFrame_last
	, std::vector<CFeatureVector>* GlobalFeatureVector, std::vector<CKeyFrame>* KeyFrame
	, CG2OParameters* G2OParameters
	, CParams* Params
	, unsigned int frameid, unsigned int& LastKeyFrameID, int& NumberOfLostFrame);

#endif
