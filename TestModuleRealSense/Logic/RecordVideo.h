#ifndef RecordVideo_H
#define RecordVideo_H

#include "opencv2/opencv.hpp"
#include <stdio.h>

using namespace cv;


class CRGBDVideoWriter
{
public:
	VideoWriter RGBWriter;
	VideoWriter DepthWriter;
	
public:
	bool Initial(const char* filename_rgb, const char* filename_d, int W, int H);
	void WriteRGB(cv::Mat* rgbframe);
	void WriteDepth(float* depth, int W, int H);
	void Release();
};


#endif
