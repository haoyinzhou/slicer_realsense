#include "DataStructure.h"
#include "InitialMemory.h"
#include "StereoVision.h"
#include "RigidMosaicking_CameraMotionEstimation.h"

#include <time.h>
#include "common.h"
#include "FinalRefinement.h"
#include "MeshGeneration.h"

using namespace cv;
using namespace std;


CParams::CParams()
{
//	this->UndistortMapID = 1;
	this->RGBDMinDistance = 300.0; //  (mm) because realsense cannot detect too close distance
	this->RGBDMaxDistance = 1500.0; // (mm) because we don't need too far away points (may include outliers)

// OpenCV ORB parameters (not used)
/*	this->ORB_scaleFactor = 1.1f;
	this->nlevels = 8;
	this->edgeThreshold = 12;
	this->fastthreshold = 12;
*/	
// camera parameters
	this->FocalLength = 601.0;//468.0;
	this->ImgCenterU = 320.0;//310.0;
    this->ImgCenterV =  240.0;//235.0;
	this->W = 640;
    this->H = 480;//360;

// stereo matching // not used in RGB-D reconstruction
/*	this->AllowedSmoothingMaxDisparityDiff   = 10.0;
	this->SmoothingRadius = 5;
	this->SmoothingNum = 1;
	this->SmoothingStep = 1;
*/	
	this->RemoveTooTiltPointsThreshold = 20.0;	

// rigid mosaicking	

	this->merging_threshold2 = 60.0 * 60.0;
	this->MinPoseDifference_2_lastkeyframeForBeingKeyFrame = 80.0;
	this->MaxPoseDifference_2_previousframeForGoodTracking = 100.0;
	this->MaxAllowedFrameBetweenKeyFrames = 6;
	this->MinNumberOfGoodMatchesForGoodTracking = 80;
//	this->MinGoodRateForGoodTracking = 0.5;
	this->inlierErrorPixelTH = 5.0;

	this->MaxNumberOfKeyFramesForBA = 10;
	this->MinNumberOfPointsForBA = 50;
	this->BAIterNumber = 6;
	
	this->MosaicThreshold2 = 1.6 * 1.6;
	
// ORB feature	// GPU-based ORB detection, not used in RGB-D
	this->SuggestedNumberOfOpenCVFeaturePointsPerFrame = 5000;
	this->MaxNumberOfFeaturePointsPerFrame = this->SuggestedNumberOfOpenCVFeaturePointsPerFrame + 500;
	this->MinPixelBetweenFeatures = 5;
	this->PixelTH = 1;

}

CKeyFrame::CKeyFrame()
{
	this->RGB = NULL;
	this->depth = NULL;
}

CMosaicAlgorithm::CMosaicAlgorithm()
{
	this->img1.create(this->Params.H, this->Params.W, CV_8UC1);

	this->frameid = 0;
	this->LastKeyFrameID = 0;

	this->NumberOfTrackingLostFrame = 0;
//	this->ORBdetector = ORB::create(SuggestedNumberOfOpenCVFeaturePointsPerFrame, 1.2f, 1, 31, 0, 2, ORB::FAST_SCORE, 31);
//	this->ORBextractor = ORB::create();
//	this->ORBmatcher.create("BruteForce-Hamming(2)");	
}


void CMosaicAlgorithm::Initial()
{
	this->Poly = vtkSmartPointer<vtkPolyData>::New();
	this->Points = vtkSmartPointer<vtkPoints>::New();
	this->Points_live = vtkSmartPointer<vtkPoints>::New();
	this->CellArray = vtkSmartPointer<vtkCellArray>::New();
	this->RGBArray = vtkSmartPointer<vtkUnsignedCharArray>::New();
	this->RGBArray->SetName("RGB");
	this->RGBArray->SetNumberOfComponents(3);	
	this->MergingWeight = vtkSmartPointer<vtkFloatArray>::New();
	this->MergingWeight->SetName("MergingWeight");
	this->MergingWeight->SetNumberOfComponents(1);	
	
	this->Poly->SetPoints(this->Points);
	this->Poly->SetPoints(this->Points);
	this->Poly->SetVerts(this->CellArray);
	this->Poly->GetPointData()->SetScalars(this->RGBArray);
	this->Poly->GetPointData()->AddArray(this->MergingWeight);
	
	this->Mesh = vtkSmartPointer<vtkPolyData>::New();

	InitialMemory_StereoVision(&(this->StereoMatchingData), &(this->StereoMatchingResult), this->Params.W, this->Params.H);

	InitialMemory_DataManagement(this->Mask_Uncovered
		, this->Params.W, this->Params.H);

	InitialMemory_FeaturePoints(this->TrackingFrame
		, (Mat*)(void*) & (this->img1)
		, this->Params.MaxNumberOfFeaturePointsPerFrame
		, this->Params.W, this->Params.H);

	this->G2OParameters.Initialization(this->Params.FocalLength, this->Params.ImgCenterU, this->Params.ImgCenterV);

//	this->ORBdetector = ORBdetector;
//	this->ORBextractor = ORBextractor;
//	this->ORBmatcher = ORBmatcher;
	
	this->frameid = 0;
	this->LastKeyFrameID = 0;
	this->NumberOfTrackingLostFrame = 0;
}

void CMosaicAlgorithm::Clear()
{
	ClearMemory_StereoVision(&(this->StereoMatchingData), &(this->StereoMatchingResult));

//	ClearMemory_DataManagement(&(this->Points));
	ClearMemory_FeaturePoints(this->TrackingFrame, &(this->KeyFrame));
}

//const char* RGBFileName, const char* DepthFileName
bool CMosaicAlgorithm::Compute()
{
//	ReadRGBDDataFromFiles(RGBFileName, DepthFileName
//		, &(this->StereoMatchingData)
//		, &(this->Params));

//	clock_t t = clock();
//	PostProcessingDepthData(&(this->StereoMatchingData)
//		, &(this->Params));
	RGBDData2Results(&(this->StereoMatchingData)
		, &(this->StereoMatchingResult)
		, &(this->Params));
	//memcpy((void*)(this->img1.data), (void*)(this->StereoMatchingResult.intense), this->Params.W * this->Params.H * sizeof(uchar));
	memcpy((void*)(this->img1.data), (void*)(this->StereoMatchingData.img1), this->Params.W * this->Params.H * sizeof(uchar));

//	t = clock() - t;
//	printf("time Pre-RigidMosaicking = %.2f(ms)\n", 1000.0 * (float)t / CLOCKS_PER_SEC);

//	t = clock();
	RigidMosaicking(&(this->StereoMatchingResult), &(this->img1)
		, this->Poly, this->Points, this->Points_live, this->CellArray, this->RGBArray, this->MergingWeight
		, this->Mask_Uncovered
		, &(this->TrackingFrame[0]), &(this->TrackingFrame[1]), this->TrackingFrame_this, this->TrackingFrame_prev
		, this->Flag_TrackingIsGood, this->Flag_IsKeyFrame, this->Flag_TrackingIsGood_last, this->Flag_IsKeyFrame_last
		, &(this->GlobalFeatureVector), &(this->KeyFrame)
		, &(this->G2OParameters)
		, &(this->Params)
		, this->frameid, this->LastKeyFrameID, this->NumberOfTrackingLostFrame);

//		, this->ORBdetector, this->ORBextractor, this->ORBmatcher
//	t = clock() - t;
//	printf("time RigidMosaicking = %.2f(ms)\n", 1000.0 * (float)t / CLOCKS_PER_SEC);
	
	this->frameid ++;
	
/*	int H = this->Params.H;
	int W = this->Params.W;
	SavePolyData(this->Poly, "//home//slam-lab//work//Align_advanced//data//PolyData_inprocess.vtp");	
	cv::Mat flag_stereomask_map = cv::Mat::zeros(H, W, CV_8UC1);
	for (int y = 0; y < H; y ++)
		for (int x = 0; x < W; x ++)
	{
		flag_stereomask_map.data[y * W + x] = this->StereoMatchingResult.Mask[y * W + x];
	}
	imshow("flag_stereomask_map", flag_stereomask_map);
	cv::Mat flag_usedbymerge_map = cv::Mat::zeros(H, W, CV_8UC1);
	for (int y = 0; y < H; y ++)
		for (int x = 0; x < W; x ++)
	{
		flag_usedbymerge_map.data[y * W + x] = this->StereoMatchingResult.flag_usedbymerge[y * W + x];
	}
	imshow("flag_usedbymerge_map", flag_usedbymerge_map);
	cv::Mat flag_usedbyadd_map = cv::Mat::zeros(H, W, CV_8UC1);
	for (int y = 0; y < H; y ++)
		for (int x = 0; x < W; x ++)
	{
		flag_usedbyadd_map.data[y * W + x] = this->StereoMatchingResult.flag_usedbyadd[y * W + x];
	}
	imshow("flag_usedbyadd_map", flag_usedbyadd_map);
	cvWaitKey(-1);
*/	
	this->Points->Modified();		
	this->Points_live->Modified();		
	this->CellArray->Modified();		
	this->RGBArray->Modified();		
	this->MergingWeight->Modified();		
	return true;
}

void CMosaicAlgorithm::Reset()
{
	ResetRigidMosaicking(&(this->GlobalFeatureVector), &(this->KeyFrame)
			, this->Points, this->Points_live, this->CellArray, this->RGBArray, this->MergingWeight);
	this->Points->Modified();		
	this->Points_live->Modified();		
	this->CellArray->Modified();		
	this->RGBArray->Modified();		
	this->MergingWeight->Modified();
	
	this->Mesh->Reset();
	this->Mesh->Modified();		
}

void CMosaicAlgorithm::Refine()
{
	FinalRefinement(this->Points, this->Points_live, this->CellArray, this->RGBArray, this->MergingWeight
				, &(this->StereoMatchingResult)
				, this->Mask_Uncovered
				, &(this->GlobalFeatureVector), &(this->KeyFrame)
				, &(this->TrackingFrame[0]), &(this->TrackingFrame[1])
				, &(this->G2OParameters)
				, &(this->Params));
	this->Points->Modified();		
	this->Points_live->Modified();		
	this->CellArray->Modified();		
	this->RGBArray->Modified();		
	this->MergingWeight->Modified();		
}

void CMosaicAlgorithm::MeshGeneration(vtkPolyData* Poly_in)
{
	MeshGeneration_Main(Poly_in, this->Mesh);
	this->Mesh->Modified();
}

