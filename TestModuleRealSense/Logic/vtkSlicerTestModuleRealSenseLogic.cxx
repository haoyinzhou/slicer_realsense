/*==============================================================================

 Program: 3D Slicer

 Portions (c) Copyright Brigham and Women's Hospital (BWH) All Rights Reserved.

 See COPYRIGHT.txt
 or http://www.slicer.org/copyright/copyright.txt for details.

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 ==============================================================================*/

// TestModuleRealSense Logic includes
#include "vtkSlicerTestModuleRealSenseLogic.h"

// MRML includes
#include <vtkMRMLScene.h>

// VTK includes
#include <vtkIntArray.h>
#include <vtkNew.h>
#include <vtkObjectFactory.h>

// STD includes
#include <cassert>

#include <librealsense2/rs.hpp>
#include <iostream>

#include <unistd.h>


#include "vtkMRMLVectorVolumeNode.h"
#include "qMRMLLayoutManager.h"
#include "ctkLayoutManager.h"
#include "qSlicerLayoutManager.h"
#include "qMRMLSliceWidget.h"
#include "vtkMRMLSliceCompositeNode.h"
#include "vtkMRMLSliceLogic.h"
#include "vtkMRMLSliceNode.h"
#include "vtkMRMLScene.h"
#include "vtkImageData.h"
#include "vtkMRMLModelNode.h"
#include "vtkMRMLModelDisplayNode.h"
#include "vtkMRMLCrosshairNode.h"
#include "vtkMRMLInteractionNode.h"
#include "vtkMRMLSelectionNode.h"
#include "vtkMRMLMarkupsFiducialNode.h"
#include "vtkMRMLMarkupsNode.h"
#include "vtkMRMLAnnotationROINode.h"
#include "vtkMRMLCameraNode.h"
#include "vtkRenderWindow.h"
#include "vtkRenderer.h"
#include "vtkRendererCollection.h"
#include "vtkCornerAnnotation.h"
#include "vtkTextProperty.h"
#include "qSlicerApplication.h"
#include "qSlicerLayoutManager.h"
#include "qMRMLThreeDWidget.h"
#include "qMRMLThreeDView.h"
#include "vtkMRMLViewNode.h"


#include <opencv2/opencv.hpp>

#include <sstream>
#include <fstream>
#include <algorithm>
#include <cstring>
#include <QApplication>
#include <QTimer>
#include <QObject>
#include <QDebug>

#include "vtkBox.h"
#include "vtkClipPolyData.h"

using namespace std;
using namespace rs2;
using namespace cv;

#define RGBVideoPath "//home//slam-lab//work//Slicer_RealSense//RGB.avi"
#define DepthVideoPath "//home//slam-lab//work//Slicer_RealSense//Depth.avi"

//----------------------------------------------------------------------------
vtkStandardNewMacro(vtkSlicerTestModuleRealSenseLogic);

//----------------------------------------------------------------------------
vtkSlicerTestModuleRealSenseLogic::vtkSlicerTestModuleRealSenseLogic()// : align(align_to)
{
	this->RGBvolumenode = vtkMRMLVectorVolumeNode::New();
	this->Depthvolumenode = vtkMRMLVectorVolumeNode::New();
	this->MosaicPointCloudModelDisplayNode = vtkMRMLModelDisplayNode::New();
	this->MosaicPointCloudModelNode = vtkMRMLModelNode::New();
	this->MosaicMeshModelDisplayNode = vtkMRMLModelDisplayNode::New();
	this->MosaicMeshModelNode = vtkMRMLModelNode::New();
	this->CornerAnnotation = vtkCornerAnnotation::New();
	//
	this->flag_domosaic = false;
	this->flag_RGB = true;
}

//----------------------------------------------------------------------------
vtkSlicerTestModuleRealSenseLogic::~vtkSlicerTestModuleRealSenseLogic()
{

	if (this->RGBvolumenode)
	{
		this->RGBvolumenode->Delete();
	}

	if (this->Depthvolumenode)
	{
		this->Depthvolumenode->Delete();
	}
	if (this->MosaicPointCloudModelDisplayNode)
	{
		this->MosaicPointCloudModelDisplayNode->Delete();
	}
	if (this->MosaicPointCloudModelNode)
	{
		this->MosaicPointCloudModelNode->Delete();
	}
	if (this->MosaicMeshModelDisplayNode)
	{
		this->MosaicMeshModelDisplayNode->Delete();
	}
	if (this->MosaicMeshModelNode)
	{
		this->MosaicMeshModelNode->Delete();
	}
	if (this->CornerAnnotation)
	{
		this->CornerAnnotation->Delete();
	}
	this->MosaicAlgorithm.Clear();
	this->RGBDVideoWriter.Release();
}

//----------------------------------------------------------------------------
void vtkSlicerTestModuleRealSenseLogic::PrintSelf(ostream& os, vtkIndent indent)
{
	this->Superclass::PrintSelf(os, indent);
}

//---------------------------------------------------------------------------
void vtkSlicerTestModuleRealSenseLogic::SetMRMLSceneInternal(vtkMRMLScene * newScene)
{
	vtkNew<vtkIntArray> events;
	events->InsertNextValue(vtkMRMLScene::NodeAddedEvent);
	events->InsertNextValue(vtkMRMLScene::NodeRemovedEvent);
	events->InsertNextValue(vtkMRMLScene::EndBatchProcessEvent);
	this->SetAndObserveMRMLSceneEventsInternal(newScene, events.GetPointer());
}

//-----------------------------------------------------------------------------
void vtkSlicerTestModuleRealSenseLogic::RegisterNodes()
{
	assert(this->GetMRMLScene() != 0);
}

//---------------------------------------------------------------------------
void vtkSlicerTestModuleRealSenseLogic::UpdateFromMRMLScene()
{
	assert(this->GetMRMLScene() != 0);
}

//---------------------------------------------------------------------------
void vtkSlicerTestModuleRealSenseLogic
::OnMRMLSceneNodeAdded(vtkMRMLNode* vtkNotUsed(node))
{
}

//---------------------------------------------------------------------------
void vtkSlicerTestModuleRealSenseLogic
::OnMRMLSceneNodeRemoved(vtkMRMLNode* vtkNotUsed(node))
{
}

bool vtkSlicerTestModuleRealSenseLogic::InitializeCamera()
{
	// Initialize the 3D view
	qMRMLLayoutManager *layoutManager = qSlicerApplication::application()->layoutManager();
	vtkMRMLViewNode* viewNode = layoutManager->threeDWidget(0)->threeDView()->mrmlViewNode();
	viewNode->SetBoxVisible(false);
	viewNode->SetAxisLabelsVisible(false);

	//Calling pipeline's start() without any additional parameters will start the first device
	// with its default streams.
	//The start function returns the pipeline profile which the pipeline used to start the device
	this->profile = this->pipe.start();

	// Each depth camera might have different units for depth pixels, so we get it here
	// Using the pipeline's profile, we can retrieve the device that the pipeline uses
	float depth_scale = this->get_depth_scale(this->profile.get_device());

	//Pipeline could choose a device that does not have a color stream
	//If there is no color stream, choose to align depth to another stream
	this->align_to = find_stream_to_align(profile.get_streams());

	// Initialize the RGBvolumenode and Depthvolumenode for display in Slicer
	vtkMRMLScene* scene = this->GetMRMLScene();
	vtkCollection* collection = scene->GetNodesByName("RGBVectorVolume");
	if (collection->GetNumberOfItems() == 0){
		this->RGBvolumenode->SetName("RGBVectorVolume");
		scene->AddNode(this->RGBvolumenode);
		this->RGBvolumenode->SetHideFromEditors(0);
	}
	collection->Delete();

	// Initialize depth volumes
	vtkCollection* depthcollection = scene->GetNodesByName("DepthVectorVolume");
	if (depthcollection->GetNumberOfItems() == 0){
		this->Depthvolumenode->SetName("DepthVectorVolume");
		scene->AddNode(this->Depthvolumenode);
		this->Depthvolumenode->SetHideFromEditors(0);
	}
	depthcollection->Delete();

	//////////////////// Initialization //////////////////////////// NEWWWW
	//    CMosaicAlgorithm MosaicAlgorithm;
/*	cv::Ptr<cv::FeatureDetector> ORBdetector = cv::ORB::create(this->MosaicAlgorithm.Params.SuggestedNumberOfOpenCVFeaturePointsPerFrame
			, this->MosaicAlgorithm.Params.ORB_scaleFactor
			, this->MosaicAlgorithm.Params.nlevels
			, this->MosaicAlgorithm.Params.edgeThreshold
			, 0, 2, ORB::FAST_SCORE
			, 31
			, this->MosaicAlgorithm.Params.fastthreshold); // use 3 nlevels may slower

	cv::Ptr<cv::DescriptorExtractor> ORBextractor = cv::ORB::create();
	cv::BFMatcher ORBmatcher(cv::NORM_HAMMING, true);
	&ORBdetector, &ORBextractor, &ORBmatcher
*/
	this->MosaicAlgorithm.Initial();

	this->RGBDVideoWriter.Initial(RGBVideoPath, DepthVideoPath, this->MosaicAlgorithm.Params.W, this->MosaicAlgorithm.Params.H);

	// Initialize the Mosaic models and display
	{
		vtkCollection* modelcollection = scene->GetNodesByName("MosaicModel");
		if (modelcollection->GetNumberOfItems() == 0){
			this->MosaicPointCloudModelNode->SetName("MosaicModel");
			scene->AddNode(this->MosaicPointCloudModelNode);
			scene->AddNode(this->MosaicPointCloudModelDisplayNode);
			this->MosaicPointCloudModelNode->SetHideFromEditors(0);
			this->MosaicPointCloudModelNode->SetAndObserveDisplayNodeID(this->MosaicPointCloudModelDisplayNode->GetID());

			this->MosaicPointCloudModelDisplayNode->SetVisibility(1);
			this->MosaicPointCloudModelDisplayNode->SetScalarVisibility(1);
			this->MosaicPointCloudModelDisplayNode->SetBackfaceCulling(0);
			this->MosaicPointCloudModelDisplayNode->SetScalarRangeFlag(4);
			this->MosaicPointCloudModelNode->Modified();
			this->MosaicPointCloudModelDisplayNode->Modified();
		}
		modelcollection->Delete();
	}
	{
		vtkCollection* modelcollection = scene->GetNodesByName("MosaicMeshModel");
		if (modelcollection->GetNumberOfItems() == 0){
			this->MosaicMeshModelNode->SetName("MosaicMeshModel");
			scene->AddNode(this->MosaicMeshModelNode);
			scene->AddNode(this->MosaicMeshModelDisplayNode);
			this->MosaicMeshModelNode->SetHideFromEditors(0);
			this->MosaicMeshModelNode->SetAndObserveDisplayNodeID(this->MosaicMeshModelDisplayNode->GetID());

			this->MosaicMeshModelDisplayNode->SetVisibility(1);
			this->MosaicMeshModelDisplayNode->SetScalarVisibility(1);
			this->MosaicMeshModelDisplayNode->SetBackfaceCulling(0);
			this->MosaicMeshModelDisplayNode->SetScalarRangeFlag(4);
            this->MosaicMeshModelDisplayNode->SetColor(205.0/255.0, 136.0/255.0, 75.0/255.0);
			this->MosaicMeshModelNode->Modified();
			this->MosaicMeshModelDisplayNode->Modified();
            
		}
		modelcollection->Delete();
	}
	return true;

}

void vtkSlicerTestModuleRealSenseLogic::stopPipe()
{
	this->RGBDVideoWriter.Release();
	this->pipe.stop();
}


void  vtkSlicerTestModuleRealSenseLogic::AcquireFrames()
{
	//std::cout << "Obtaining the active frames" << std::endl;
	// Create a rs2::align object.
	// rs2::align allows us to perform alignment of depth frames to others frames
	//The "align_to" is the stream type to which we plan to align depth frames.
	float depth_scale;
	rs2::align align(this->align_to);

	// Using the align object, we block the application until a frameset is available
	rs2::frameset frameset = this->pipe.wait_for_frames();

	if (profile_changed(this->pipe.get_active_profile().get_streams(), this->profile.get_streams()))
	{
		//If the profile was changed, update the align object, and also get the new device's depth scale
		this->profile = this->pipe.get_active_profile();
		this->align_to = find_stream_to_align(this->profile.get_streams());
		align = rs2::align(this->align_to);
		depth_scale = get_depth_scale(this->profile.get_device());
	}

	//Get processed aligned frame
	auto processed = align.process(frameset);

	// Trying to get both other and aligned depth frames
	rs2::video_frame other_frame = processed.first(this->align_to);
	rs2::depth_frame aligned_depth_frame = processed.get_depth_frame();
	rs2::frame colorDepth = aligned_depth_frame.apply_filter(this->color_map);

	//If one of them is unavailable, continue iteration
	if (!aligned_depth_frame || !other_frame)
	{
		return;
	}

	// Creating OpenCV Matrix from a color image
	const int w_other = other_frame.get_width();
	const int h_other = other_frame.get_height();

	//std::cout << "Width = " << w_other << "  Height = " << h_other << std::endl;

	Mat color(cv::Size(w_other, h_other), CV_8UC3, (void*)other_frame.get_data(), Mat::AUTO_STEP);
	Mat depth(cv::Size(w_other, h_other), CV_8UC3, (void*)colorDepth.get_data(), Mat::AUTO_STEP);

	// Render the image in the slice node
	this->CreateMRMLVolumeNode(&color, COLORIMAGE);
	this->CreateMRMLVolumeNode(&depth, DEPTHIMAGE);

	/*----------------------------------------------------------------------------------------*/
	// Start the mosaicing of the images
	/*----------------------------------------------------------------------------------------*/

	if (this->flag_domosaic == true)
	{
		float* depthresults = new float[(int)w_other * (int)h_other];
		auto depthdata_pointer = (uint16_t*)aligned_depth_frame.get_data();
		for (auto y = 0; y < 480; y++)
		{
			for (auto x = 0; x < 640; x++)
			{
				depthresults[y * 640 + x] = (float)(depthdata_pointer[y * 640 + x]);
				if (y == 240 && x == 320)
					std::cout << "sample depth value = " << depthdata_pointer[y * 640 + x] << std::endl;
			}
		}
		this->RGBDVideoWriter.WriteRGB(&color);
		this->RGBDVideoWriter.WriteDepth(depthresults, 640, 480);

		this->MosaicImages(color.data, depthresults);
		delete[] depthresults;
	}
	else
	{
		//this->MosaicAlgorithm.Reset();
	}
}

void vtkSlicerTestModuleRealSenseLogic::Update3DView()
{
	if (this->MosaicPointCloudModelNode->GetPolyData() == NULL)
		return;

	vtkMRMLScene* scene = this->GetMRMLScene();
	vtkCollection* collection = scene->GetNodesByClass("vtkMRMLCameraNode");
	 for (unsigned int i = 0; i < collection->GetNumberOfItems(); ++i)
	 {
		 vtkMRMLCameraNode *cameraNode = vtkMRMLCameraNode::SafeDownCast(collection->GetItemAsObject(i));
		 double center[3], bounds[6], radius[3], maxradius;
		 this->MosaicPointCloudModelNode->GetPolyData()->GetBounds(bounds);
		 center[0] = (bounds[0] + bounds[1])/2.0;
		 center[1] = (bounds[2] + bounds[3])/2.0;
		 center[2] = (bounds[4] + bounds[5])/2.0;

		 radius[0] = fabs(bounds[0]-bounds[1])/2.0;
		 radius[1] = fabs(bounds[2]-bounds[3])/2.0;
		 radius[2] = fabs(bounds[4]-bounds[5])/2.0;

		 maxradius = radius[0];
		 if (maxradius < radius[1])
			 maxradius = radius[1];
		 if (maxradius < radius[2])
			 maxradius = radius[2];

		 double campos[3];
		 campos[0] = center[0];
		 campos[1] = center[1];
		 campos[2] = center[2] - (maxradius*3.0);

		 cameraNode->SetPosition(campos[0], campos[1], campos[2]);
		 cameraNode->SetFocalPoint(center[0], center[1], center[2]);
         cameraNode->SetViewUp(0,-1,0);

         qMRMLLayoutManager *layoutManager = qSlicerApplication::application()->layoutManager();
         layoutManager->threeDWidget(0)->threeDView()->resetFocalPoint();

	 }
    collection->Delete();
}


void vtkSlicerTestModuleRealSenseLogic::MosaicImages(uchar* colordata, float* depthresults)
{
	bool trackflag;
	RealSenseData2StereoMatchingData(colordata, depthresults
			, &(this->MosaicAlgorithm.StereoMatchingData)
			, &(this->MosaicAlgorithm.Params));

	this->MosaicAlgorithm.Compute();
	if (MosaicAlgorithm.Flag_TrackingIsGood){
		std::cout << "Tracking is good" << std::endl;
		trackflag = true;

		// Generate the models
		this->GeneratePointCloudModel();
		this->Update3DView();
	}
	else
	{
		trackflag = false;
		if (this->MosaicAlgorithm.KeyFrame.size() == 0)
			std::cout << "Cannot find the first frame with enough features!" << std::endl;
		else
			std::cout << "Tracking is lost!!" << std::endl;
	}
	this->UpdateCornerAnnotation(trackflag);
}

void vtkSlicerTestModuleRealSenseLogic::UpdateCornerAnnotation(bool trackflag)
{
	qMRMLLayoutManager *layoutManager = qSlicerApplication::application()->layoutManager();
	vtkRenderer* renderer = layoutManager->threeDWidget(0)->threeDView()->renderWindow()->GetRenderers()->GetFirstRenderer();

	this->CornerAnnotation->SetLinearFontScaleFactor(8);
	this->CornerAnnotation->GetTextProperty()->BoldOn();
	char status[50];
	if (trackflag)
	{
		sprintf(status,"Normal");
		this->CornerAnnotation->GetTextProperty()->SetColor(1.0,1.0,1.0);
	}
	else
	{
		sprintf(status,"Lost");
		this->CornerAnnotation->GetTextProperty()->SetColor(1.0,0.1,0.1);
	}
	this->CornerAnnotation->SetText(0,status);
	renderer->AddViewProp(this->CornerAnnotation);
}


void vtkSlicerTestModuleRealSenseLogic::GeneratePointCloudModel()
{
	// Point cloud is stored in this->MosaicAlgorithm.Poly
	if (this->MosaicAlgorithm.Poly != NULL )
	{
		std::cout << "GENERATING the Point cloud model, Poly.NumberOfPoints = " << this->MosaicAlgorithm.Poly->GetPoints()->GetNumberOfPoints() << std::endl;
		this->MosaicAlgorithm.Points->Modified();
		this->MosaicAlgorithm.CellArray->Modified();
		this->MosaicAlgorithm.RGBArray->Modified();
		this->MosaicAlgorithm.MergingWeight->Modified();
		this->MosaicAlgorithm.Poly->Modified();
		this->MosaicPointCloudModelNode->SetAndObservePolyData(this->MosaicAlgorithm.Poly);
		this->MosaicPointCloudModelNode->Modified();
		this->MosaicPointCloudModelDisplayNode->Modified();
	}
}

void vtkSlicerTestModuleRealSenseLogic::GenerateMeshModel()
{
	// Point cloud is stored in this->MosaicAlgorithm.Poly
	if (this->MosaicAlgorithm.Mesh != NULL )
	{
		std::cout << "GENERATING the mesh model, mesh.NumberOfPoints = " << this->MosaicAlgorithm.Mesh->GetPoints()->GetNumberOfPoints() << std::endl;
		this->MosaicAlgorithm.Mesh->Modified();
		this->MosaicMeshModelNode->SetAndObservePolyData(this->MosaicAlgorithm.Mesh);
		this->MosaicMeshModelNode->Modified();
		this->MosaicMeshModelDisplayNode->Modified();
	}
}

void vtkSlicerTestModuleRealSenseLogic::CreateMRMLVolumeNode(cv::Mat* image, int imagetype)
{
	if (image->empty())
	{
		std::cerr << "Image is not initiailzed" << std::endl;
		return;
	}

	vtkSmartPointer<vtkImageData> frameData = vtkSmartPointer<vtkImageData>::New();
	cv::Size tempframeSize = image->size();
	cv::Mat outputMat(tempframeSize, CV_8U);
	int width = tempframeSize.width;
	int height = tempframeSize.height;
	frameData->SetDimensions(width, height, 1);
	frameData->SetExtent(0, width-1, 0, height-1, 0, 0 );
	frameData->SetSpacing(1.0, 1.0, 1.0);
	frameData->SetOrigin(0.0, 0.0, 0.0);
	frameData->AllocateScalars(VTK_UNSIGNED_CHAR, 3);;
	cv::Mat flippedMat(tempframeSize, CV_8U);
	cv::flip(*image, flippedMat, 0);

	int dsize = tempframeSize.width*tempframeSize.height*3;
	memcpy((void*)frameData->GetScalarPointer(), (void*)flippedMat.data, dsize);
	if (imagetype == COLORIMAGE)
	{
		this->RGBvolumenode->SetAndObserveImageData(frameData);
		// std::cout << "GOT image data" << std::endl;
	}
	else if (imagetype == DEPTHIMAGE)
	{
		this->Depthvolumenode->SetAndObserveImageData(frameData);
	}
	else
	{
		std::cout << "Type of image is incorrect" << std::endl;
	}
}

void vtkSlicerTestModuleRealSenseLogic::Refinement()
{
	if (this->MosaicAlgorithm.KeyFrame.size() == 0)
	{
		std::cout << "No keyframe is available!" << std::endl;
		return;
	}

	std::cout << "Refinement starts, it may take a while..." << std::endl;
	this->MosaicAlgorithm.Refine();
	this->GeneratePointCloudModel();

}

void vtkSlicerTestModuleRealSenseLogic::Reset()
{
	this->MosaicAlgorithm.Reset();
	this->GeneratePointCloudModel();
	//	this->GenerateMeshModel();
}


void vtkSlicerTestModuleRealSenseLogic::SetupCropExtent()
{
	if (this->MosaicPointCloudModelNode->GetPolyData() != NULL)
	{
		vtkPolyData* poly = this->MosaicPointCloudModelNode->GetPolyData();
		double bounds[6], center[3], radius[3];
		poly->GetBounds(&bounds[0]);
		center[0] = (bounds[0]+bounds[1])/2.0;
		center[1] = (bounds[2]+bounds[3])/2.0;
		center[2] = (bounds[4]+bounds[5])/2.0;
		
		radius[0] = fabs(bounds[0]-bounds[1])/2.0;
		radius[1] = fabs(bounds[2]-bounds[3])/2.0;
		radius[2] = fabs(bounds[4]-bounds[5])/2.0;

		vtkSmartPointer<vtkMRMLAnnotationROINode> roiNode = vtkSmartPointer<vtkMRMLAnnotationROINode>::New();
		roiNode->SetXYZ(center[0], center[1], center[2]);
		roiNode->SetRadiusXYZ(radius[0], radius[1], radius[2]);
		vtkMRMLScene* scene = this->GetMRMLScene();
		roiNode->SetName("CropROINode");
		roiNode->SetScene(scene);
		roiNode->SetHideFromEditors(0);
		scene->AddNode(roiNode);
		scene->SaveStateForUndo();
		roiNode->Modified();
	}
	else
	{
		std::cout << "The model has not been created" << std::endl;
	}
}



void vtkSlicerTestModuleRealSenseLogic::CropReconstructutedSurface()
{
	vtkMRMLScene* scene = this->GetMRMLScene();
	vtkCollection* collection = scene->GetNodesByName("CropROINode");

	if (this->MosaicPointCloudModelNode->GetPolyData() == NULL)
	{
		std::cout << "Point cloud model not created" << std::endl;
		return;
	}

	if (collection->GetNumberOfItems() == 0)
	{
		std::cout << "No crop node" << std::endl;
		return;
	}
	else
	{
		vtkMRMLAnnotationROINode* cropNode = vtkMRMLAnnotationROINode::SafeDownCast(collection->GetItemAsObject(0));
		double bounds[6];
		cropNode->GetBounds(&bounds[0]);
		// Cropping according to the extent chosen by user
		// Note the reference frames used by RealSense and Slicer are not the same
		vtkPolyData* inputPolyData = this->MosaicPointCloudModelNode->GetPolyData();
		vtkSmartPointer<vtkBox> cropBox = vtkSmartPointer<vtkBox>::New();
		vtkSmartPointer <vtkClipPolyData> clipper = vtkSmartPointer <vtkClipPolyData>::New();
		cropBox->SetBounds(bounds);
		clipper->SetClipFunction(cropBox);
		clipper->SetInputData(inputPolyData);
		clipper->InsideOutOn();
		clipper->Update();
		clipper->Modified();

		//Set new cropped model to the scene
		vtkSmartPointer<vtkMRMLModelNode> cropModelNode = vtkSmartPointer<vtkMRMLModelNode>::New();
		vtkSmartPointer<vtkMRMLModelDisplayNode> cropDisplayNode = vtkSmartPointer<vtkMRMLModelDisplayNode>::New();
		vtkCollection* modelcollection = scene->GetNodesByName("CropMosaicModel");
		if (modelcollection->GetNumberOfItems() == 0){
			cropModelNode->SetName("CropMosaicModel");
			scene->AddNode(cropModelNode);
			scene->AddNode(cropDisplayNode);
			cropModelNode->SetHideFromEditors(0);
			cropModelNode->SetAndObserveDisplayNodeID(cropDisplayNode->GetID());

			cropDisplayNode->SetVisibility(0);
			cropDisplayNode->SetScalarVisibility(1);
			cropDisplayNode->SetBackfaceCulling(0);
			cropDisplayNode->SetScalarRangeFlag(4);
			cropModelNode->Modified();
			cropDisplayNode->Modified();
		}
		else
		{
			cropModelNode = vtkMRMLModelNode::SafeDownCast(modelcollection->GetItemAsObject(0));
		}
		modelcollection->Delete();
		cropModelNode->SetAndObservePolyData(clipper->GetOutput());
		cropModelNode->Modified();
		this->MosaicPointCloudModelDisplayNode->SetOpacity(0.0);
		this->MosaicPointCloudModelDisplayNode->Modified();
		cropNode->SetDisplayVisibility(0);
		
		// Generate the mesh model
		std::cout << "Start to generate the mesh!" << std::endl;
		this->MosaicAlgorithm.MeshGeneration(cropModelNode->GetPolyData());
		this->GenerateMeshModel();
		std::cout << "Mesh generation is finished!" << std::endl;
	}
    collection->Delete();
}


float vtkSlicerTestModuleRealSenseLogic::get_depth_scale(rs2::device dev)
{
	// Go over the device's sensors
	for (rs2::sensor& sensor : dev.query_sensors())
	{
		// Check if the sensor if a depth sensor
		if (rs2::depth_sensor dpt = sensor.as<rs2::depth_sensor>())
		{
			return dpt.get_depth_scale();
		}
	}
	throw std::runtime_error("Device does not have a depth sensor");

}

rs2_stream vtkSlicerTestModuleRealSenseLogic::find_stream_to_align(const std::vector<rs2::stream_profile>& streams)
{
	//Given a vector of streams, we try to find a depth stream and another stream to align depth with.
	//We prioritize color streams to make the view look better.
	//If color is not available, we take another stream that (other than depth)
	rs2_stream align_to = RS2_STREAM_ANY;
	bool depth_stream_found = false;
	bool color_stream_found = false;
	for (rs2::stream_profile sp : streams)
	{
		rs2_stream profile_stream = sp.stream_type();
		if (profile_stream != RS2_STREAM_DEPTH)
		{
			if (!color_stream_found)         //Prefer color
				align_to = profile_stream;

			if (profile_stream == RS2_STREAM_COLOR)
			{
				color_stream_found = true;
			}
		}
		else
		{
			depth_stream_found = true;
		}
	}

	if(!depth_stream_found)
		throw std::runtime_error("No Depth stream available");

	if (align_to == RS2_STREAM_ANY)
		throw std::runtime_error("No stream found to align with Depth");

	return align_to;
}

bool vtkSlicerTestModuleRealSenseLogic::profile_changed(const std::vector<rs2::stream_profile>& current, const std::vector<rs2::stream_profile>& prev)
{
	for (auto&& sp : prev)
	{
		//If previous profile is in current (maybe just added another)
		auto itr = std::find_if(std::begin(current), std::end(current), [&sp](const rs2::stream_profile& current_sp) { return sp.unique_id() == current_sp.unique_id(); });
		if (itr == std::end(current)) //If it previous stream wasn't found in current
		{
			return true;
		}
	}
	return false;
}

