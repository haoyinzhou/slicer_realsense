#ifndef RigidMosaicking_PointsManagement_H
#define RigidMosaicking_PointsManagement_H

#include <iostream>
#include <time.h>   
#include <stdio.h>     
#include <stdlib.h>     

#include "opencv2/opencv.hpp"
#include <vector>
#include "DualQ.h"
#include "DataStructure.h"


//using namespace std;
//using namespace cv;


void GenerateLivePoints(vtkPoints* Points, vtkPoints* Points_live
	, double* dcm_, double* T);

vtkIdType InitialAddPoints(vtkPoints* Points, vtkPoints* Points_live, vtkCellArray* CellArray, vtkUnsignedCharArray* RGBArray, vtkFloatArray* MergingWeight
							, CStereoMatchingResult* StereoMatchingResult
							, int W, int H );

/*
long RuntimeAddNewPointsMainFunction(CPoint* Points, long OldPointCount
				, CReprojectedExistingPoints* ReprojectedExistingPoints
				, CStereoMatchingResult* StereoMatchingResult
				, uchar* Mask_Uncovered
				, double* dcm, double* T
				, CParams* Params
				, int W, int H);
*/

vtkIdType RuntimeAddNewPointsMainFunction_new(vtkPoints* Points, vtkPoints* Points_live, vtkCellArray* CellArray, vtkUnsignedCharArray* RGBArray, vtkFloatArray* MergingWeight
				, CStereoMatchingResult* StereoMatchingResult
				, double* dcm, double* T
				, CParams* Params
				, int W, int H
				, bool flag_usekdtree);

void MergeLivePointsAndUpdateOrigCoord(vtkPoints* Points, vtkPoints* Points_live, vtkFloatArray* MergingWeight
				, double* dcm, double* T
				, CStereoMatchingResult* StereoMatchingData
				, CParams* Params
				, int W, int H);
							

#endif






