/*==============================================================================

  Program: 3D Slicer

  Portions (c) Copyright Brigham and Women's Hospital (BWH) All Rights Reserved.

  See COPYRIGHT.txt
  or http://www.slicer.org/copyright/copyright.txt for details.

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

==============================================================================*/

// .NAME vtkSlicerTestModuleRealSenseLogic - slicer logic class for volumes manipulation
// .SECTION Description
// This class manages the logic associated with reading, saving,
// and changing propertied of the volumes


#ifndef __vtkSlicerTestModuleRealSenseLogic_h
#define __vtkSlicerTestModuleRealSenseLogic_h

// Slicer includes
#include "vtkSlicerTestModuleRealSenseModuleLogicExport.h"
#include "vtkSlicerModuleLogic.h"
#include "vtkMRMLVectorVolumeNode.h"
#include <QTimer>
#include <QObject>
#include <librealsense2/rs.hpp>
#include "DataStructure.h"
#include "RigidMosaicking_CameraMotionEstimation.h"
#include "common.h"
#include "StereoVision.h"
#include "FinalRefinement.h"
#include "RecordVideo.h"
#include "vtkMRMLModelNode.h"
#include "vtkMRMLModelDisplayNode.h"

class vtkCornerAnnotation;


/// \ingroup Slicer_QtModules_ExtensionTemplate
class VTK_SLICER_TESTMODULEREALSENSE_MODULE_LOGIC_EXPORT vtkSlicerTestModuleRealSenseLogic :
  public vtkSlicerModuleLogic
{
   // Q_OBJECT
public:

  static vtkSlicerTestModuleRealSenseLogic *New();
  vtkTypeMacro(vtkSlicerTestModuleRealSenseLogic, vtkSlicerModuleLogic);
  void PrintSelf(ostream& os, vtkIndent indent);
  float get_depth_scale(rs2::device dev);
  rs2_stream find_stream_to_align(const std::vector<rs2::stream_profile>& streams);
  bool profile_changed(const std::vector<rs2::stream_profile>& current, const std::vector<rs2::stream_profile>& prev);
 
protected:
  vtkSlicerTestModuleRealSenseLogic();
  virtual ~vtkSlicerTestModuleRealSenseLogic();
  virtual void SetMRMLSceneInternal(vtkMRMLScene* newScene);
  virtual void RegisterNodes();
  virtual void UpdateFromMRMLScene();
  virtual void OnMRMLSceneNodeAdded(vtkMRMLNode* node);
  virtual void OnMRMLSceneNodeRemoved(vtkMRMLNode* node);

public:
    bool flag_domosaic;
    bool flag_RGB;

public:
	  
	    
    
public:
  bool InitializeCamera();
  void AcquireFrames();
  void CreateMRMLVolumeNode(cv::Mat* image, int imagetype);
  void MosaicImages(uchar* colordata, float* depthresults);
  void Refinement();
  void Reset();
  void GeneratePointCloudModel();
  void GenerateMeshModel();
  void stopPipe();
  void SetupCropExtent();
  void CropReconstructutedSurface();
  void Update3DView();
  void UpdateCornerAnnotation(bool trackflag);
  rs2::pipeline pipe;
  rs2::pipeline_profile profile;
  rs2::frameset frameset;
  rs2::colorizer color_map;
  rs2_stream align_to;
  vtkMRMLVectorVolumeNode* RGBvolumenode;
  vtkMRMLVectorVolumeNode* Depthvolumenode;
  vtkMRMLModelNode* MosaicPointCloudModelNode;
  vtkMRMLModelDisplayNode* MosaicPointCloudModelDisplayNode;
  vtkMRMLModelNode* MosaicMeshModelNode;
  vtkMRMLModelDisplayNode* MosaicMeshModelDisplayNode;
  CMosaicAlgorithm MosaicAlgorithm;
  CRGBDVideoWriter RGBDVideoWriter;
  vtkCornerAnnotation* CornerAnnotation;
  
    enum {
  	 COLORIMAGE = 0,
  	 DEPTHIMAGE = 1,
    };

    enum {
  	 LEFT = 0,
  	 RIGHT = 1,
  	 SUPERIOR = 2,
  	 INFERIOR = 3,
    };


private:
  vtkSlicerTestModuleRealSenseLogic(const vtkSlicerTestModuleRealSenseLogic&); // Not implemented
  void operator=(const vtkSlicerTestModuleRealSenseLogic&); // Not implemented
  
};



#endif
