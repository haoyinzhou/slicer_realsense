#include "InitialMemory.h"

using namespace cv;
using namespace std;


void InitialMemory_StereoVision(CStereoMatchingData* StereoMatchingData
	, CStereoMatchingResult* StereoMatchingResult
	, int W, int H)
{
	//CPU
	StereoMatchingData->img1 = new uchar[W * H];
	StereoMatchingData->img1_RGB = new uchar[3 * W * H];

	StereoMatchingData->disparitymap = new float[W * H];

	StereoMatchingData->Mask = new uchar[W * H];
	StereoMatchingData->Mask_temp = new uchar[W * H];

	StereoMatchingData->disparitymap_forsmooth1 = new float[W * H];
	StereoMatchingData->disparitymap_forsmooth2 = new float[W * H];
	StereoMatchingData->disparitymap_forsmooth3 = new float[W * H];

	// CPU
	StereoMatchingResult->Mask = new uchar[W * H];
	StereoMatchingResult->disparity = new float[W * H];
//	StereoMatchingResult->intense = new uchar[W * H];
	StereoMatchingResult->RGB = new uchar[3 * W * H];
	StereoMatchingResult->coord = new double[3 * W * H];
//	StereoMatchingResult->normal = new double[3 * W * H];
	StereoMatchingResult->Poly = vtkSmartPointer<vtkPolyData>::New();
	StereoMatchingResult->Points = vtkSmartPointer<vtkPoints>::New();
	StereoMatchingResult->Poly->SetPoints(StereoMatchingResult->Points);

	StereoMatchingResult->flag_usedbymerge = new uchar[W * H];
	memset(StereoMatchingResult->flag_usedbymerge, 0, W * H * sizeof(uchar));
	StereoMatchingResult->flag_usedbyadd = new uchar[W * H];
	memset(StereoMatchingResult->flag_usedbyadd, 0, W * H * sizeof(uchar));
	//GPU	
}



void ClearMemory_StereoVision(CStereoMatchingData* StereoMatchingData
	, CStereoMatchingResult* StereoMatchingResult)
{
	delete[] StereoMatchingData->img1;
	delete[] StereoMatchingData->img1_RGB;
	delete[] StereoMatchingData->disparitymap;

	delete[] StereoMatchingData->Mask;
	delete[] StereoMatchingData->Mask_temp;

	delete[] StereoMatchingData->disparitymap_forsmooth1;
	delete[] StereoMatchingData->disparitymap_forsmooth2;
	delete[] StereoMatchingData->disparitymap_forsmooth3;

	delete[] StereoMatchingResult->Mask;
	delete[] StereoMatchingResult->disparity;
//	delete[] StereoMatchingResult->intense;
	delete[] StereoMatchingResult->RGB;
	delete[] StereoMatchingResult->coord;
//	delete[] StereoMatchingResult->normal;
	delete[] StereoMatchingResult->flag_usedbymerge;
	delete[] StereoMatchingResult->flag_usedbyadd;
}


void InitialMemory_DataManagement(uchar*& Mask_Uncovered
						, int W, int H)
{
/*	// Points
	{
		Points->flag = new uchar[MaxNumberofPoints];
		Points->coord = new double[3 * MaxNumberofPoints];
		Points->coord_live = new double[3 * MaxNumberofPoints];
	//	Points->intense = new uchar[MaxNumberofPoints];
		Points->RGB = new uchar[3 * MaxNumberofPoints];
		Points->MergingWeight = new double[MaxNumberofPoints];
	//	Points->closest_u = new int[MaxNumberofPoints];
	//	Points->closest_v = new int[MaxNumberofPoints];
	//	Points->cost = new double[MaxNumberofPoints];
	}	
*/
//	AreaRow_UnCovered_GPU = new long[W];

	Mask_Uncovered = new uchar[W * H];
	memset(Mask_Uncovered, 0, W * H * sizeof(uchar));
}

/*
void ClearMemory_DataManagement(CPoint* Points)

{
	// Points
	{
		delete[] Points->flag;
		delete[] Points->coord;
		delete[] Points->coord_live;
	//	delete[] Points->intense;
		delete[] Points->RGB;
		delete[] Points->MergingWeight;
		//	delete[] Points->closest_u;
		//	delete[] Points->closest_v;
		//	delete[] Points->cost;
	}
}
*/

void InitialMemory_FeaturePoints(CTrackingFrame* TrackingFrame, cv::Mat* img1, unsigned int MaxNumberOfFeaturePointsPerFrame, int W, int H)
{

	for (int i = 0; i < 2; i ++)
	{
		TrackingFrame[i].dcm = new double[9];
		TrackingFrame[i].T = new double[3];		
	//	TrackingFrame[i].u = new double[MaxNumberOfFeaturePointsPerFrame];
	//	TrackingFrame[i].v = new double[MaxNumberOfFeaturePointsPerFrame];
	//	TrackingFrame[i].Discriptor = new uchar[32 * MaxNumberOfFeaturePointsPerFrame];
	//	cudaMalloc((void **)&(TrackingFrame[i].DiscriptorData_GPU), MaxNumberOfFeaturePointsPerFrame * 32 * sizeof(uchar));		
		TrackingFrame[i].flag_coordisvalid = new uchar[MaxNumberOfFeaturePointsPerFrame];
		TrackingFrame[i].coord = new double[3 * MaxNumberOfFeaturePointsPerFrame];
		TrackingFrame[i].coord_live = new double[3 * MaxNumberOfFeaturePointsPerFrame];
		TrackingFrame[i].MatchedID = new int[MaxNumberOfFeaturePointsPerFrame]; 
		TrackingFrame[i].globalpid = new int[MaxNumberOfFeaturePointsPerFrame]; 
		TrackingFrame[i].localpid = new int[MaxNumberOfFeaturePointsPerFrame]; 
		
		TrackingFrame[i].img = img1;
	//	TrackingFrame[i].CornerResponseMap = new int[W * H];
	}
	
//	TrackingFrame_keyframe->CornerResponseMap = new int[W * H];
}

void ClearMemory_FeaturePoints(CTrackingFrame* TrackingFrame, std::vector<CKeyFrame>* KeyFrame)
{
	for (int i = 0; i < 2; i ++)
	{
	//	delete[] TrackingFrame[i].u;
	//	delete[] TrackingFrame[i].v;
	//	delete[] TrackingFrame[i].Discriptor;
		delete[] TrackingFrame[i].flag_coordisvalid;
		delete[] TrackingFrame[i].coord;
		delete[] TrackingFrame[i].coord_live;
		delete[] TrackingFrame[i].MatchedID;
		delete[] TrackingFrame[i].globalpid;
		delete[] TrackingFrame[i].localpid;		
	
		delete[] TrackingFrame[i].dcm;
		delete[] TrackingFrame[i].T;
	
//		delete[] TrackingFrame[i].CornerResponseMap;

		TrackingFrame[i].ORBkeypoints.clear();
	}	
//	delete[] TrackingFrame;

//	delete[] TrackingFrame_keyframe->CornerResponseMap;

	for (unsigned int i = 0; i < KeyFrame->size(); i ++)
	{
		if (KeyFrame->at(i).RGB != NULL)
			delete[] KeyFrame->at(i).RGB;
		if (KeyFrame->at(i).depth != NULL)	
			delete[] KeyFrame->at(i).depth;
	}

}

/*


*/
