/*==============================================================================

  Program: 3D Slicer

  Portions (c) Copyright Brigham and Women's Hospital (BWH) All Rights Reserved.

  See COPYRIGHT.txt
  or http://www.slicer.org/copyright/copyright.txt for details.

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

==============================================================================*/

#ifndef __qSlicerTestModuleRealSenseModuleWidget_h
#define __qSlicerTestModuleRealSenseModuleWidget_h

// SlicerQt includes
#include "qSlicerAbstractModuleWidget.h"

#include "qSlicerTestModuleRealSenseModuleExport.h"

#include <QTimer>
//#include <QCheckBox>

class qSlicerTestModuleRealSenseModuleWidgetPrivate;
class vtkMRMLNode;

/// \ingroup Slicer_QtModules_ExtensionTemplate
class Q_SLICER_QTMODULES_TESTMODULEREALSENSE_EXPORT qSlicerTestModuleRealSenseModuleWidget :
  public qSlicerAbstractModuleWidget
{
  Q_OBJECT

public:

  typedef qSlicerAbstractModuleWidget Superclass;
  qSlicerTestModuleRealSenseModuleWidget(QWidget *parent=0);
  virtual ~qSlicerTestModuleRealSenseModuleWidget();
  void SetupDisplayPanels();

public slots:
    void TimerFunc();
    bool InitializeCamera();
    void RGBcheckBox();
    void RGBcheckBox_stateChanged(bool);
    void depthcheckBox();
    void depthcheckBox_stateChanged();
    void mosaiccheckBox();
    void mosaiccheckBox_stateChanged(bool);
    void CropExtentButtonPressed();
    void RefineButtonPressed();
    void CropButtonPressed();
    

protected:
  QScopedPointer<qSlicerTestModuleRealSenseModuleWidgetPrivate> d_ptr;
  int counter;
  enum {
	 LEFT = 0,
	 RIGHT = 1,
	 SUPERIOR = 2,
	 INFERIOR = 3,
  };

  virtual void setup();
    
  protected slots:
    void stopliveButton_clicked();
    void loadButton();

private:
  Q_DECLARE_PRIVATE(qSlicerTestModuleRealSenseModuleWidget);
  Q_DISABLE_COPY(qSlicerTestModuleRealSenseModuleWidget);
};

#endif
