#ifndef StereoVision_H
#define StereoVision_H

#include "DataStructure.h"

#include <stdio.h>
#include <iostream>     // std::cout
#include <fstream>      // std::ifstream

//using namespace std;

#define WinSize 5
#define WinFullSize (2 * WinSize + 1)
#define WinPixNum (WinFullSize * WinFullSize)
#define WinPixNum_inv 1/WinPixNum



// RGBD-related
void RealSenseData2StereoMatchingData(uchar* RGB_in, float* depth_in
                                      , CStereoMatchingData* StereoMatchingData
                                      , CParams* Params);



bool ReadRGBDDataFromFiles(const char* RGBFileName, const char* DepthFileName
						, CStereoMatchingData* StereoMatchingData
						, CParams* Params);

bool PostProcessingDepthData(CStereoMatchingData* StereoMatchingData
	, CParams* Params);

void RGBDData2Results(CStereoMatchingData* StereoMatchingData
	, CStereoMatchingResult* StereoMatchingResult
	, CParams* Params);

void GenerateImgPhyCoord(uchar* Mask, float* disparity, double* coord, vtkPoints* Points
						, int W, int H
						, double FocalLength, double ImgCenterU, double ImgCenterV);

void GenerateImgNormal(uchar* Mask, double* coord, double* normal
										, int W, int H);
										

#define CrossProduct(u1, u2, u3, v1, v2, v3, out1, out2, out3) \
{ \
	out1 = u2 * v3 - v2 * u3; \
	out2 = v1 * u3 - u1 * v3; \
	out3 = u1 * v2 - v1 * u2; \
} \

#endif
