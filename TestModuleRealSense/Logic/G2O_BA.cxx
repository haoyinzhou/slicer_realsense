#include "G2O_BA.h"

using namespace cv;
using namespace std;
using namespace Eigen;


void CG2OParameters::Initialization(double FocalLength, double ImgCenterU, double ImgCenterV)
{
	this->linearSolver = g2o::make_unique<g2o::LinearSolverCholmod<g2o::BlockSolver_6_3::PoseMatrixType>>();
	this->solver = new g2o::OptimizationAlgorithmLevenberg(g2o::make_unique<g2o::BlockSolver_6_3>(std::move(this->linearSolver)));
	this->optimizer.setAlgorithm(this->solver);
	
	this->camera = new g2o::CameraParameters(FocalLength, Eigen::Vector2d(ImgCenterU, ImgCenterV), 0);
	this->camera->setId(0);
	this->optimizer.addParameter(this->camera);
}

/*    g2o::SparseOptimizer optimizer;
	std::unique_ptr<g2o::BlockSolver_6_3::LinearSolverType> linearSolver = g2o::make_unique<g2o::LinearSolverCholmod<g2o::BlockSolver_6_3::PoseMatrixType>>();
	g2o::OptimizationAlgorithmLevenberg* solver = new g2o::OptimizationAlgorithmLevenberg( g2o::make_unique<g2o::BlockSolver_6_3>(std::move(linearSolver))  );
	optimizer.setAlgorithm(solver);
	g2o::CameraParameters* camera = new g2o::CameraParameters( FocalLength, Eigen::Vector2d(ImgCenterU, ImgCenterV), 0 );
	camera->setId(0);
	optimizer.addParameter( camera );
*/
//	G2OParameters->optimizer.clear();


//#define MaxNumberOfKeyFramesForBA 5

//, double FocalLength, double ImgCenterU, double ImgCenterV

void BundleAdjustment_G2O(std::vector<CKeyFrame> *KeyFrame, std::vector<CFeatureVector> *GlobalFeatureVector
			, CTrackingFrame *TrackingFrame_this
			, CG2OParameters *G2OParameters
			, size_t MaxNumberOfKeyFramesForBA, size_t MinNumberOfPointsForBA, int BAIterNumber
			, bool flag_islocalBA)
{
	if (KeyFrame->size() < 2)
		return;

	int FirstKeyFrameID = KeyFrame->size() - MaxNumberOfKeyFramesForBA;
	if (FirstKeyFrameID < 0)
		FirstKeyFrameID = 0;

	G2OParameters->optimizer.clear();

	//	vector<g2o::SE3Quat,aligned_allocator<g2o::SE3Quat> > pnp_poses;

	size_t vertex_count = 0;
	for (size_t fid = FirstKeyFrameID; fid < KeyFrame->size(); fid++)
	{
		//	Eigen::Matrix3d dcm_eigen(LocalFrame->at(fid).dcm); // eigen automatically transpose the matrix, so stupid.
		Eigen::Matrix3d dcm_eigen;
		dcm_eigen << KeyFrame->at(fid).dcm[0], KeyFrame->at(fid).dcm[1], KeyFrame->at(fid).dcm[2], KeyFrame->at(fid).dcm[3], KeyFrame->at(fid).dcm[4], KeyFrame->at(fid).dcm[5], KeyFrame->at(fid).dcm[6], KeyFrame->at(fid).dcm[7], KeyFrame->at(fid).dcm[8];
		Eigen::Vector3d T_eigen(KeyFrame->at(fid).T);
		g2o::SE3Quat pose(dcm_eigen, T_eigen);

		g2o::VertexSE3Expmap *v_se3 = new g2o::VertexSE3Expmap();
		v_se3->setId(vertex_count);
		v_se3->setEstimate(pose);
		
		if (flag_islocalBA == false)
		{
			if (fid == (size_t)FirstKeyFrameID) // || (fid == (size_t)FirstKeyFrameID + 1 && KeyFrame->size() > 2))
				v_se3->setFixed(true);
			else
				v_se3->setFixed(false);
		}
		else
		{
			if (fid == KeyFrame->size() - 1) 
				v_se3->setFixed(false);
			else
				v_se3->setFixed(true);		
		}
		
//		v_se3->setFixed(false);
		
		G2OParameters->optimizer.addVertex(v_se3);
		vertex_count++;

		//	Eigen::MatrixXd temp(3, 4);
		//	temp << dcm_eigen, T_eigen;
		//	std::cout << "pose_before = " << std::endl << temp << std::endl;
		//	pnp_poses.push_back(pose);
	}
	size_t NumofPoses = KeyFrame->size();
	if (NumofPoses > MaxNumberOfKeyFramesForBA)
		NumofPoses = MaxNumberOfKeyFramesForBA;

	vector<size_t> globalpid_of_vertex;
	for (size_t i = 0; i < NumofPoses; i++)
		globalpid_of_vertex.push_back(0);

	vector<g2o::EdgeProjectXYZ2UV*> edge_vector;
	for (size_t pid = 0; pid < GlobalFeatureVector->size(); pid++)
	{
		size_t NumberOfObservedKeyFrames = GlobalFeatureVector->at(pid).ObservedGlobalKeyFrameID.size();
		if (NumberOfObservedKeyFrames < 2)
			continue;

		size_t FirstValidI = GlobalFeatureVector->at(pid).ObservedGlobalKeyFrameID.size();
		for (size_t i = 0; i < NumberOfObservedKeyFrames; i++)
		{
			if (GlobalFeatureVector->at(pid).ObservedGlobalKeyFrameID.at(i) >= FirstKeyFrameID)
			{
				FirstValidI = i;
				break;
			}
		}
		if (FirstValidI > NumberOfObservedKeyFrames - 2)
			continue;

		bool flag_observedatAnchorKeyFrame = (GlobalFeatureVector->at(pid).ObservedGlobalKeyFrameID.at(FirstValidI) == FirstKeyFrameID) ? true : false;

		// add this point to g2o as a vertex
		{
			Eigen::Vector3d coord_feature(GlobalFeatureVector->at(pid).coord);

			g2o::VertexSBAPointXYZ *v_point = new g2o::VertexSBAPointXYZ();
			v_point->setId(vertex_count);
			v_point->setMarginalized(true);
			v_point->setEstimate(coord_feature);
		//	v_point->setFixed(flag_observedatAnchorKeyFrame); // I am trying to avoid the scale drift problem of BA, maybe it is not needed
			if (flag_islocalBA) v_point->setFixed(true);
			else  v_point->setFixed(flag_observedatAnchorKeyFrame);
			
			G2OParameters->optimizer.addVertex(v_point);

			globalpid_of_vertex.push_back(pid);
		}

		// add edges to g2o
		for (size_t i = FirstValidI; i < NumberOfObservedKeyFrames; i++)
		{
			int fid = GlobalFeatureVector->at(pid).ObservedGlobalKeyFrameID.at(i);
			int vertexid_frame = fid - FirstKeyFrameID;

			double u = GlobalFeatureVector->at(pid).u.at(i);
			double v = GlobalFeatureVector->at(pid).v.at(i);

			g2o::EdgeProjectXYZ2UV *edge = new g2o::EdgeProjectXYZ2UV();
			edge->setVertex(0, dynamic_cast<g2o::VertexSBAPointXYZ *>(G2OParameters->optimizer.vertex(vertex_count)));
			edge->setVertex(1, dynamic_cast<g2o::VertexSE3Expmap *>(G2OParameters->optimizer.vertex(vertexid_frame)));
			edge->setMeasurement(Eigen::Vector2d(u, v));
			edge->setInformation(Eigen::Matrix2d::Identity());
			edge->setParameterId(0, 0);

			edge->setRobustKernel(new g2o::RobustKernelHuber());
			G2OParameters->optimizer.addEdge(edge);

			edge_vector.push_back(edge);
		}
		vertex_count++;
	}

	std::cout << "Number of points for BA optimization = " << vertex_count - NumofPoses << ", Number of key frames = " << NumofPoses << std::endl;
	if (vertex_count - NumofPoses < MinNumberOfPointsForBA)
	{
		std::cout << "No sufficient number of vertex, BA fails, Number of points should be at least " << MinNumberOfPointsForBA << std::endl;
		return;
	}

//	G2OParameters->optimizer.setVerbose(true);
//	G2OParameters->optimizer.initializeOptimization();
//	G2OParameters->optimizer.optimize(BAIterNumber);

	int iternum = 4;
//	if (flag_islocalBA) iternum = 2;
//	else iternum = 4;
	for (int iter = 0; iter < iternum; iter ++)
	{
		G2OParameters->optimizer.setVerbose(!flag_islocalBA);
		G2OParameters->optimizer.initializeOptimization(0);
		G2OParameters->optimizer.optimize(BAIterNumber);
	    for(size_t i = 0; i < edge_vector.size(); i ++)
		{
			g2o::EdgeProjectXYZ2UV* e = edge_vector.at(i);
	        e->computeError();
			const float chi2 = e->chi2();
		//	std::cout << "i = " << i << ", chi2 = " << chi2 << std::endl;
			if(chi2 > 6.0)	e->setLevel(1);
	        else e->setLevel(0);
	        
			if(iter == 2) e->setRobustKernel(0);
		}
	}

/*
	for (int vid = 0; vid < NumofPoses; vid ++)
	{
		g2o::VertexSE3Expmap* v_pose = dynamic_cast<g2o::VertexSE3Expmap*>( optimizer.vertex(vid) );
		Eigen::Isometry3d pose = v_pose->estimate();
		std::cout <<"Pose_after = " << endl << pose.matrix() << std::endl;
	}
*/
	
	for (size_t i = FirstKeyFrameID; i < FirstKeyFrameID + NumofPoses; i ++)
	{
		Eigen::Isometry3d pose_eigen = dynamic_cast<g2o::VertexSE3Expmap *>(G2OParameters->optimizer.vertex(i-FirstKeyFrameID))->estimate();
		for (int l1 = 0; l1 < 3; l1++)
			for (int l2 = 0; l2 < 3; l2++)
				KeyFrame->at(i).dcm[3 * l1 + l2] = pose_eigen.matrix()(l1, l2);
		for (int l = 0; l < 3; l++)
			KeyFrame->at(i).T[l] = pose_eigen.matrix()(l, 3);

	/*	if (flag_islocalBA && i == FirstKeyFrameID + NumofPoses - 1)
		{
			printf("KeyFrame->size() = %u, i = %d, NumofPoses = %d\n", KeyFrame->size(), i, NumofPoses);
			printf("%.2f %.2f %.2f  \n", KeyFrame->at(i).dcm[0], KeyFrame->at(i).dcm[1], KeyFrame->at(i).dcm[2]);
			printf("%.2f %.2f %.2f  \n", KeyFrame->at(i).dcm[3], KeyFrame->at(i).dcm[4], KeyFrame->at(i).dcm[5]);
			printf("%.2f %.2f %.2f  \n", KeyFrame->at(i).dcm[6], KeyFrame->at(i).dcm[7], KeyFrame->at(i).dcm[8], TrackingFrame_this->dcm[6]);
			printf("%.2f %.2f %.2f  \n", KeyFrame->at(i).T[0], KeyFrame->at(i).T[1], KeyFrame->at(i).T[2]);
		}
	*/
	}

	if (flag_islocalBA == false)
	{
		// get new coords for global feature points
		for (size_t i = NumofPoses; i < vertex_count; i++)
		{
			g2o::VertexSBAPointXYZ *pointcoord_eigen = dynamic_cast<g2o::VertexSBAPointXYZ *>(G2OParameters->optimizer.vertex(i));
		//	g2o::VertexSBAPointXYZ *pointcoord_eigen = static_cast<g2o::VertexSBAPointXYZ *>(G2OParameters->optimizer.vertex(i));
			size_t pid = globalpid_of_vertex.at(i);
			//	std::cout << "point_after (" << pid << ") = " << pointcoord_eigen->estimate()(0) << ", " << pointcoord_eigen->estimate()(1) << ", " << pointcoord_eigen->estimate()(2) << ", " << std::endl;
			//	std::cout << "point_befor (" << pid << ") = " << GlobalFeatureVector->at(pid).coord[0] << ", " << GlobalFeatureVector->at(pid).coord[1] << ", " << GlobalFeatureVector->at(pid).coord[2] << std::endl;
			for (int l = 0; l < 3; l++)
				GlobalFeatureVector->at(pid).coord[l] = pointcoord_eigen->estimate()(l);
		}
	}
	
	if (TrackingFrame_this == NULL) return;

	Eigen::Isometry3d pose_eigen = dynamic_cast<g2o::VertexSE3Expmap *>(G2OParameters->optimizer.vertex(NumofPoses - 1))->estimate();
	for (int l1 = 0; l1 < 3; l1++)
		for (int l2 = 0; l2 < 3; l2++)
			TrackingFrame_this->dcm[3 * l1 + l2] = pose_eigen.matrix()(l1, l2);
	for (int l = 0; l < 3; l++)
		TrackingFrame_this->T[l] = pose_eigen.matrix()(l, 3);

/*	if (flag_islocalBA)
	{
		printf("%.2f %.2f %.2f\n", TrackingFrame_this->dcm[0], TrackingFrame_this->dcm[1], TrackingFrame_this->dcm[2]);
		printf("%.2f %.2f %.2f\n", TrackingFrame_this->dcm[3], TrackingFrame_this->dcm[4], TrackingFrame_this->dcm[5]);
		printf("%.2f %.2f %.2f\n", TrackingFrame_this->dcm[6], TrackingFrame_this->dcm[7], TrackingFrame_this->dcm[8]);
		printf("%.2f %.2f %.2f\n", TrackingFrame_this->T[0], TrackingFrame_this->T[1], TrackingFrame_this->T[2]);
	}
*/
	if (flag_islocalBA)
	{
		for (size_t i = 0; i < TrackingFrame_this->ORBkeypoints.size(); i++)
		{
			if (TrackingFrame_this->flag_coordisvalid[i] != 255)
				continue;

			double coord[3], coord_live[3];
			for (int l = 0; l < 3; l++)
				coord_live[l] = TrackingFrame_this->coord_live[3 * i + l];
			DCMT_LiveCoord_2_OrigCoord(TrackingFrame_this->dcm, TrackingFrame_this->T, coord_live, coord);
			for (int l = 0; l < 3; l++)
				TrackingFrame_this->coord[3 * i + l] = coord[l];
		}
	}
	
/*	for (size_t i = 0; i < TrackingFrame_this->ORBkeypoints.size(); i++)
	{
		if (TrackingFrame_this->flag_coordisvalid[i] != 255)
			continue;

		int pid = TrackingFrame_this->globalpid[i];
		if (pid < 0) continue;

		//	printf("before: i = %d (%d), %.3f %.3f %.3f\n", i, LocalFeatureVector->at(pid).ObservedLocalFrameID.size(), TrackingFrame_this->coord[3 * i + 0], TrackingFrame_this->coord[3 * i + 1], TrackingFrame_this->coord[3 * i + 2]);
		if (GlobalFeatureVector->at(pid).ObservedGlobalKeyFrameID.size() < 2)
		{
			double coord[3], coord_live[3];
			for (int l = 0; l < 3; l++)
				coord_live[l] = TrackingFrame_this->coord_live[3 * i + l];
			DCMT_LiveCoord_2_OrigCoord(TrackingFrame_this->dcm, TrackingFrame_this->T, coord_live, coord);
			for (int l = 0; l < 3; l++)
				TrackingFrame_this->coord[3 * i + l] = coord[l];
		}
		else
		{
			for (int l = 0; l < 3; l++)
				TrackingFrame_this->coord[3 * i + l] = GlobalFeatureVector->at(pid).coord[l];
		}
		//	printf("after : i = %d (%d), %.3f %.3f %.3f\n", i, LocalFeatureVector->at(pid).ObservedLocalFrameID.size(), TrackingFrame_this->coord[3 * i + 0], TrackingFrame_this->coord[3 * i + 1], TrackingFrame_this->coord[3 * i + 2]);
	}
*/	
}
